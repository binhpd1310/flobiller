package com.app.flocash.appflobiller.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.CountryWithFlagAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.models.CountryResponseModel;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;
import com.app.flocash.appflobiller.utilities.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 12/11/2015.
 */
public class ProfileFragment extends FloBillerBaseFragment {
    private String mCountryCode;
    private String mCountryPhoneCode;
    private String mCurrency;
    private String mEmail;
    private String mLastName;
    private String mDateOfBirth;
    private String mFirstName;
    private String mCustomerId;
    private String mMobileNumber;
    private ProgressDialog mProgressDialog;

    @Bind(R.id.edtFirstName)
    EditText mEdtFirstName;
    @Bind(R.id.edtLastName)
    EditText mEdtLastName;
    @Bind(R.id.edtDateOfBirth)
    EditText mEdtDateOfBirth;
    @Bind(R.id.edtMobileNumber)
    EditText mEdtMobileNumber;
    @Bind(R.id.spSelectCountry)
    Spinner mSpCountry;
    @Bind(R.id.tvEmail)
    TextView mTvEmail;

    ArrayList<CountryResponseModel> countryResponseModels;
    private boolean mUpdateInProcessing = false;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onCreateViewFragment(View view) {
        countryResponseModels = new ArrayList<>();
        listeners();
        getCountryList();
    }

    @OnClick({R.id.ivFlagFrench, R.id.ivFlagEnglish})
    public void onClick(View view) {
        SavedSharedPreference savedSharedPreference = new SavedSharedPreference(mActivity);
        switch (view.getId()) {
            case R.id.ivFlagEnglish:
                savedSharedPreference.setLanguage(FlobillerConstaints.ENGLISH_LANGUAGE);
                setLanguage();
                restartApp();
                break;

            case R.id.ivFlagFrench:
                savedSharedPreference.setLanguage(FlobillerConstaints.FRENCH_LANGUAGE);
                setLanguage();
                restartApp();
                break;
        }
    }

    private void restartApp() {
        Intent i = mActivity.getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( mActivity.getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void listeners() {
        mSpCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCountryCode = countryResponseModels.get(position).getKey();
                mCountryPhoneCode = countryResponseModels.get(position).getCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }


    public void retrieveUserInfo(boolean isShowing) {
        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_USER_INFO, client, this);
        js.setShowDialog(isShowing);
        js.execute(new String[]{ConfigUrlsAPI.user_retrieve_url, JsonParse.REQUEST_TYPE_GET});
    }

    public void getCountryList() {
        mProgressDialog = new ProgressDialog(mActivity);
        mProgressDialog.setMessage(mActivity.getString(R.string.please_wait));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_COUNTRIES_LIST, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.API_GET_USER_COUNTRIES, JsonParse.REQUEST_TYPE_GET});
    }

    public void updateUser(String countryCode) {
        mProgressDialog = new ProgressDialog(mActivity);
        mProgressDialog.setMessage(mActivity.getString(R.string.please_wait));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String authtok = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, authtok);
        client.AddParam(ConfigUrlsAPI.update_firstname, mEdtFirstName.getText()
                .toString());
        client.AddParam(ConfigUrlsAPI.update_lastname, mEdtLastName.getText()
                .toString());
        client.AddParam(ConfigUrlsAPI.update_dob, mEdtDateOfBirth.getText().toString());
        client.AddParam(ConfigUrlsAPI.update_country, countryCode.toString());
        client.AddParam(ConfigUrlsAPI.update_mobile, mEdtMobileNumber.getText().toString());
        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_UPDATE_USER, client, this);
        apiConnectTask.setShowDialog(false);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.user_update_url, JsonParse.REQUEST_TYPE_PUT});

    }

    @Override
    public <T> void onSuccess(T data, String method) {
        JSONObject result = (JSONObject) data;
        if ((result != null) && (result.length() > 0)) {
            switch (method) {
                case ConfigUrlsAPI.REQUEST_GET_USER_INFO:
                    try {
                        JSONObject obj = result.getJSONObject(ConfigUrlsAPI.PARA_USER);
                        mEmail = obj.getString(ConfigUrlsAPI.PARA_EMAIL);
                        mLastName = obj.getString(ConfigUrlsAPI.PARA_LAST_NAME);

                        mDateOfBirth = obj.getString(ConfigUrlsAPI.PARA_DATE_OF_BIRTH);
                        mFirstName = obj.getString(ConfigUrlsAPI.PARA_FIRST_NAME);
                        mCustomerId = obj.getString(ConfigUrlsAPI.PARA_CUSTOMER_ID);
                        mCurrency = obj.getString(ConfigUrlsAPI.PARA_CURRENCY);
                        mCountryCode = obj.getString(ConfigUrlsAPI.PARA_COUNTRY).toString();
                        mCountryPhoneCode = obj.get(ConfigUrlsAPI.PARA_COUNTRY_CODE).toString();
                        mMobileNumber = obj.getString(ConfigUrlsAPI.PARA_MOBILE);


                        mTvEmail.setText(mEmail);
                        mEdtDateOfBirth.setText(mDateOfBirth);
                        mEdtFirstName.setText(mFirstName);
                        mEdtLastName.setText(mLastName);
                        mEdtMobileNumber.setText(mMobileNumber);
                        for (int i = 0; i < countryResponseModels.size(); i++){
                            if (countryResponseModels.get(i).getKey().equals(mCountryCode)) {
                                mSpCountry.setSelection(i);
                                break;
                            }
                        }

                        values.setUserEmail(mEmail);
                        values.setUserLastname(mLastName);
                        values.setUserDob(mDateOfBirth);
                        values.setUserFirstname(mFirstName);
                        values.setCustomerId(mCustomerId);
                        values.setCurrency(mCurrency);
                        values.setUserCountry(mCountryCode);
                        values.setCountryCode(mCountryPhoneCode);
                        values.setUserMobile(mMobileNumber);

                        if (mUpdateInProcessing) {
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.SUCCESS, getString(R.string.update_profile_success)).show();
                            mUpdateInProcessing = false;
                        }
                    } catch (JSONException e) {
                        LogUtils.e("Exception is ", e.toString());
                    }
                    mProgressDialog.dismiss();
                    break;
                case ConfigUrlsAPI.REQUEST_UPDATE_USER:
                    LogUtils.e("result..............", "" + result);
                    try {
                        String status = result.getString(ConfigUrlsAPI.PARA_STATUS);
                        LogUtils.e("status.........", status);
                        if (status.equalsIgnoreCase(ConfigUrlsAPI.RESULT_SUCCESS)) {
                            Utils.clearCache(mActivity);
                            retrieveUserInfo(false);
                            mUpdateInProcessing = true;
                        } else {
                            DialogUtilities.getOkAlertDialog(mActivity, result.getString(ConfigUrlsAPI.PARA_MESSAGE)).show();
                        }
                    } catch (Exception e) {
                        mProgressDialog.dismiss();
                        DialogUtilities.getOkAlertDialog(mActivity, R.string.update_profile_fail).show();
                        LogUtils.e("Exception is", e.toString());
                    }
                    break;

                case ConfigUrlsAPI.REQUEST_GET_COUNTRIES_LIST:
                    JSONArray result_array1 = null;
                    try {
                        result_array1 = result.getJSONArray(ConfigUrlsAPI.PARA_COUNTRIES);
                        Gson gson = new Gson();
                        CountryResponseModel countryResponseModel;
                        String baseUrl, extension;
                        baseUrl = result.getJSONObject(ConfigUrlsAPI.PARA_FLAG).getString(ConfigUrlsAPI.PARA_BASE_URL);
                        extension = result.getJSONObject(ConfigUrlsAPI.PARA_FLAG).getString(ConfigUrlsAPI.PARA_EXTENSION);

                        for (int i = 0, size = result_array1.length(); i < size; i++) {
                            countryResponseModel = gson.fromJson(result_array1.get(i).toString(), CountryResponseModel.class);
                            countryResponseModel.setUrlImg(baseUrl + countryResponseModel.getKey() + FlobillerConstaints.DOT + extension);
                            countryResponseModel.setName(countryResponseModel.getValue());
                            countryResponseModels.add(countryResponseModel);
                        }
                        CountryWithFlagAdapter countryWithFlagAdapter = new CountryWithFlagAdapter(mActivity, R.layout.row_spinner_flag, countryResponseModels);
                        mSpCountry.setAdapter(countryWithFlagAdapter);
                        retrieveUserInfo(false);
                    } catch (JSONException e) {
                        mProgressDialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }


    @Override
    public void onFail(String method) {
        mProgressDialog.dismiss();

        AlertDialog alertDialog = DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network), new DialogUtilities.DialogCallBack() {
            @Override
            public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                getCountryList();
            }
        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                }
                return true;
            }
        });

        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (mEdtFirstName.getText().toString().length() < 1
                || mEdtLastName.getText().length() < 1
                || mEdtDateOfBirth.getText().length() < 1
                || mEdtMobileNumber.getText().toString().length() < 1) {
            Toast.makeText(mActivity,
                    getString(R.string.all_text_are_mandatory ), Toast.LENGTH_LONG)
                    .show();
        } else if (mEdtFirstName.getText().length() < 2) {
            Toast.makeText(mActivity,
                    getString(R.string.min_first_name),
                    Toast.LENGTH_LONG).show();
        } else if (mEdtLastName.getText().length() < 2) {
            Toast.makeText(mActivity,getString(R.string.min_last_name),
                    Toast.LENGTH_LONG).show();
        } else {
            updateUser(mCountryCode);
        }
        return true;
    }

    private void setLanguage() {
        SavedSharedPreference savedSharedPreference = new SavedSharedPreference(mActivity);
        String languageToLoad  = savedSharedPreference.getLanguage();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mActivity.getResources().updateConfiguration(config,
                mActivity.getResources().getDisplayMetrics());

    }
}
package com.app.flocash.appflobiller.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by ${binhpd} on 12/1/2015.
 */
public class SplashActivity extends Activity {

    private static final int SPLASH_SCREEN = 3100;
    private int delayTime = SPLASH_SCREEN;
    private static final String SPLASH_FILE = "splash.gif";

    @Bind(R.id.giv)
    GifImageView mGifImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        loadGif();
        if (savedInstanceState == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    SavedSharedPreference values = new SavedSharedPreference(SplashActivity.this);
                    // user hadn't been skip introduction
                    if (!values.isSawIntroduction()) {
                        Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                        startActivity(intent);
                        finish();
                        return;
                    }

                    // user had been skip introduction
                    if (TextUtils.isEmpty(values.getAuthToken())) {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }, delayTime);
        }
    }

    private void loadGif() {
        try {
            GifDrawable gifDrawable = new GifDrawable(getAssets(), SPLASH_FILE);
            delayTime = gifDrawable.getDuration();
            gifDrawable.setCallback(new Drawable.Callback() {
                @Override
                public void invalidateDrawable(@NonNull Drawable who) {

                }

                @Override
                public void scheduleDrawable(@NonNull Drawable who, @NonNull Runnable what, long when) {

                }

                @Override
                public void unscheduleDrawable(@NonNull Drawable who, @NonNull Runnable what) {

                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mGifImageView.setBackground(gifDrawable);
            } else {
                mGifImageView.setBackgroundDrawable(gifDrawable);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




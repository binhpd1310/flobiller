package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 1/12/2016.
 */
public class AppInfoFragment extends BaseFragment {

    @Bind(R.id.tvLink)
    TextView tvLink;

    @Bind(R.id.tvVersion)
    TextView mTvVersion;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_about_app;
    }

    @Override
    public void onCreateViewFragment(View view) {
//        tvLink.setText(Html.fromHtml("<a href=\\>https://flobiller.flocash.com</a>"));
        tvLink.setMovementMethod(LinkMovementMethod.getInstance());

        try {
            String versionName = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionName;
            int versionCode = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionCode;
            mTvVersion.setText(getString(R.string.version) + versionName + "." + versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.tvLink)
    public void onClick(View view) {
        Uri uriUrl = Uri.parse(getString(R.string.home_url));
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}

package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.models.GiftCard;
import com.app.flocash.appflobiller.models.GiftModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 5/24/2016.
 */
public class GiftCardAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<GiftCard> mGiftCards;
    private LayoutInflater mLayoutInflater;
    private OnSendGiftListener mOnSendGiftListener;

    public GiftCardAdapter(Context context, ArrayList<GiftCard> giftCards, OnSendGiftListener onSendGiftListener) {
        mGiftCards = giftCards;
        mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mOnSendGiftListener = onSendGiftListener;
    }

    @Override
    public int getCount() {
        return mGiftCards.size();
    }

    @Override
    public GiftCard getItem(int position) {
        return mGiftCards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_giftcard, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final GiftCard giftCard = mGiftCards.get(position);
        Picasso.with(mContext).load(giftCard.getLogo()).into(holder.ivLogo);
        holder.tvName.setText(giftCard.getName().toUpperCase());
        // set background
        holder.llBackground.setBackgroundResource(R.drawable.bg_thumb_round_blue);

        // set gifts list
        final String[] amounts = giftCard.getAmount().replace('|', ',').split(",");
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        holder.rvGift.setLayoutManager(layoutManager);
        ArrayList<GiftModel> giftModels = new ArrayList<>();
        for (int i = 0; i < amounts.length; i++) {
            giftModels.add(new GiftModel(amounts[i]));
        }
        GiftAdapter giftAdapter = new GiftAdapter(mContext, giftCard.getId(), giftCard.getName(), giftModels, mOnSendGiftListener);
        holder.rvGift.setAdapter(giftAdapter);

        return convertView;
    }

    public static class Holder {
        LinearLayout llBackground;
        ImageView ivLogo;
        TextView tvName;
        RecyclerView rvGift;

        public Holder (View view) {
            llBackground = (LinearLayout) view.findViewById(R.id.llBackground);
            ivLogo = (ImageView) view.findViewById(R.id.ivLogoGiftCard);
            tvName = (TextView) view.findViewById(R.id.tvGiftCard);
            rvGift = (RecyclerView) view.findViewById(R.id.rvGift);
        }
    }

    public interface OnSendGiftListener {
        void onSendGift(String id, String name, String amount);
    }
}

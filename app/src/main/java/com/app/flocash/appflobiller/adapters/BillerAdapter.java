package com.app.flocash.appflobiller.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.models.BillerItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by binh.pd on 12/10/2015.
 */
public class BillerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<BillerItem> gridBillers;
    private LayoutInflater inflater;

    public BillerAdapter(Activity context, ArrayList<BillerItem> gridBillers) {
        this.context = context;
        this.gridBillers = gridBillers;
        inflater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return gridBillers.size();
    }

    @Override
    public BillerItem getItem(int position) {
        return gridBillers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null){
            row = inflater.inflate(R.layout.item_thumbnail_billers, parent,false);
            holder = new ViewHolder();
            holder.rlFrameContent = (RelativeLayout)row.findViewById(R.id.rlFrameContent);
            holder.imageView = (ImageView)row.findViewById(R.id.ivLogo);
            holder.name = (TextView)row.findViewById(R.id.tvBillerName);
            holder.subscribe=(TextView)row.findViewById(R.id.tvSubscribe);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        BillerItem biller = gridBillers.get(position);
        if ((position % 2 == 0)) {
            holder.rlFrameContent.setBackgroundResource(R.drawable.bg_thumb_round_red);
            holder.name.setBackgroundResource(R.drawable.bg_round_title_thumb_red);
        } else {
            holder.rlFrameContent.setBackgroundResource(R.drawable.bg_thumb_round_blue);
            holder.name.setBackgroundResource(R.drawable.bg_round_title_thumb_blue);
        }
        holder.name.setText(biller.getName());
        holder.subscribe.setText(biller.getSubscribe());
        Picasso.with(context).load(biller.getImage()).into(holder.imageView);
        return row;
    }

    static class ViewHolder {
        RelativeLayout rlFrameContent;
        ImageView imageView;
        TextView name;
        TextView subscribe;
    }
}

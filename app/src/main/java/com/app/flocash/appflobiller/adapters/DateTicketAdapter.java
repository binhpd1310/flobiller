package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.EventTicketData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by ${binhpd} on 12/1/2016.
 */

public class DateTicketAdapter extends RecyclerView.Adapter<DateTicketAdapter.DateHolder> {

    private final ArrayList<ArrayList<EventTicketData>> eventTickets;
    private FlobillerInterfaces.OnItemClick onItemClick;
    private int selectedItem = -1;

    public DateTicketAdapter(Context context, ArrayList<ArrayList<EventTicketData>> eventTickets, FlobillerInterfaces.OnItemClick listener) {
        this.eventTickets = eventTickets;
        this.onItemClick = listener;
    }

    @Override
    public DateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_date, null);
        return new DateHolder(view);
    }

    @Override
    public void onBindViewHolder(final DateHolder holder, final int position) {
        EventTicketData eventTicketdData = eventTickets.get(position).get(0);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(FlobillerConstaints.ticketDateFormat.parse(eventTicketdData.getTicket_date()));
            holder.tvDay.setText(FlobillerConstaints.dayOfWeekFormat.format(calendar.getTime()));
            holder.tvDate.setText(FlobillerConstaints.dateFormat.format(calendar.getTime()));

            holder.vFocus.setVisibility(selectedItem == position?View.VISIBLE:View.GONE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedItem = position;
                    onItemClick.onItemClick(v, position);
                    notifyDataSetChanged();
                }
            });
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return eventTickets.size();
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
        notifyDataSetChanged();
    }

    public static class DateHolder extends RecyclerView.ViewHolder {
        TextView tvDay;
        TextView tvDate;
        View vFocus;
        public DateHolder(View itemView) {
            super(itemView);
            tvDay = (TextView) itemView.findViewById(R.id.tvDay);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            vFocus = itemView.findViewById(R.id.vFocus);
        }
    }

    public int getSelectedItem() {
        return selectedItem;
    }
}

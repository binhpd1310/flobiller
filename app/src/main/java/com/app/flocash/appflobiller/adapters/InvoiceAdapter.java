package com.app.flocash.appflobiller.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.fragments.InvoiceFragment;
import com.app.flocash.appflobiller.models.InvoiceItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 12/10/2015.
 */
public class InvoiceAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<InvoiceItem> listInvoices = new ArrayList<>();
    private LayoutInflater inflater;
    private OnSelectButtonInvoice onSelectButtonInvoice;
    public InvoiceAdapter(Context context, ArrayList<InvoiceItem> listInvoices) {
        this.context = context;
        this.listInvoices = listInvoices;
        this.inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public int getCount() {
        return listInvoices.size();
    }

    @Override
    public InvoiceItem getItem(int position) {
        return listInvoices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHoler holder;
        if (convertView==null){
            convertView = inflater.inflate(R.layout.row_invoice_layout, parent,false);
            holder = new ViewHoler();
            holder.customerName = (TextView)convertView.findViewById(R.id.custAccountName);
            holder.currency = (TextView)convertView.findViewById(R.id.currency);
            holder.tvStatus = (TextView)convertView.findViewById(R.id.tvStatus);
            holder.ivLogo = (ImageView) convertView.findViewById(R.id.ivLogo);
            holder.tvMarkAsRead = (TextView) convertView.findViewById(R.id.tvMarkAsRead);
            holder.tvPayNow = (TextView) convertView.findViewById(R.id.tvPaynow);
            holder.tvView = (TextView) convertView.findViewById(R.id.tvView);
            holder.llGroup = (LinearLayout) convertView.findViewById(R.id.rlGroup);
            convertView.setTag(holder);

        } else{
            holder = (ViewHoler) convertView.getTag();
        }
        InvoiceItem invoiceItem = listInvoices.get(position);
        holder.customerName.setText(invoiceItem.getName());
        holder.tvStatus.setText(invoiceItem.getGenerateDate());
        holder.currency.setText(invoiceItem.getCurrency() + " " + invoiceItem.getAmount());

        holder.tvMarkAsRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectButtonInvoice.onSelectButtonInvoice(OnSelectButtonInvoice.MARK_AS_READ, position);
            }
        });

        holder.tvPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectButtonInvoice.onSelectButtonInvoice(OnSelectButtonInvoice.MARK_PAY_NOW, position);
            }
        });

        holder.tvView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectButtonInvoice.onSelectButtonInvoice(OnSelectButtonInvoice.MARK_PAY_NOW, position);
            }
        });
        Picasso.with(context).load(invoiceItem.getUrl()).into(holder.ivLogo);

        String status = invoiceItem.getFlocashStatus();
        if (InvoiceFragment.STATUS_PAID.equals(status)) {
            holder.llGroup.setVisibility(View.GONE);
            holder.tvView.setVisibility(View.VISIBLE);
            holder.tvMarkAsRead.setEnabled(false);
            holder.tvPayNow.setEnabled(false);
        } else if (TextUtils.isEmpty(status) || InvoiceFragment.STATUS_NOT_PAID.equals(status)) {
            holder.llGroup.setVisibility(View.VISIBLE);
            holder.tvView.setVisibility(View.GONE);
            holder.tvMarkAsRead.setEnabled(true);
            holder.tvPayNow.setEnabled(true);
        } else if (InvoiceFragment.STATUS_PENDING.equals(status)) {
            holder.llGroup.setVisibility(View.GONE);
            holder.tvView.setVisibility(View.VISIBLE);
            holder.tvMarkAsRead.setEnabled(false);
            holder.tvPayNow.setEnabled(false);
        } else {
            holder.llGroup.setVisibility(View.INVISIBLE);
            holder.tvView.setVisibility(View.INVISIBLE);
        }


        return convertView;

    }

    public void setOnSelectButtonInvoice(OnSelectButtonInvoice onSelectButtonInvoice) {
        this.onSelectButtonInvoice = onSelectButtonInvoice;
    }

    static class ViewHoler{
        TextView customerName,currency;
        TextView tvStatus;
        ImageView ivLogo;
        TextView tvMarkAsRead;
        TextView tvPayNow;
        TextView tvView;
        LinearLayout llGroup;
    }

    public interface OnSelectButtonInvoice {
        int MARK_AS_READ = 0;
        int MARK_PAY_NOW = 1;
        void onSelectButtonInvoice(int button, int position);
    }
}

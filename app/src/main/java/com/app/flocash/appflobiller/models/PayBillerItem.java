package com.app.flocash.appflobiller.models;

/**
 * Created by ${binhpd} on 8/18/2016.
 */
public class PayBillerItem extends ItemListing {
    private String billerId;

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }
}

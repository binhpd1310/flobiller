package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.adapters.DateTicketAdapter;
import com.app.flocash.appflobiller.adapters.SpinnerAdapter;
import com.app.flocash.appflobiller.adapters.TimeTicketAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.DateModel;
import com.app.flocash.appflobiller.models.EventData;
import com.app.flocash.appflobiller.models.EventTicketData;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.Utils;
import com.app.flocash.appflobiller.views.CustomNumberPicker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 11/20/2016.
 */

public class EventDetailFragment  extends FloBillerBaseFragment implements FlobillerInterfaces.OnItemClick {

    private ArrayList<DateModel> dateModels = new ArrayList<>();

    private DateTicketAdapter mDateAdapter;

    private LinearLayoutManager mLinearLayoutManagerDate;

    private EventData mEventData;

    private ArrayList<ArrayList<EventTicketData>> mTicketEvents = new ArrayList<>();

    private ArrayList<EventTicketData> mTicketsResponse;

    private TimeTicketAdapter mTimeAdapter;

    private SpinnerAdapter mSpinnerAdapter;

    @Bind(R.id.ivPoster)
    ImageView mIvPoster;

    @Bind(R.id.tvEventTitle)
    TextView mTvEventTitle;

    @Bind(R.id.tvBookmark)
    ImageView mTvBookmark;

    @Bind(R.id.ivLocal)
    ImageView mIvLocal;

    @Bind(R.id.tvVenueName)
    TextView mTvVenueName;

    @Bind(R.id.tvVenueCity)
    TextView mTvVenueCity;

    @Bind(R.id.tvPrice)
    TextView mTvPrice;

    @Bind(R.id.tvShortDesc)
    TextView mTvShortDesc;

    @Bind(R.id.tvDescription)
    TextView mTvDescription;

    @Bind(R.id.tvMapCity)
    TextView mTvMapCity;

    @Bind(R.id.tvMapAddress)
    TextView mTvMapAddress;

    @Bind(R.id.btnOpenMap)
    TextView mBtnOpenMap;

    @Bind(R.id.rvDay)
    RecyclerView mRvDate;

    @Bind(R.id.rvTime)
    RecyclerView mRvTime;

    @Bind(R.id.npAmount)
    CustomNumberPicker mNpQuantity;

    @Bind(R.id.llTakeYourPick)
    LinearLayout mLlTakeYourPick;

    @Bind(R.id.llQuantity)
    View mLlQuantity;

    @Bind(R.id.tvNumberTicket)
    TextView mTvNumberTicket;

    @Bind(R.id.tvFeeValue)
    TextView mTvFeeValue;

    @Bind(R.id.tvPriceTicket)
    TextView mTvPriceTicket;

    @Bind(R.id.tvSubTotalValue)
    TextView mTvSubTotalValue;

    @Bind(R.id.tvTotalValue)
    TextView mTvTotalValue;

    @Bind(R.id.tvNotice)
    TextView mTvNote;

    @Bind(R.id.llEventTime)
    LinearLayout mLlEventTime;

    @Bind(R.id.btnBook)
    Button mBtnBook;

    private float mTotal = 0;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_event_detail;
    }

    @Override
    public void onCreateViewFragment(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String eventId = bundle.getString(FlobillerConstaints.KEY_EXTRA_EVENT_ID);
            String eventName = bundle.getString(FlobillerConstaints.KEY_EXTRA_EVENT_NAME);

            updateTitle(eventName);
            
            getEventDetail(eventId);
        }
    }

    @OnClick(R.id.btnBook)
    public void onClick(View view) {
        if (mTimeAdapter == null ||
                (mDateAdapter.getSelectedItem() == -1 && mTimeAdapter.getSelectedItem() == -1)) {
            Toast.makeText(mActivity, R.string.notify_fields_required, Toast.LENGTH_LONG).show();
            return;
        }
        bookQuantity(mEventData.getEvent_id());
    }

    private void bookQuantity(String eventId) {
        JsonParse jsonParse = new JsonParse<>(JsonObject.class);
        jsonParse.AddParam(ConfigUrlsAPI.Authentication_token, values.getAuthToken());
        jsonParse.AddParam(ConfigUrlsAPI.PARA_EVENT_ID, eventId);
        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_EVENT_QUANTITY, jsonParse, this);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_CONFIRM_QUANTITY, JsonParse.REQUEST_TYPE_POST});

    }

    private void getEventDetail(String eventId) {
        JsonParse jsonParse = new JsonParse<>(JsonObject.class);
        jsonParse.AddParam(ConfigUrlsAPI.Authentication_token, values.getAuthToken());
        jsonParse.AddParam(ConfigUrlsAPI.PARA_EVENT_ID, eventId);

        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_EVENT_BY_ID,
                jsonParse, this);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_GET_EVENT_BY_ID, JsonParse.REQUEST_TYPE_GET});
    }

    private void createInvoice(EventTicketData ticketEventData) {
        JsonParse jsonParse = new JsonParse(JSONObject.class);
        jsonParse.AddParam(ConfigUrlsAPI.Authentication_token, values.getAuthToken());
        jsonParse.AddParam(ConfigUrlsAPI.PARA_CREATE_EVENT_ID, ticketEventData.getEvent_id());
        jsonParse.AddParam(ConfigUrlsAPI.PARA_CREATE_TICKET_ID, ticketEventData.getTicket_id());
        jsonParse.AddParam("eventname", mEventData.getName());
        jsonParse.AddParam("eventcountries", mEventData.getCity());
        jsonParse.AddParam("eventemail", values.getUserEmail());
        jsonParse.AddParam("eventmobile", values.getCountryCode() + values.getUserMobile());
        jsonParse.AddParam("selectqty", mNpQuantity.getValue() + "");
        jsonParse.AddParam("eventcurrency", ticketEventData.getCurrency());
        jsonParse.AddParam("eventamount", String.valueOf(mTotal));

        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_ADD_INVOICE,
                jsonParse, this);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_EVENT_ADD_INVOICE, JsonParse.REQUEST_TYPE_POST});
    }

    @Override
    public void onSuccess(Object result, String method) {
        JSONObject jsonResult = (JSONObject) result;
        if (isHaveData(jsonResult)) {
            switch (method) {
                case ConfigUrlsAPI.REQUEST_GET_EVENT_BY_ID:
                    Gson gson = new Gson();
                    try {
                        JSONObject msg = jsonResult.getJSONObject(ConfigUrlsAPI.MSG_RESPONSE);
                        mEventData = gson.fromJson(
                                msg.getJSONArray(ConfigUrlsAPI.PARA_EVENT_DATA).get(0).toString(), EventData.class);
                        Type type = new TypeToken<ArrayList<EventTicketData>>(){}.getType();
                        mTicketsResponse = gson.fromJson(msg.getJSONArray(ConfigUrlsAPI.PARA_EVENT_TICKET_DATA).toString(), type);

                        bindEventInfo();
                        bindTicketEvent(mTicketsResponse);
                        bindValidTicket();
                        mBtnOpenMap.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String uri = String.format(Locale.ENGLISH, "geo:%s,%s", mEventData.getMap_lat(), mEventData.getMap_long());
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                                mActivity.startActivity(intent);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case ConfigUrlsAPI.REQUEST_EVENT_QUANTITY:
                    int selectedDate = mDateAdapter.getSelectedItem();
                    int selectedTime = mTimeAdapter.getSelectedItem();
                    EventTicketData selectedTicket = mTicketEvents.get(selectedDate).get(selectedTime);
                    createInvoice(selectedTicket);
                    break;

                case ConfigUrlsAPI.REQUEST_ADD_INVOICE:
                    try {
                        String status = jsonResult.getString(ConfigUrlsAPI.PARA_STATUS);
                        if (ConfigUrlsAPI.RESULT_SUCCESS.equalsIgnoreCase(status)) {
                            // direct to invoices
                            String invoiceId = jsonResult.getJSONObject(ConfigUrlsAPI.MSG_RESPONSE).getString(ConfigUrlsAPI.PAR_LOCAL_INVOICE);
                            Intent intent = new Intent(mActivity, SubScreenActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT);
                            bundle.putString(FlobillerConstaints.KEY_EXTRA_ORDER_ID, invoiceId);
                            intent.putExtras(bundle);
                            startActivity(intent);

                            mActivity.finish();
                        }  else if (ConfigUrlsAPI.result_error.equalsIgnoreCase(status)) {
                            String errMsg = jsonResult.getString(ConfigUrlsAPI.PARA_MESSAGE);
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, errMsg).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * auto find valid ticket and show it
     */
    private void bindValidTicket() {
        int validDate = -1;
        int validTime = -1;
        for (int i = 0; i < mTicketEvents.size(); i++) {
            if (validDate != -1) {
                break;
            }

            ArrayList<EventTicketData> ticketEventDatas = mTicketEvents.get(i);
            for (int j = 0; j < ticketEventDatas.size(); j++) {
                EventTicketData ticketEventData = ticketEventDatas.get(j);
                if (Integer.valueOf(ticketEventData.getQuantity()) > 0) {
                    validDate = i;
                    validTime = j;
                    break;
                }
            }
        }

        if (validDate == -1) {
            bindNoValidTicket();
            return;
        }

        mLinearLayoutManagerDate.scrollToPosition(validDate);
        mDateAdapter.setSelectedItem(validDate);

        bindTimeAdapter();
        mTimeAdapter.setSelectedItem(validTime);

        EventTicketData ticketEventData = mTicketEvents.get(validDate).get(validTime);
        int quantity = Integer.valueOf(ticketEventData.getQuantity());
        bindPriceUI(ticketEventData, 1);
        initSpinner(quantity);
    }

    private void bindNoValidTicket() {
        mLlEventTime.setVisibility(View.GONE);
        mTvNote.setVisibility(View.VISIBLE);
        mBtnBook.setEnabled(false);
    }

    private void bindPriceUI(EventTicketData ticketEventData, int quantity) {
        mTvNumberTicket.setText(String.valueOf(quantity));
        mTvFeeValue.setText(ticketEventData.getCommission());
        mTvPriceTicket.setText(ticketEventData.getAmount() + " " + ticketEventData.getCurrency());
        mTvSubTotalValue.setText(String.valueOf(
                (Float.valueOf(ticketEventData.getAmount()) + Float.valueOf(ticketEventData.getCommission())))
                + " " + ticketEventData.getCurrency());
        mTotal = (Float.valueOf(ticketEventData.getAmount()) + Float.valueOf(ticketEventData.getCommission())) * quantity;
        mTvTotalValue.setText(
                String.valueOf(mTotal) + " " + ticketEventData.getCurrency());
    }

    private void bindEventInfo() {
        Picasso.with(mActivity).load(mEventData.getPoster()).into(mIvPoster);
        mTvEventTitle.setText(mEventData.getName());
        mTvVenueName.setText(mEventData.getAddress() + ", " + mEventData.getCity());
        mTvVenueCity.setText(mEventData.getCity());

        mTvShortDesc.setText(Utils.getTextFromHtml(mEventData.getShort_desc()));
        mTvDescription.setText(Utils.getTextFromHtml(mEventData.getDesc()));
        mTvMapCity.setText(mEventData.getCity());
        mTvMapAddress.setText(mEventData.getAddress());
    }

    private void bindTicketEvent(ArrayList<EventTicketData> eventsTicketData) {
        mTicketEvents.clear();
        for (EventTicketData node : eventsTicketData) {
            boolean isExists = false;
            for (ArrayList<EventTicketData> ticketsEventData : mTicketEvents) {
                if (compare(ticketsEventData.get(0).getTicket_date(), node.getTicket_date())) {
                    addMore(node, ticketsEventData);
                    addMore(node, ticketsEventData);
                    isExists = true;
                }
            }

            if (!isExists) {
                addNew(node);
            }
        }
        mDateAdapter = new DateTicketAdapter(mActivity, mTicketEvents, this);
        mLinearLayoutManagerDate = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mRvDate.setLayoutManager(mLinearLayoutManagerDate);
        mRvDate.setAdapter(mDateAdapter);

    }

    private void initSpinner(int total) {
        mLlQuantity.setVisibility(total > 0 ? View.VISIBLE:View.GONE);
        mNpQuantity.setMaxValue(total);
        mNpQuantity.setMinValue(1);
        mNpQuantity.setValue(1);
        mNpQuantity.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                EventTicketData ticketEventData = getTicket();
                bindPriceUI(ticketEventData, newVal);
            }
        });
    }

    private EventTicketData getTicket() {
        return mTicketEvents.get(mDateAdapter.getSelectedItem()).get(mTimeAdapter.getSelectedItem());
    }

    private void addMore(EventTicketData node, ArrayList<EventTicketData> eventTicketdDatas) {
        for (int i = 0; i < eventTicketdDatas.size(); i++) {
            if (node.getTicket_date().compareTo(eventTicketdDatas.get(i).getTicket_date()) <= 0) {
                eventTicketdDatas.add(i, node);
                break;
            }
        }
    }

    private void addNew(EventTicketData node) {
        ArrayList<EventTicketData> eventTicketDatas = new ArrayList<>();
        eventTicketDatas.add(node);
        mTicketEvents.add(eventTicketDatas);
    }

    private boolean compare(String dateTicket, String ticket_date) {
        return dateTicket.substring(0, 10).equals(ticket_date.substring(0, 10));
    }

    @Override
    public void onItemClick(View view, final int indexDay) {
        bindTimeAdapter();
    }

    private void bindTimeAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mLlTakeYourPick.setVisibility(View.VISIBLE);
        mLlQuantity.setVisibility(View.GONE);
        initSpinner(0);

        mTimeAdapter = new TimeTicketAdapter(mTicketEvents.get(mDateAdapter.getSelectedItem()), new FlobillerInterfaces.OnItemClick() {
            @Override
            public void onItemClick(View view, int index) {
                try {
                    EventTicketData ticketEventData = mTicketEvents.get(mDateAdapter.getSelectedItem()).get(index);
                    int quantity = Integer.valueOf(ticketEventData.getQuantity());
                    bindPriceUI(ticketEventData, quantity);
                    initSpinner(quantity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mRvTime.setLayoutManager(linearLayoutManager);
        mRvTime.setAdapter(mTimeAdapter);
    }
}

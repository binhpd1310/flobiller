package com.app.flocash.appflobiller.dao;

import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.models.GsonObject;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by TuanMobile on 11/12/2015.
 */
public interface IFloCashAPI {

    @FormUrlEncoded
    @POST("/user")
    void registerUser(
            @Field("firstname") String first_name,
            @Field("lastname") String last_name,
            @Field("dateofbirth") String date_of_birth,
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("country") String country,
            @Field("password") String password,
            @Field("cpassword") String confirm_password,
            Callback<GsonObject.GsonRegisterResult> callback
    );

    @FormUrlEncoded
    @POST("/login")
    void login(
            @Field(ConfigUrlsAPI.user_email) String userEmail,
            @Field(ConfigUrlsAPI.user_password) String userPassword,
            Callback<GsonObject.GsonRegisterResult> callback
    );
}
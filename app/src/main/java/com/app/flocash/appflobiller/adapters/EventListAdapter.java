package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.EventModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 11/19/2016.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventHolder> {
    private FlobillerInterfaces.OnItemClick mListener;;
    private ArrayList<EventModel> mEventModels;
    private Context mContext;

    public EventListAdapter(Context context, ArrayList<EventModel> eventModels, FlobillerInterfaces.OnItemClick listener) {
        mContext = context;
        this.mListener = listener;
        this.mEventModels = eventModels;
    }
    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_event, null);
        EventHolder holder = new EventHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EventHolder holder, final int position) {
        try {
            EventModel item = mEventModels.get(position);
            Picasso.with(mContext).load(item.getPoster()).into(holder.ivPoster);

            holder.tvEventTitle.setText(item.getName());
            holder.tvVenueName.setText(item.getAddress() + ", " + item.getLocation());
            holder.tvVenueCity.setText(item.getCity());
            holder.tvPrice.setText(mContext.getString(R.string.from) + " " + item.getAmount() + " " + item.getCurrency());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(v, position);
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mEventModels.size();
    }

    public class EventHolder extends RecyclerView.ViewHolder {
        public ImageView ivPoster;
        public TextView tvEventTitle;
        public TextView tvVenueName;
        public TextView tvVenueCity;
        public TextView tvPrice;

        public EventHolder(View itemView) {
            super(itemView);
            ivPoster = (ImageView) itemView.findViewById(R.id.ivPoster);
            tvEventTitle = (TextView) itemView.findViewById(R.id.tvEventTitle);
            tvVenueName = (TextView) itemView.findViewById(R.id.tvVenueName);
            tvVenueCity = (TextView) itemView.findViewById(R.id.tvVenueCity);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
        }
    }
}

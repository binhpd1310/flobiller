package com.app.flocash.appflobiller.models;

/**
 * Created by ${binhpd} on 5/24/2016.
 */
public class GiftCard {
    private String id;
    private String name;
    private String logo;
    private String amount;

    public GiftCard(String id, String name, String logo, String amount) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}

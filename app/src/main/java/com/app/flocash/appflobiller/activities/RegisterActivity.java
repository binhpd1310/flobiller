package com.app.flocash.appflobiller.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.CountryWithFlagAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.dao.APIManager;
import com.app.flocash.appflobiller.models.CountryResponseModel;
import com.app.flocash.appflobiller.models.GsonObject;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.KeyboardUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;
import com.app.flocash.appflobiller.utilities.Utils;
import com.app.flocash.appflobiller.utilities.ValidationUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by TuanMobile on 11/12/2015.
 */
public class RegisterActivity extends BaseActivity {

    @Bind(R.id.edtFirstName)
    EditText mEdtFirstName;
    @Bind(R.id.edtLastName)
    EditText mEdtLastName;
    @Bind(R.id.edtEmail)
    EditText mEdtEmail;
    @Bind(R.id.edtMobile)
    EditText mEdtMobile;
    @Bind(R.id.edtPassword)
    EditText mEdtPassword;
    @Bind(R.id.edtConfirmPassword)
    EditText mEdtConfirmPassword;
    @Bind(R.id.spCountry)
    Spinner mSpCountry;
    @Bind(R.id.tvPostCountry)
    TextView mTvPostCountry;

    private String mEmailAddress;
    private String mFirstName, mLastName, mEmail, mMobile, mPassWord, mConfirmPassword, mKeyCountry = "AF", mCode = "+93";
    private ProgressDialog mDialog;
    private SavedSharedPreference mSavedSharedPreference;
    private ArrayList<CountryResponseModel> mCountryList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedSharedPreference = new SavedSharedPreference(RegisterActivity.this);
        setContentView(R.layout.activity_cutomer_register);
        ButterKnife.bind(this);
        initView();
        KeyboardUtils.hideKeyboard(this, getWindow().getDecorView().findViewById(android.R.id.content));
        receiveCountriesList();
    }

    private void initView() {
        KeyboardUtils.hideKeyboard(RegisterActivity.this);

        //suggestion e-mail
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Account[] accounts = AccountManager.get(RegisterActivity.this).getAccounts();
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    mEmailAddress = account.name;
                    break;
                }
            }
            if (!TextUtils.isEmpty(mEmailAddress)) {
                mEdtEmail.setText(mEmailAddress);
            }
        }


        mEdtConfirmPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    register();
                }
                return false;
            }
        });

        //Initialize progress dialog login
        mDialog = new ProgressDialog(RegisterActivity.this);
        mDialog.setMessage(getString(R.string.notify_please_wait));
        mDialog.setCancelable(false);
    }

    private void receiveCountriesList() {
        JsonParse client = new JsonParse(JsonObject.class);
        ApiConnectTask apiConnectTask = new ApiConnectTask(this, ConfigUrlsAPI.API_GET_COUNTRY_FLOBILLER, client, RegisterActivity.this);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_GET_COUNTRY_FLOBILLER, JsonParse.REQUEST_TYPE_GET});
    }

    @Override
    public void onSuccess(Object result, String method) {
        JSONObject response = (JSONObject) result;
        switch (method) {
            case ConfigUrlsAPI.API_GET_COUNTRY_FLOBILLER:
                JSONArray countryJsonArray = null;
                try {
                    mCountryList = new ArrayList<>();
                    countryJsonArray = response.getJSONArray(ConfigUrlsAPI.PARA_COUNTRIES);
                    Gson gson = new Gson();
                    CountryResponseModel countryResponseModel;
                    String baseUrl, extension;
                    baseUrl = response.getJSONObject(ConfigUrlsAPI.PARA_FLAG).getString(ConfigUrlsAPI.PARA_BASE_URL);
                    extension = response.getJSONObject(ConfigUrlsAPI.PARA_FLAG).getString(ConfigUrlsAPI.PARA_EXTENSION);

                    for (int i = 0, size = countryJsonArray.length(); i < size; i++) {
                        countryResponseModel = gson.fromJson(countryJsonArray.get(i).toString(), CountryResponseModel.class);
                        countryResponseModel.setUrlImg(baseUrl + countryResponseModel.getKey() + FlobillerConstaints.DOT + extension);
                        countryResponseModel.setName(countryResponseModel.getValue());
                        mCountryList.add(countryResponseModel);
                    }
                    CountryWithFlagAdapter countryWithFlagAdapter = new CountryWithFlagAdapter(RegisterActivity.this, R.layout.row_spinner_flag, mCountryList);
                    mSpCountry.setAdapter(countryWithFlagAdapter);
                    mSpCountry.setOnItemSelectedListener(new CustomOnItemSelectedListener());

                    //suggestion country
                    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    String countryCodeValue = tm.getNetworkCountryIso().toUpperCase();
                    if (!TextUtils.isEmpty(countryCodeValue)) {
                        mSpCountry.setSelection(getIndexCountry(countryCodeValue));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFail(String method) {

    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            CountryResponseModel countryModel = mCountryList.get(pos);
            mKeyCountry = countryModel.getKey();
            mCode = countryModel.getCode();
            mTvPostCountry.setText(mCode);
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    }

    private int getIndexCountry(String pCountryISO) {
        for (int i = 0, size = mCountryList.size(); i < size; i++) {
            if (mCountryList.get(i).getKey().equalsIgnoreCase(pCountryISO)) {
                return i;
            }
        }

        return 0;
    }

    @OnClick({R.id.ivBack, R.id.btnRegister})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.btnRegister:
                register();
                break;
        }
    }

    private void register() {
        mFirstName = mEdtFirstName.getText().toString().trim();
        mLastName = mEdtLastName.getText().toString().trim();
        mEmail = mEdtEmail.getText().toString().trim();
        mMobile = mEdtMobile.getText().toString().trim();
        mPassWord = mEdtPassword.getText().toString().trim();
        mConfirmPassword = mEdtConfirmPassword.getText().toString().trim();
        validateRegister();
    }

    private void validateRegister() {
        if (TextUtils.isEmpty(mFirstName) || TextUtils.isEmpty(mLastName) || TextUtils.isEmpty(mEmail) || TextUtils.isEmpty(mMobile) ||
                TextUtils.isEmpty(mPassWord) || TextUtils.isEmpty(mConfirmPassword)) {
            Toast.makeText(getApplicationContext(), getString(R.string.notify_fields_required), Toast.LENGTH_SHORT).show();
        } else if (!ValidationUtils.validateUserName(mFirstName)) {
            Toast.makeText(getApplicationContext(), getString(R.string.notify_first_name_only_alphabets), Toast.LENGTH_SHORT).show();
        } else if (!ValidationUtils.validateUserName(mLastName)) {
            Toast.makeText(getApplicationContext(), getString(R.string.notify_last_name_only_alphabets), Toast.LENGTH_SHORT).show();
        } else if (!ValidationUtils.isEmailValid(mEmail)) {
            Toast.makeText(getApplicationContext(), getString(R.string.notify_email_not_valid), Toast.LENGTH_SHORT).show();
//        } else if (mPassWord.length() < 8) {
//            Toast.makeText(getApplicationContext(), getString(R.string.notify_pass_minimum), Toast.LENGTH_SHORT).show();
        } else if (!mPassWord.equals(mConfirmPassword)) {
            Toast.makeText(getApplicationContext(), getString(R.string.notify_pass_not_match), Toast.LENGTH_SHORT).show();
        } else if (!Utils.checkNetwork(RegisterActivity.this)) {
            Toast.makeText(getApplicationContext(), getString(R.string.notify_error_network), Toast.LENGTH_SHORT).show();
        } else {
            mDialog.show();
            APIManager.getAPIServices().registerUser(mFirstName, mLastName, "1987-09-29", mEmail, mMobile, mKeyCountry, mPassWord, mConfirmPassword, new Callback<GsonObject.GsonRegisterResult>() {
                @Override
                public void success(GsonObject.GsonRegisterResult gsonRegisterResult, Response response) {
                    if (gsonRegisterResult != null) {
                        String message = gsonRegisterResult.message;
                        Log.e("mMessage: ", "" + message);
                        mDialog.dismiss();
                        DialogUtilities.getOkAlertDialogColor(RegisterActivity.this, DialogUtilities.SUCCESS, message, new DialogUtilities.DialogCallBack() {
                            @Override
                            public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }).show();
//                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//                        startActivity(intent);

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("RetrofitError register", error.getLocalizedMessage());
                    mDialog.dismiss();
                    DialogUtilities.getOkAlertDialogColor(RegisterActivity.this, DialogUtilities.FAIL, error.getLocalizedMessage(), new DialogUtilities.DialogCallBack() {
                        @Override
                        public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
//                            finish();
                        }
                    }).show();

                }
            });
        }
    }
}

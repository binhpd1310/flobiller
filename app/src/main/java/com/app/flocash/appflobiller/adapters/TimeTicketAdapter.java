package com.app.flocash.appflobiller.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.EventTicketData;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by ${binhpd} on 12/12/2016.
 */

public class TimeTicketAdapter extends RecyclerView.Adapter<TimeTicketAdapter.TimeHolder> {

    private ArrayList<EventTicketData> ticketsEvent;
    private FlobillerInterfaces.OnItemClick onItemClick;
    private int selectedItem = -1;

    public TimeTicketAdapter(ArrayList<EventTicketData> eventsTicketData, FlobillerInterfaces.OnItemClick onItemClick) {
        this.ticketsEvent = eventsTicketData;
        this.onItemClick = onItemClick;
    }

    @Override
    public TimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time, null);
        return new TimeHolder(view);
    }

    @Override
    public void onBindViewHolder(final TimeHolder holder, final int position) {
        EventTicketData ticketEventData = ticketsEvent.get(position);
        try {
            holder.tvTime.setText(FlobillerConstaints.timeFormat.format(FlobillerConstaints.ticketDateFormat.parse(ticketEventData.getTicket_date())));
            if (Integer.valueOf(ticketEventData.getQuantity()) > 0) {
                holder.tvTime.setBackgroundResource(selectedItem == position ?
                        R.drawable.round_available_selected : R.drawable.round_available);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedItem = position;
                        onItemClick.onItemClick(holder.itemView, position);
                        notifyDataSetChanged();
                    }
                });
            } else {
                holder.itemView.setBackgroundResource(R.drawable.round_red);
                holder.itemView.setOnClickListener(null);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return ticketsEvent.size();
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
        notifyDataSetChanged();
    }

    public static class TimeHolder extends RecyclerView.ViewHolder {

        public TextView tvTime;

        public TimeHolder(View itemView) {
            super(itemView);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        }
    }

    public int getSelectedItem() {
        return selectedItem;
    }
}

package com.app.flocash.appflobiller.dao;

import android.util.Base64;

import com.app.flocash.appflobiller.models.ErrorResponse;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by TuanMobile on 11/12/2015.
 */
public class APIManager {

    private static IFloCashAPI sIFloCashAPI;

    public static IFloCashAPI getAPIServices() {
        if (sIFloCashAPI == null) {
            // concatenate username and password with colon for authentication
            String credentials = "flobiller" + ":" + "flo@12345";
            // create Base64 encodet string
            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://flobiller.flocash.com/api/").setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Authorization", basic);
                    request.addHeader("Acceppt", "application/json");
                }
            })
                    .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                    .setErrorHandler(new CustomErrorHandler())
                    .build();
            sIFloCashAPI = restAdapter.create(IFloCashAPI.class);
        }
        return sIFloCashAPI;
    }

    private static class CustomErrorHandler implements ErrorHandler {

        public CustomErrorHandler() {
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            String errorDescription;

            if (cause.isNetworkError()) {
                errorDescription = "Error connection!";
            } else {
                if (cause.getResponse() == null) {
                    errorDescription = "No response";
                } else {
                    // Error message handling - return a simple error to Retrofit handlers..
                    try {
                        ErrorResponse errorResponse = (ErrorResponse) cause.getBodyAs(ErrorResponse.class);
                        errorDescription = errorResponse.getMsg();
                    } catch (Exception ex) {
                        try {
                            errorDescription = " Error code: " + cause.getResponse().getStatus();
                        } catch (Exception ex2) {
//                            Log.e(TAG, "handleError: " + ex2.getLocalizedMessage());
                            errorDescription = "unknown error";
                        }
                    }
                }
            }

            return new Exception(errorDescription);
        }
    }
}
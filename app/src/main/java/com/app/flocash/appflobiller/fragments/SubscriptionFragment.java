package com.app.flocash.appflobiller.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.adapters.SubscriptionAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.models.SubscriptionItem;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by binh.pd on 12/10/2015.
 */
public class SubscriptionFragment extends FloBillerBaseFragment implements SubscriptionAdapter.ClickItemButtonListener {
    private static final String TAG = SubscriptionFragment.class.getSimpleName();
    @Bind(R.id.gvBillers)
    GridView mGridView;

    @Bind(R.id.btnDirectSubcription)
    Button mBtnDirectSubcription;

    @Bind(R.id.llDoNotHaveSubscription)
    LinearLayout llDoNotHaveSubscription;

    private SubscriptionAdapter listAdapter;
    private ArrayList<SubscriptionItem> subscriptionItems;
    ArrayList<String> subscriptionIds = new ArrayList<String>();

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_billers;
    }

    @Override
    public void onCreateViewFragment(View view) {
        subscriptionItems = new ArrayList<>();
        retrieve_subscriptions();
    }

    public void retrieve_subscriptions() {
        String token = values.getAuthToken();
        LogUtils.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(String.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_SUBSCRIPTION, client, this);
        js.execute(new String[]{ConfigUrlsAPI.subscribe_url, JsonParse.REQUEST_TYPE_GET});
    }

    public void removeSubscription(String id) {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.REMOVE__SUBSCRIPTION_ID, id);
        ApiConnectTask js_countries = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_REMOVE_SUBSCRIPTION, client, this);
        js_countries.setMessageDialog(getString(R.string.remove_subscription));
        js_countries.execute(new String[]{ConfigUrlsAPI.API_REMOVE_SUBSCRIPTION_URL, JsonParse.REQUEST_TYPE_DELETE});
    }

    @OnClick({R.id.btnDirectSubcription})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDirectSubcription:
                Intent intent = new Intent(mActivity, SubScreenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_BILLER);
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
                mActivity.finish();
                break;
        }
    }
    @Override
    public <T> void onSuccess(T data, String method) {
        switch (method) {
            case ConfigUrlsAPI.REQUEST_GET_SUBSCRIPTION:
                String result = (String) data;
                result = result.replaceAll("^\"|\"$", "");
                result = result.replaceAll("\\\\", "");
                if ((result != null) && (result.length() > 0)) {
                    try {
//                        LogUtils.e("result", result);
                        subscriptionItems.clear();
                        JSONObject jsonResponse = new JSONObject(result);
                        JSONArray jsonMainNode = jsonResponse.getJSONArray("subscriptions");
                        SubscriptionItem item;
                        for (int i = 0; i < jsonMainNode.length(); i++) {
                            JSONObject jsonChildNode = jsonMainNode.optJSONObject(i);
                            String billername = jsonChildNode.getString(ConfigUrlsAPI.PARA_BILLER_NAME);
                            String billerlogo = jsonChildNode.getString(ConfigUrlsAPI.PARA_BILLER_LOGO);
                            String serviceId = jsonChildNode.getString(ConfigUrlsAPI.PARA_SERVICE_ID);
                            subscriptionIds.add(serviceId);
                            String invoices = "View Invoices";
                            item = new SubscriptionItem();
                            item.setName(billername);
                            item.setLogo("https://flobiller.flocash.com/assets/images/upload/" + billerlogo);
                            item.setInvoices(invoices);
                            subscriptionItems.add(item);
                        }
                        listAdapter = new SubscriptionAdapter(mActivity, subscriptionItems, this);
                        mGridView.setAdapter(listAdapter);
                    } catch (JSONException e) {
                        LogUtils.e("Exception is", e.toString());
                    }
                } else {
                    // do not subcrible any things
                    llDoNotHaveSubscription.setVisibility(View.VISIBLE);
                    mGridView.setVisibility(View.GONE);
                    mBtnDirectSubcription.setVisibility(View.VISIBLE);
                }
                break;
            case ConfigUrlsAPI.REQUEST_REMOVE_SUBSCRIPTION:
                try {
                    final JSONObject resultJson = (JSONObject) data;
                    if (resultJson != null && resultJson.length() > 0) {
                        String status = resultJson.getString(ConfigUrlsAPI.PARA_STATUS);
                        if (status.equalsIgnoreCase(ConfigUrlsAPI.RESULT_SUCCESS)) {
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.SUCCESS, resultJson.getString(ConfigUrlsAPI.PARA_MESSAGE), new DialogUtilities.DialogCallBack() {
                                @Override
                                public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                                    retrieve_subscriptions();
                                }
                            }).show();

                        } else if (ConfigUrlsAPI.result_error.equalsIgnoreCase(status)) {
                            String errMsg = resultJson.getString(ConfigUrlsAPI.PARA_MESSAGE);
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, errMsg).show();
                        }
                    }
                } catch (JSONException e) {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFail(String method) {
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }

    @Override
    public void onClickItem(final int index, int buttonId) {
        Intent intent;
        Bundle bundle;
        switch (buttonId) {
            case  SubscriptionAdapter.ClickItemButtonListener.VIEW_INVOICE:
                String sub_id = subscriptionIds.get(index);
                values.setSubId(sub_id);
                intent = new Intent(mActivity, SubScreenActivity.class);
                bundle = new Bundle();
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_INVOICES);
                bundle.putString(FlobillerConstaints.KEY_EXTRA_INVOICE_ITEM, sub_id);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case SubscriptionAdapter.ClickItemButtonListener.EDIT_SUBSCRIPTION:
                SubscriptionItem subscriptionItem = subscriptionItems.get(index);
                intent = new Intent(mActivity, SubScreenActivity.class);
                bundle = new Bundle();
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_UPDATE_SUBCRIBLE);
                bundle.putString(FlobillerConstaints.KEY_EXTRA_URL, subscriptionItem.getLogo());
                bundle.putString(FlobillerConstaints.KEY_EXTRA_NANE, subscriptionItem.getName());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case SubscriptionAdapter.ClickItemButtonListener.REMOVE_SUBSCRIPTION:
                DialogUtilities.getOkCancelAlertDialog(mActivity, R.string.msg_confirm_remove_subscription, new DialogUtilities.DialogCallBack() {
                    @Override
                    public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                        if (pDialogType == pAlertDialog.BUTTON_POSITIVE) {
                            removeSubscription(subscriptionIds.get(index));
                        }
                    }
                }).show();
                break;
        }
    }
}

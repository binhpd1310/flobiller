package com.app.flocash.appflobiller.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ${binhpd} on 12/10/2015.
 */
public class InvoiceItem implements Parcelable {

    String invoiceId;
    String billerInvoiceid;
    String name;
    String currency;
    String amount;
    String serviceId;
    String generateDate;
    String lastPayDate;
    String custEmail;
    String custMobile;
    String custAccountName;
    String custAccountNumber;
    String custTopup;
    String flocashEmail;
    String flocashStatus;
    String flocashTransId;
    String flocashPaidDate;
    String api;
    String api_status;
    String url;

    public InvoiceItem(){

    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getCustAccountName() {
        return custAccountName;
    }

    public void setCustAccountName(String custAccountName) {
        this.custAccountName = custAccountName;
    }

    public String getGenerateDate() {
        return generateDate;
    }

    public void setGenerateDate(String generateDate) {
        this.generateDate = generateDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBillerInvoiceid() {
        return billerInvoiceid;
    }

    public void setBillerInvoiceid(String billerInvoiceid) {
        this.billerInvoiceid = billerInvoiceid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getLastPayDate() {
        return lastPayDate;
    }

    public void setLastPayDate(String lastPayDate) {
        this.lastPayDate = lastPayDate;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustAccountNumber() {
        return custAccountNumber;
    }

    public void setCustAccountNumber(String custAccountNumber) {
        this.custAccountNumber = custAccountNumber;
    }

    public String getCustTopup() {
        return custTopup;
    }

    public void setCustTopup(String custTopup) {
        this.custTopup = custTopup;
    }

    public String getFlocashEmail() {
        return flocashEmail;
    }

    public void setFlocashEmail(String flocashEmail) {
        this.flocashEmail = flocashEmail;
    }

    public String getFlocashStatus() {
        return flocashStatus;
    }

    public void setFlocashStatus(String flocashStatus) {
        this.flocashStatus = flocashStatus;
    }

    public String getFlocashTransId() {
        return flocashTransId;
    }

    public void setFlocashTransId(String flocashTransId) {
        this.flocashTransId = flocashTransId;
    }

    public String getFlocashPaidDate() {
        return flocashPaidDate;
    }

    public void setFlocashPaidDate(String flocashPaidDate) {
        this.flocashPaidDate = flocashPaidDate;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getApi_status() {
        return api_status;
    }

    public void setApi_status(String api_status) {
        this.api_status = api_status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(invoiceId);
        dest.writeString(billerInvoiceid);
        dest.writeString(name);
        dest.writeString(currency);
        dest.writeString(amount);
        dest.writeString(serviceId);
        dest.writeString(generateDate);
        dest.writeString(lastPayDate);
        dest.writeString(custEmail);
        dest.writeString(custMobile);
        dest.writeString(custAccountName);
        dest.writeString(custAccountNumber);
        dest.writeString(custTopup);
        dest.writeString(flocashEmail);
        dest.writeString(flocashStatus);
        dest.writeString(flocashTransId);
        dest.writeString(flocashPaidDate);
        dest.writeString(api);
        dest.writeString(api_status);
        dest.writeString(url);
    }

    public static final Parcelable.Creator<InvoiceItem> CREATOR = new Creator<InvoiceItem>() {
        public InvoiceItem createFromParcel(Parcel source) {
            InvoiceItem folder = new InvoiceItem();
            folder.setInvoiceId(source.readString());
            folder.setBillerInvoiceid(source.readString());
            folder.setName(source.readString());
            folder.setCurrency(source.readString());
            folder.setAmount(source.readString());
            folder.setServiceId(source.readString());
            folder.setGenerateDate(source.readString());
            folder.setLastPayDate(source.readString());
            folder.setCustEmail(source.readString());
            folder.setCustMobile(source.readString());
            folder.setCustAccountName(source.readString());
            folder.setCustAccountNumber(source.readString());
            folder.setCustTopup(source.readString());
            folder.setFlocashEmail(source.readString());
            folder.setFlocashStatus(source.readString());
            folder.setFlocashTransId(source.readString());
            folder.setFlocashPaidDate(source.readString());
            folder.setApi(source.readString());
            folder.setApi_status(source.readString());
            folder.setUrl(source.readString());
            return folder;
        }

        public InvoiceItem[] newArray(int size) {
            return new InvoiceItem[size];
        }
    };
}
package com.app.flocash.appflobiller.fragments;

import android.app.Dialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.flocash.core.service.entity.CardInfo;
import com.flocash.core.service.entity.Request;
import com.flocash.core.service.entity.Response;
import com.flocash.sdk.tasks.CreateTokenCard;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 5/27/2017.
 */

public class EnterCardDetailFragment extends BaseFragment {

    private static final int MIN_LENGTH_CARD_NUMBER = 16;
    private static final int MAX_LENGTH_SECURITY = 3;

    @Bind(R.id.edtNameOnCard)
    EditText mEdtNameOnCard;

    @Bind(R.id.edtCardNumber)
    EditText mEdtCardNumber;

    @Bind(R.id.tvExpiryDate)
    TextView mTvExpiryDate;

    @Bind(R.id.edtSecurityCode)
    EditText mTvSecurityCode;

    private String mMonth;
    private String mYear;


    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_enter_card_detail;
    }

    @Override
    public void onCreateViewFragment(View view) {

    }

    @OnClick({R.id.ibSave, R.id.tvExpiryDate})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibSave:
                if (valid(mEdtNameOnCard.getText().toString(), mEdtCardNumber.getText().toString(),
                        mTvExpiryDate.getText().toString(), mTvSecurityCode.getText().toString())) {
                    createCard();
                }
                break;

            case R.id.tvExpiryDate:
                showExpiryDate(mTvExpiryDate);
                break;
        }
    }

    private void createCard() {
        CardInfo cardInfo = new CardInfo();
        cardInfo.setAlgorithm(CardInfo.Algorithm.RSA1_5);
        cardInfo.setCardNumber(mEdtCardNumber.getText().toString());
        cardInfo.setCardHolder(mEdtNameOnCard.getText().toString());
        cardInfo.setExpireMonth(mMonth);
        cardInfo.setExpireYear(mYear);
        cardInfo.setCvv(mTvSecurityCode.getText().toString());
        Request request = new Request();
        request.setCardInfo(cardInfo);

        CreateTokenCard createTokenCard = new CreateTokenCard() {
            @Override
            protected void onPostExecute(Response response) {
                if (response != null && response.isSuccess()) {

                } else {

                }
            }
        };
        createTokenCard.execute(request);
    }

    private boolean valid(String nameOnCard, String cardNumber, String expiryDate, String securityCode) {
        if (TextUtils.isEmpty(nameOnCard.trim()) ||
                TextUtils.isEmpty(cardNumber.trim()) ||
                TextUtils.isEmpty(expiryDate) ||
                TextUtils.isEmpty(securityCode.trim())) {
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.all_text_are_mandatory)).show();
            return false;
        }

        if (cardNumber.trim().length() < MIN_LENGTH_CARD_NUMBER) {
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.input_error_card_number)).show();
            return false;
        }

        if (securityCode.trim().length() > MAX_LENGTH_SECURITY) {
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.input_error_cvc)).show();
            return false;
        }
        return true;
    }

    private void showExpiryDate(final TextView tvExpiryDate) {
        final Dialog dialog = new Dialog(mActivity);
        View view = LayoutInflater.from(mActivity).inflate(com.flocash.sdk.R.layout.content_input_date, null);

        final NumberPicker numberPickerMonth = (NumberPicker) view.findViewById(com.flocash.sdk.R.id.npMonth);
        final NumberPicker numberPickerYear = (NumberPicker) view.findViewById(com.flocash.sdk.R.id.npYear);
        String[] arrMonth = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerMonth.setWrapSelectorWheel(false);
        numberPickerMonth.setDisplayedValues(arrMonth);

        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int min = currentYear-10;
        int max = currentYear+10;

        numberPickerYear.setMinValue(min);
        numberPickerYear.setMaxValue(max);
        numberPickerYear.setValue(currentYear);
        numberPickerYear.setWrapSelectorWheel(false);

        Button btnOk = (Button) view.findViewById(com.flocash.sdk.R.id.btnOk);
        Button btnCancel = (Button) view.findViewById(com.flocash.sdk.R.id.btnCancel);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int valMonth = numberPickerMonth.getValue();
                mMonth = valMonth < 10 ? "0" + valMonth : String.valueOf(valMonth);
                mYear = String.valueOf(numberPickerYear.getValue()).substring(2,4);
                tvExpiryDate.setText(mMonth + "/" + mYear);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }
}

package com.app.flocash.appflobiller.tasks;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.utilities.JsonParse;

/**
 * Created by ${binhpd} on 12/4/2015.
 */
public class ApiConnectTask<T> extends AsyncTask<String, Void, T> {
    private FloBillerRequestListener listener;
    private Activity context;
    private JsonParse jsonParse;
    private String methodCalled;
    private String methodCall;
    private ProgressDialog mProgressDialog;
    private boolean mIsShow = true;
    public ApiConnectTask(Activity activity, String methCalled, JsonParse jsonParse, FloBillerRequestListener listener) {
        this.context = activity;
        this.jsonParse = jsonParse;
        this.methodCalled = methCalled;
        this.listener = listener;
        this.mProgressDialog = new ProgressDialog(this.context);
        this.mProgressDialog.setMessage(this.context.getString(R.string.notify_please_wait));
        this.mProgressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (context == null || context.isFinishing() ||
                ( android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN_MR1 && context.isDestroyed())) {
            return;
        }
        if (mIsShow) {
            mProgressDialog.show();
        }
    }

    @Override
    protected T doInBackground(String... params) {
        String url = params[0];
        JsonParse.RequestMethod requestMethod = JsonParse.RequestMethod.values()[Integer.parseInt(params[1])];
        try {
            jsonParse.Execute(requestMethod, url);
            return (T) jsonParse.getResponse();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(T result) {
        if (context == null || context.isFinishing() ||
                ( android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN_MR1 && context.isDestroyed())) {
            return;
        }
        mProgressDialog.dismiss();
        if (result != null) {
            listener.onSuccess(result, this.methodCalled);
        } else {
            listener.onFail(this.methodCalled);
        }
    }

    public interface FloBillerRequestListener {
        <T> void onSuccess(T result, String method);
        void onFail(String method);
    }

    public void setMessageDialog(String msg) {
        this.mProgressDialog.setMessage(msg);
    }

    public void setShowDialog(boolean isShow) {
        mIsShow = isShow;
    }
}

package com.app.flocash.appflobiller.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.ListingAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.dao.HistoryDataAccess;
import com.app.flocash.appflobiller.models.ItemListing;
import com.app.flocash.appflobiller.models.PayBillHistoryModel;
import com.app.flocash.appflobiller.models.PayBillerItem;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.JsonParse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnItemClick;

/**
 * Created by ${binhpd} on 8/17/2016.
 */
public class ListOptionPayBillFragment extends BaseFragment implements ApiConnectTask.FloBillerRequestListener {
    public static final int PAY_BILL = 0;
    public static final int AIRTIME = 1;
    public static final String EXTRA_STEP = "STEP";
    public static final String EXTRA_TYPE = "TYPE";
    public static final String EXTRA_COUNTRY = "COUNTRY";
    public static final String EXTRA_BILLER_ID = "BILLER_ID";
    public static final String EXTRA_BILLER_NAME = "BILLER_NAME";
    public static final String EXTRA_SUBSCRIPTION_ID = "SUBSCRIPTION_ID";
    public static final String EXTRA_SUBSCRIPTION_NAME = "SUBSCRIPTION_NAME";
    public static final String EXTRA_LOGO = "LOGO";

    private int mStep = 0;
    private int mPayType;

    private ListingAdapter mListingAdapter;
    private ArrayList<ItemListing> mListItems;

    @Bind(R.id.lvList)
    ListView mLvList;

    @Bind(R.id.llDoNotHaveSubscription)
    LinearLayout llDoNotHaveSubscription;

    @Bind(R.id.tvNotice)
    TextView mTvNotice;

    private String mCountry;
    private String mBillerId;
    private String mBillerName;
    private String mSubscriptionId;
    private String mSubscriptionName;
    private String mLogo;
    private ArrayList<PayBillHistoryModel> mListHistory;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_list;
    }

    @Override
    public void onCreateViewFragment(View view) {
        if (mIsLoad) {
            return;
        }
        readHistory();
        Bundle bundle = getArguments();
        if (bundle != null) {
            mStep = bundle.getInt(EXTRA_STEP);
            mPayType = bundle.getInt(EXTRA_TYPE);
            mBillerId = bundle.getString(EXTRA_BILLER_ID);
            mBillerName = bundle.getString(EXTRA_BILLER_NAME);
            mLogo = bundle.getString(EXTRA_LOGO);
        }

        mCountry = values.getUserCountry();
        if (mPayType == PAY_BILL) {
            if (mStep == 0) {
                retrieveBillers(mCountry);
            } else if (mStep == 1) {
                retrieve_subscriptions(mCountry, mBillerId);
            }
        } else {

        }
    }

    private void readHistory() {
        HistoryDataAccess historyDataAccess = new HistoryDataAccess(mActivity);
        mListHistory = historyDataAccess.getAllHistory();
        // filter item that is will be header
        int size = mListHistory.size();
        if (size > 0) {
            mListHistory.get(0).setHeader(true);
            for (int i = 1; i < size; i++) {
                mListHistory.get(i).setHeader(!mListHistory.get(i).getTimeHistory().equalsIgnoreCase(mListHistory.get(i-1).getTimeHistory()));
            }
        }
    }

    @Override
    public <T> void onSuccess(T result, String method) {
        final JSONObject resultJson = (JSONObject)result;
        if (result == null || resultJson.length() == 0) {
            return;
        }
        switch (method) {
            case ConfigUrlsAPI.REQUEST_GET_BILLER:
                try {
                    JSONArray result_array = resultJson.getJSONArray("billers");
                    mListItems = new ArrayList<>();
                    for (int i = 0; i < result_array.length(); i++) {
                        JSONObject obj = (JSONObject) result_array.get(i);
                        PayBillerItem payBillerItem = new PayBillerItem();
                        payBillerItem.setBillerId(obj.getString(FlobillerConstaints.PARA_KEY));
                        payBillerItem.setName(obj.getString(ConfigUrlsAPI.PARA_VALUE));
                        if (mCountry.equals(FlobillerConstaints.NIGERIA_CODE)) {
                            payBillerItem.setLogo((ConfigUrlsAPI.URL_BASE_LOGO + payBillerItem.getName()).replace(" ", "%20"));
                        } else {
                            payBillerItem.setLogo(ConfigUrlsAPI.URL_BASELOGO_PAYBILLER + obj.getString(ConfigUrlsAPI.PARA_LOGO));
                        }
                        mListItems.add(payBillerItem);
                    }

                    for (ItemListing itemListing : mListItems) {
                        for (PayBillHistoryModel payBillHistoryModel : mListHistory) {
                            if (itemListing.getName().equals(payBillHistoryModel.getSubscriptionName())) {
                                String time = FlobillerConstaints.TIME_PAID.format(
                                        FlobillerConstaints.TIME_VISIT_FORMAT.parseObject(payBillHistoryModel.getTimeHistory()));
                                itemListing.setLastPaid("You last paid " + payBillHistoryModel.getCurrencyCode() + payBillHistoryModel.getAmount() +
                                " on " + time);
                                break;
                            }
                        }
                    }

                    if (mListItems.size() == 0) {
                        llDoNotHaveSubscription.setVisibility(View.VISIBLE);
                        mLvList.setVisibility(View.GONE);
                        mTvNotice.setText(getString(R.string.dont_have_biller));
                    }

                    mListingAdapter = new ListingAdapter(mActivity, mListItems);
                    mLvList.setAdapter(mListingAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case ConfigUrlsAPI.REQUEST_GET_SUBSCRIPTION:
                try {
                    mListItems = new ArrayList<>();
                    JSONArray result_array = resultJson.getJSONArray("subscriptions");
                    if (result_array.length() <= 1) {
                        JSONObject obj = (JSONObject) result_array.get(0);
                        mSubscriptionId = obj.getString(FlobillerConstaints.PARA_KEY);
                        mSubscriptionName = obj.getString(ConfigUrlsAPI.PARA_VALUE);
                        Bundle bundle = new Bundle();
                        bundle.putString(EXTRA_SUBSCRIPTION_ID, mSubscriptionId);
                        bundle.putString(EXTRA_SUBSCRIPTION_NAME, mSubscriptionName);
                        bundle.putString(EXTRA_BILLER_ID, mBillerId);
                        bundle.putString(EXTRA_BILLER_NAME, mBillerName);
                        bundle.putString(EXTRA_LOGO, mLogo);

                        PayBillFragment payBillFragment = new PayBillFragment();
                        payBillFragment.setArguments(bundle);
                        replaceFragment(R.id.content, payBillFragment);
                        return;
                    }
                    for (int i = 0; i < result_array.length(); i++) {
                        JSONObject obj = (JSONObject) result_array.get(i);
                        PayBillerItem payBillerItem = new PayBillerItem();
                        payBillerItem.setBillerId(obj.getString(FlobillerConstaints.PARA_KEY));
                        payBillerItem.setName(obj.getString(ConfigUrlsAPI.PARA_VALUE));
                        mListItems.add(payBillerItem);
                    }

                    mListingAdapter = new ListingAdapter(mActivity, mListItems);
                    mLvList.setAdapter(mListingAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @OnItemClick(R.id.lvList)
    public void onClickItemList(AdapterView<?> parent, View view, int position, long id) {
        if (mPayType == PAY_BILL) {
            if (mPayType == 0) {
                if (mStep == 0) {
                    mBillerId = ((PayBillerItem) mListItems.get(position)).getBillerId();
                    mBillerName = ((PayBillerItem) mListItems.get(position)).getName();
                    mLogo = mListItems.get(position).getLogo();
                    ListOptionPayBillFragment listFragment = new ListOptionPayBillFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(EXTRA_BILLER_ID, mBillerId);
                    bundle.putString(EXTRA_BILLER_NAME, mBillerName);
                    bundle.putString(EXTRA_COUNTRY, mCountry);
                    bundle.putString(EXTRA_LOGO, mLogo);
                    bundle.putInt(EXTRA_STEP, 1);
                    listFragment.setArguments(bundle);
                    addFragment(R.id.content, listFragment);
                } else if (mStep == 1) {
                    mSubscriptionId = ((PayBillerItem)mListItems.get(position)).getBillerId();
                    mSubscriptionName = ((PayBillerItem)mListItems.get(position)).getName();
                    Bundle bundle = new Bundle();
                    bundle.putString(EXTRA_SUBSCRIPTION_ID, mSubscriptionId);
                    bundle.putString(EXTRA_SUBSCRIPTION_NAME, mSubscriptionName);
                    bundle.putString(EXTRA_BILLER_NAME, mBillerName);
                    bundle.putString(EXTRA_BILLER_ID, mBillerId);
                    bundle.putString(EXTRA_LOGO, mLogo);
                    PayBillFragment payBillFragment = new PayBillFragment();
                    payBillFragment.setArguments(bundle);
                    replaceFragment(R.id.content, payBillFragment);
                }
            }
        }
    }
    @Override
    public void onFail(String method) {

    }

    public void retrieveBillers(String countryCode) {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.paybillbillers_country_code, countryCode);
        ApiConnectTask js_billers = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_BILLER, client, this);
        js_billers.execute(new String[]{ConfigUrlsAPI.paybillbillers_url, JsonParse.REQUEST_TYPE_GET});
    }

    public void retrieve_subscriptions(String countryCode, String billerIds) {
        // String country_code = values.getCountryCode();
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.paybillbillers_country_code, countryCode);
        client.AddParam(ConfigUrlsAPI.paybillbiller_billerId, billerIds);
        ApiConnectTask js_billers = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_SUBSCRIPTION, client, this);
        js_billers.execute(new String[]{ConfigUrlsAPI.API_PAYBILL_SUBSCRIPTIONS, JsonParse.REQUEST_TYPE_GET});
    }
}

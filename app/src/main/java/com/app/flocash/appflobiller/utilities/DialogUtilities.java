package com.app.flocash.appflobiller.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FloBillderInterfaces;


/**
 * Created by ${binhpd} on 12/6/2015.
 */
public class DialogUtilities {

    private static final int FLAG_RESOURCE_NULL = -1;
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;

    public interface DialogCallBack {
        /**
         * @param pAlertDialog
         * @param pDialogType
         */
        void onClickDialog(DialogInterface pAlertDialog, int pDialogType);
    }

    public static class SimpleDialogDismissListener implements DialogCallBack {

        /**
         * @param pAlertDialog
         * @param pDialogType
         */
        @Override
        public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
            pAlertDialog.dismiss();
        }
    }

    /**
     * Get AlertDialog with 2 buttons button no title
     *
     * @param context
     * @param messageResId
     * @param positiveButtonResId
     * @param negativeButtonResId
     * @param callBack
     * @return
     */
    public static AlertDialog getAlertDialog(Context context, int messageResId, int positiveButtonResId, int negativeButtonResId, final DialogCallBack callBack) {
        return getAlertDialog(context, FLAG_RESOURCE_NULL, messageResId, positiveButtonResId, negativeButtonResId, FLAG_RESOURCE_NULL, callBack);
    }

    /**
     * Get AlertDialog with one negative button
     *
     * @param context
     * @param messageResId
     * @param negativeButtonResId
     * @param callBack
     * @return
     */
    public static AlertDialog getAlertDialog(Context context, int messageResId, int negativeButtonResId, final DialogCallBack callBack) {
        return getAlertDialog(context, FLAG_RESOURCE_NULL, messageResId, FLAG_RESOURCE_NULL, negativeButtonResId, FLAG_RESOURCE_NULL, callBack);
    }

    /**
     * Get AlertDialog with one negative button
     *
     * @param context
     * @param messageContent
     * @param negativeButtonResId
     * @param callBack
     * @return
     */
    public static AlertDialog getAlertDialog(Context context, String messageContent, int negativeButtonResId, final DialogCallBack callBack) {
        return getAlertDialog(context, FLAG_RESOURCE_NULL, messageContent, FLAG_RESOURCE_NULL, negativeButtonResId, FLAG_RESOURCE_NULL, callBack);
    }

    /**
     * show dialog with list select
     *
     * @param context context
     * @param titles  array title display
     */
    public static void showDialogSelected(Context context, final int id, final CharSequence[] titles, String currentTitle, final FloBillderInterfaces.OnSelectItemListener onSelectItemListener) {
        int size = titles.length;
        int lastCheck = 0;
        for (int i = 0; i < size; i++) {
            if (titles[i].equals(currentTitle)) {
                lastCheck = i;
                break;
            }
        }
        new AlertDialog.Builder(context)
                .setSingleChoiceItems(titles, lastCheck, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (onSelectItemListener != null) {
                            onSelectItemListener.onSelectItem(id, which);
                        }
                        dialog.dismiss();
                    }
                }).show();
    }

    //-------------------------
    public static AlertDialog getOkAlertDialog(Context pContext, int pMsgContentResId, final DialogCallBack pDialogCallBack) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentResId, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, pDialogCallBack);
    }

    public static AlertDialog getOkAlertDialog(Context pContext, String pMsgContent, final DialogCallBack pDialogCallBack) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContent, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, pDialogCallBack);
    }

    public static AlertDialog getOkAlertDialog(Context pContext, int pMsgContentResId) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentResId, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, new SimpleDialogDismissListener());
    }

    public static AlertDialog getOkAlertDialog(Context pContext, String pMsgContentRes) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentRes, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, new SimpleDialogDismissListener());
    }

    public static AlertDialog getOkAlertDialogColor(Context pContext, int result, String pMsgContent, final DialogCallBack pDialogCallBack) {
        return getAlertDialogColor(pContext, result, FLAG_RESOURCE_NULL, pMsgContent, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, pDialogCallBack);
    }

    public static AlertDialog getOkAlertDialogColor(Context pContext, int result, String pMsgContentResId) {
        return getAlertDialogColor(pContext, result, FLAG_RESOURCE_NULL, pMsgContentResId, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, null);
    }

    public static AlertDialog getOkAlertDialogColor(Context pContext, int result, String pMsgContentResId, boolean isDismiss) {
        return getAlertDialogColor(pContext, result, FLAG_RESOURCE_NULL, pMsgContentResId, R.string.alert_ok_button, FLAG_RESOURCE_NULL, FLAG_RESOURCE_NULL, null, isDismiss);
    }

    //--------------------------------
    public static AlertDialog getOkCancelAlertDialog(Context pContext, int pMsgContentResId, final DialogCallBack pDialogCallBack) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentResId, R.string.alert_ok_button, R.string.alert_cancel_button, FLAG_RESOURCE_NULL, pDialogCallBack);
    }

    public static AlertDialog getOkCancelAlertDialog(Context pContext, String pMsgContentResContent, final DialogCallBack pDialogCallBack) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentResContent, R.string.alert_ok_button, R.string.alert_cancel_button, FLAG_RESOURCE_NULL, pDialogCallBack);
    }

//    public static AlertDialog getYesNoAlertDialog(Context pContext, String pMsgContentResContent, final DialogCallBack pDialogCallBack) {
//        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentResContent, R.string.alert_yes_button, R.string.alert_no_button, FLAG_RESOURCE_NULL, pDialogCallBack);
//    }

    public static AlertDialog getOkCancelAlertDialog(Context pContext, int pMsgContentResId) {
        return getAlertDialog(pContext, FLAG_RESOURCE_NULL, pMsgContentResId, R.string.alert_ok_button, R.string.alert_cancel_button, FLAG_RESOURCE_NULL, new SimpleDialogDismissListener());
    }

    /**
     * Get AlertDialog
     *
     * @param context
     * @param titleResId          Set -1 if do not want to show
     * @param messageResId        Set -1 if do not want to show
     * @param positiveButtonResId Set -1 if do not want to show
     * @param negativeButtonResId Set -1 if do not want to show
     * @param callBack
     * @return
     */
    public static AlertDialog getAlertDialog(Context context, int titleResId, int messageResId, int positiveButtonResId, int negativeButtonResId, int neutralButtonResId, final DialogCallBack callBack) {
        return getAlertDialog(context, titleResId, context.getString(messageResId), positiveButtonResId, negativeButtonResId, neutralButtonResId, callBack);
    }

    public static AlertDialog getAlertDialog(final Context context, int titleResId, String message, int positiveButtonResId, int negativeButtonResId, int neutralButtonResId, final DialogCallBack callBack) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context) {
            @NonNull
            @Override
            public AlertDialog show() {
                final AlertDialog alertDialog = super.show();
                //Only after .show() was called
                return alertDialog;
            }
        };
        if (titleResId != FLAG_RESOURCE_NULL) {
            alertDialogBuilder.setTitle(titleResId);
        }
        alertDialogBuilder.setMessage(message);
        if (positiveButtonResId != FLAG_RESOURCE_NULL) {
            alertDialogBuilder.setPositiveButton(positiveButtonResId, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    if (callBack != null) {
                        callBack.onClickDialog(dialogInterface, DialogInterface.BUTTON_POSITIVE);
                    }
                }
            });
        }
        if (neutralButtonResId != FLAG_RESOURCE_NULL) {
            alertDialogBuilder.setNeutralButton(neutralButtonResId, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (callBack != null) {
                        callBack.onClickDialog(dialogInterface, DialogInterface.BUTTON_NEUTRAL);
                    }
                }
            });
        }
        if (negativeButtonResId != FLAG_RESOURCE_NULL) {
            alertDialogBuilder.setNegativeButton(negativeButtonResId, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int which) {
                    if (callBack != null) {
                        callBack.onClickDialog(dialogInterface, DialogInterface.BUTTON_NEGATIVE);
                    }
                }
            });
        }

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                //Only after .show() was called
//                final int buttonTextColor = context.getResources().getColor(isRed ? R.color.primary_red : R.color.colorTabSubcriptions);
////              alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(buttonTextColor);
//                alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(buttonTextColor);
//                alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(buttonTextColor);
            }
        });
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        return alertDialog;
    }

    public static AlertDialog getAlertDialogColor(final Context context, final int result, int titleResId, String message, int positiveButtonResId, int negativeButtonResId, int neutralButtonResId, final DialogCallBack callBack, final Boolean isDismiss) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context) {
            @NonNull
            @Override
            public AlertDialog show() {
                final AlertDialog alertDialog = super.show();
                //Only after .show() was called
                return alertDialog;
            }
        };
        if (titleResId != FLAG_RESOURCE_NULL) {
            alertDialogBuilder.setTitle(titleResId);
        }

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_dialog_success, null);
        ((TextView) view.findViewById(R.id.tvMsg)).setText(message);
        ((TextView) view.findViewById(R.id.tvMsg)).setBackgroundColor(context.getResources().getColor(result == FAIL ? R.color.primary_red :R.color.background_login));

        Button btnOk, btnCancel;
        btnOk = (Button) view.findViewById(R.id.btnOk);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        alertDialogBuilder.setView(view);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

        // handler click
        if (positiveButtonResId != FLAG_RESOURCE_NULL) {
            btnOk.setText(positiveButtonResId);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDismiss != null && isDismiss) {
                        alertDialog.dismiss();
                    }
                    if (callBack != null) {
                        callBack.onClickDialog(null, DialogInterface.BUTTON_POSITIVE);
                    }
                }
            });
        } else {
            btnOk.setVisibility(View.GONE);
        }

        if (negativeButtonResId != FLAG_RESOURCE_NULL) {
            btnCancel.setText(negativeButtonResId);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDismiss != null && isDismiss) {
                        alertDialog.dismiss();
                    }
                    if (callBack != null) {
                        callBack.onClickDialog(null, DialogInterface.BUTTON_NEGATIVE);
                    }
                }
            });
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        return alertDialog;
    }

    public static AlertDialog getAlertDialogColor(final Context context, final int result, int titleResId, String message, int positiveButtonResId, int negativeButtonResId, int neutralButtonResId, final DialogCallBack callBack) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context) {
            @NonNull
            @Override
            public AlertDialog show() {
                final AlertDialog alertDialog = super.show();
                //Only after .show() was called
                return alertDialog;
            }
        };
        if (titleResId != FLAG_RESOURCE_NULL) {
            alertDialogBuilder.setTitle(titleResId);
        }

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_dialog_success, null);
        ((TextView) view.findViewById(R.id.tvMsg)).setText(message);
        ((TextView) view.findViewById(R.id.tvMsg)).setBackgroundColor(context.getResources().getColor(result == FAIL ? R.color.primary_red :R.color.background_login));

        Button btnOk, btnCancel;
        btnOk = (Button) view.findViewById(R.id.btnOk);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        alertDialogBuilder.setView(view);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

        // handler click
        if (positiveButtonResId != FLAG_RESOURCE_NULL) {
            btnOk.setText(positiveButtonResId);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (callBack != null) {
                        callBack.onClickDialog(null, DialogInterface.BUTTON_POSITIVE);
                    }
                }
            });
        } else {
            btnOk.setVisibility(View.GONE);
        }

        if (negativeButtonResId != FLAG_RESOURCE_NULL) {
            btnCancel.setText(negativeButtonResId);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (callBack != null) {
                        callBack.onClickDialog(null, DialogInterface.BUTTON_NEGATIVE);
                    }
                }
            });
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        return alertDialog;
    }
}

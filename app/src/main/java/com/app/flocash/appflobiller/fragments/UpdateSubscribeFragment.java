package com.app.flocash.appflobiller.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;

import static com.app.flocash.appflobiller.common.ConfigUrlsAPI.REQUEST_GET_SUBSCRIPTION_DETAIL;
import static com.app.flocash.appflobiller.common.ConfigUrlsAPI.REQUEST_UPDATE_SUBSCRIPTION;

/**
 * Created by ${binhpd} on 12/13/2015.
 */
public class UpdateSubscribeFragment extends FloBillerBaseFragment {
    @Bind(R.id.edtAccountName)
    EditText mEdtAccountName;
    @Bind(R.id.edtAccountNumber)
    EditText mEdtAccountNumber;

    @Bind(R.id.ivFlag)
    ImageView mIvFlag;

    @Bind(R.id.tvNameSubscription)
    TextView mTvNameSubscription;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_create_edit_subscribe;
    }

    @Override
    public void onCreateViewFragment(View view) {
        values = new SavedSharedPreference(mActivity);
        Bundle bundle = getArguments();
        String url = bundle.getString(FlobillerConstaints.KEY_EXTRA_URL);
        String name= bundle.getString(FlobillerConstaints.KEY_EXTRA_NANE);

        Picasso.with(mActivity).load(url).into(mIvFlag);
        mTvNameSubscription.setText(name);
        retrieve_subscription();
    }

    public void retrieve_subscription() {
        String token = values.getAuthToken();
        LogUtils.e("tokennnnnnnnnnnnnn", token);
        String billerId = values.getBillerId();
        JsonParse client = new JsonParse(String.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        client.AddParam(ConfigUrlsAPI.biller_id, billerId);
        ApiConnectTask js = new ApiConnectTask(mActivity, REQUEST_GET_SUBSCRIPTION_DETAIL, client, this);
        js.execute(new String[] { ConfigUrlsAPI.subscribe_url, JsonParse.REQUEST_TYPE_GET });

    }

    public void update_subscription() {
        String authtok = values.getAuthToken();
        String billerId = values.getBillerId();
        JsonParse client = new JsonParse(String.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, authtok);
        client.AddParam(ConfigUrlsAPI.update_subAccName, mEdtAccountName.getText().toString());
        client.AddParam(ConfigUrlsAPI.subscribe_billerId, billerId);
        client.AddParam(ConfigUrlsAPI.update_subAccNo, mEdtAccountNumber.getText().toString());
        ApiConnectTask js = new ApiConnectTask(mActivity, REQUEST_UPDATE_SUBSCRIPTION, client, this);
        js.execute(new String[] { ConfigUrlsAPI.subscribe_url, JsonParse.REQUEST_TYPE_PUT });

    }

    @Override
    public <T> void onSuccess(T data, String method) {
        String result = (String) data;
        result = result.replaceAll("^\"|\"$", "");
        result = result.replaceAll("\\\\", "");
        if ((result != null) && (result.length() > 0)) {
            switch (method) {
                case REQUEST_GET_SUBSCRIPTION_DETAIL:
                    try {
                        JSONObject jsonResponse = new JSONObject(result);
                        JSONArray jsonMainNode = jsonResponse.getJSONArray("subscriptions");
                        for (int i = 0; i < jsonMainNode.length(); i++) {
                            JSONObject jsonChildNode = jsonMainNode
                                    .optJSONObject(i);
                            mEdtAccountNumber.setText(jsonChildNode.getString("account_number"));
                            mEdtAccountName.setText(jsonChildNode.getString("account_name"));
                        }
                    } catch (JSONException e) {
                        LogUtils.e("Exception is ", e.toString());
                    }
                    break;
                case REQUEST_UPDATE_SUBSCRIPTION:
                    try {
                        JSONObject jsonResponse = new JSONObject(result);
                        String status = jsonResponse.getString(ConfigUrlsAPI.PARA_STATUS);
                        LogUtils.e("status.........", status);
                        if (status.equalsIgnoreCase(ConfigUrlsAPI.RESULT_SUCCESS)) {
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.SUCCESS, jsonResponse.getString(ConfigUrlsAPI.PARA_MESSAGE), new DialogUtilities.DialogCallBack() {
                                @Override
                                public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                                    mActivity.finish();
                                }
                            }).show();

                        } else {
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, jsonResponse.getString(ConfigUrlsAPI.PARA_MESSAGE) ).show();
                        }
                    } catch (Exception e) {
                        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail) ).show();
                        LogUtils.e("Exception is", e.toString());
                    }
                    break;
            }
        }
    }

    @Override
    public void onFail(String method) {
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (TextUtils.isEmpty(mEdtAccountName.getText()) ||
                TextUtils.isEmpty(mEdtAccountNumber.getText())){
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_fields_required)).show();
        } else {
            update_subscription();
        }
        return true;
    }
}

package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.models.GiftModel;

import java.util.ArrayList;

/**
 * Created by lion on 3/16/17.
 */

public class GiftAdapter extends RecyclerView.Adapter<GiftAdapter.ViewHolder> {
    private final Context mContext;
    private final String giftId;
    private ArrayList<GiftModel> mGiftModels;
    private GiftCardAdapter.OnSendGiftListener listener;
    private String giftName;
    public GiftAdapter(Context context, String giftId, String giftName,
                       ArrayList<GiftModel> giftModels, GiftCardAdapter.OnSendGiftListener onSendGiftListener) {
        this.mContext = context;
        this.giftId = giftId;
        this.giftName = giftName;
        this.mGiftModels = giftModels;
        this.listener = onSendGiftListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.gift_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String amount = mGiftModels.get(position).getAmount();
        holder.tvAmount.setText("$" + amount);
        holder.tvAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSendGift(giftId, giftName, amount);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGiftModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAmount;
        public ViewHolder(View itemView) {
            super(itemView);
            tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
        }
    }
}

package com.app.flocash.appflobiller.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;

/**
 * Created by ${binhpd} on 12/12/2015.
 */
public class CreateSubcribleFragment extends FloBillerBaseFragment {
    @Bind(R.id.edtAccountName)
    EditText mEdtAccountName;

    @Bind(R.id.edtAccountNumber)
    EditText mEdtAccountNumber;

    @Bind(R.id.ivFlag)
    ImageView mIvFlag;

    @Bind(R.id.tvNameSubscription)
    TextView mTvNameSubscription;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_create_edit_subscribe;
    }

    @Override
    public void onCreateViewFragment(View view) {
        Bundle bundle = getArguments();

        String url = bundle.getString(FlobillerConstaints.KEY_EXTRA_URL);
        String name= bundle.getString(FlobillerConstaints.KEY_EXTRA_NANE);

        Picasso.with(mActivity).load(url).into(mIvFlag);
        mTvNameSubscription.setText(name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(mEdtAccountName.getText().length()<1 || mEdtAccountNumber.getText().length()<1){
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_fields_required)).show();
        } else {
            addSubscription();
        }
        return true;
    }
    public void addSubscription() {

        JsonParse client = new JsonParse(JSONObject.class);
        String auth_token = values.getAuthToken();
        String biller_id = values.getBillerId();
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        Log.e("auth_token", auth_token);
        client.AddParam(ConfigUrlsAPI.subscribe_billerId, biller_id);
        client.AddParam(ConfigUrlsAPI.subscribe_accName, mEdtAccountName.getText()
                .toString());
        client.AddParam(ConfigUrlsAPI.subscribe_accno, mEdtAccountNumber.getText().toString());
        ApiConnectTask js = new ApiConnectTask(mActivity, "save", client, this);
        js.execute(new String[] { ConfigUrlsAPI.subscribe_url, JsonParse.REQUEST_TYPE_POST });

    }

    @Override
    public <T> void onSuccess(T data, String method) {
        JSONObject result = (JSONObject) data;
        if ((result != null) && (result.length() > 0)) {
            try {
                Log.e("result is ", "" + result);
                String status = result.getString(ConfigUrlsAPI.PARA_STATUS);
                if (status.equalsIgnoreCase(ConfigUrlsAPI.RESULT_SUCCESS)) {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.SUCCESS, result.getString(ConfigUrlsAPI.PARA_MESSAGE), new DialogUtilities.DialogCallBack() {
                        @Override
                        public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                            Intent intent = new Intent(mActivity, SubScreenActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_SUBSCRIPTION);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            mActivity.finish();
                        }
                    }).show();
                } else {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
                }
            } catch (JSONException e) {
                Log.e("Exception is ", e.toString());
                DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail) ).show();
            }
        }
//        mProgressDialog.dismiss();
    }

    @Override
    public void onFail(String method) {
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }
}

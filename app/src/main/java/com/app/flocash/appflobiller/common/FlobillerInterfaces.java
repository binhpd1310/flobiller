package com.app.flocash.appflobiller.common;

import android.view.View;

/**
 * Created by ${binhpd} on 11/20/2016.
 */

public interface FlobillerInterfaces {
    interface OnItemClick {
        void onItemClick(View view, int index);
    }
}

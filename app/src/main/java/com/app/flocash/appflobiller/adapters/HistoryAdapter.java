package com.app.flocash.appflobiller.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.models.PayBillHistoryModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by ${binhpd} on 3/17/2016.
 */
public class HistoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PayBillHistoryModel> payBillHistoryModels = new ArrayList<>();
    private LayoutInflater inflater;
    public HistoryAdapter(Context context, ArrayList<PayBillHistoryModel> payBillHistoryModels) {
        this.context = context;
        this.payBillHistoryModels = payBillHistoryModels;
        this.inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public int getCount() {
        return payBillHistoryModels.size();
    }

    @Override
    public PayBillHistoryModel getItem(int position) {
        return payBillHistoryModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView==null){
            convertView = inflater.inflate(R.layout.item_history, parent,false);
            holder = new ViewHolder();
            holder.tvHeader = (TextView) convertView.findViewById(R.id.tvTitleHeader);
            holder.tvBillerName = (TextView)convertView.findViewById(R.id.tvBillerName);
            holder.tvTime = (TextView)convertView.findViewById(R.id.tvTime);
            holder.ivLogo = (ImageView) convertView.findViewById(R.id.ivLogo);
            holder.tvAmount = (TextView) convertView.findViewById(R.id.tvAmount);
            convertView.setTag(holder);

        } else{
            holder = (ViewHolder) convertView.getTag();
        }
        PayBillHistoryModel payBillHistoryModel = payBillHistoryModels.get(position);

        holder.tvBillerName.setText(payBillHistoryModel.getBillerName());
        try {
            // display header title listview
            if (payBillHistoryModel.isHeader()) {
                holder.tvHeader.setVisibility(View.VISIBLE);
                holder.tvHeader.setText(FlobillerConstaints.TIME_VISIT_FORMAT_HEADER.format(
                        FlobillerConstaints.TIME_VISIT_FORMAT.parse(payBillHistoryModel.getTimeHistory())));
            } else {
                holder.tvHeader.setVisibility(View.GONE);
            }

            holder.tvTime.setText(FlobillerConstaints.TIME_VISIT_FORMAT_DISPLAY.format(
                    FlobillerConstaints.TIME_VISIT_FORMAT.parse(payBillHistoryModel.getTimeHistory())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvAmount.setText("$ " + payBillHistoryModel.getAmount());
        if (!TextUtils.isEmpty(payBillHistoryModel.getUrlLogo())) {
            Picasso.with(context).load(payBillHistoryModel.getUrlLogo()).into(holder.ivLogo);
        }
        return convertView;

    }
    static class ViewHolder {
        TextView tvHeader;
        TextView tvBillerName;
        TextView tvTime;
        ImageView ivLogo;
        TextView tvAmount;
    }
}

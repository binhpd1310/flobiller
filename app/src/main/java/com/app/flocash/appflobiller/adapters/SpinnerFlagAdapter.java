package com.app.flocash.appflobiller.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 3/24/2016.
 */

public class SpinnerFlagAdapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList data;
    private LayoutInflater inflater;
    private ArrayList<String> logoesUrl;

    public SpinnerFlagAdapter(Activity context, int textViewResourceId, ArrayList objects, ArrayList<String> urlLogoes) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.data = objects;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.logoesUrl = urlLogoes;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_spinner_flag, parent, false);
            holder = new Holder();
            holder.tvLabel = (TextView) convertView.findViewById(R.id.tvCountryName);
            holder.ivFlag = (ImageView) convertView.findViewById(R.id.ivFlag);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        String operatorName = (String) data.get(position);
        holder.tvLabel.setText(operatorName);
        if (position == 0) {
            holder.ivFlag.setVisibility(View.INVISIBLE);
        } else {
            holder.ivFlag.setVisibility(View.VISIBLE);
            Picasso.with(context).load(logoesUrl.get(position)).into(holder.ivFlag);
        }
        return convertView;
    }

    public class Holder {
        TextView tvLabel;
        ImageView ivFlag;
    }
}



package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 8/20/2016.
 */
public class AmountAdapter extends BaseAdapter {
    private ArrayList<String> mListAmount;
    private Context context;

    private int indexFocus = -1;

    public AmountAdapter(Context context, ArrayList<String> mListAmount) {
        this.mListAmount = mListAmount;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mListAmount.size();
    }

    @Override
    public Object getItem(int position) {
        return mListAmount.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_amount, null);
        view.findViewById(R.id.llContent).setBackgroundResource(
                position == indexFocus ?
                R.drawable.round_with_border_and_background :
                R.drawable.round_with_border);
        String[] currencyAmount = mListAmount.get(position).split(" ");
        ((TextView)view.findViewById(R.id.tvCurrency)).setText(currencyAmount[0]);
        ((TextView)view.findViewById(R.id.tvAmount)).setText(currencyAmount[1]);
        return view;
    }

    public int getIndexFocus() {
        return indexFocus;
    }

    public void setIndexFocus(int indexFocus) {
        this.indexFocus = indexFocus;
    }
}

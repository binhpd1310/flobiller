package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.LoginActivity;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;
import com.app.flocash.appflobiller.utilities.ServiceUtil;
import com.app.flocash.appflobiller.utilities.Utils;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 12/4/2015.
 */
public class HomeFragment extends FloBillerBaseFragment {
    private String mCountryCode;
    private String mCountryPhoneCode;
    private String mCurrency;
    private String mEmail;
    private String mLastName;
    private String mDateOfBirth;
    private String mFirstName;
    private String mCustomerId;
    private String mMobileNumber;

    @Bind(R.id.llPayNow)
    LinearLayout mLLPayNow;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_home_screen;
    }

    @Override
    public void onCreateViewFragment(View view) {
        retrieveUserInfo();
        retrieveBillers();
    }

    @OnClick({R.id.llPayNow, R.id.llSubscription, R.id.llInvoices, R.id.llTickets, R.id.llGiftCard, R.id.llAirtime})
    public void onClick(View view) {
        Intent intent;
        Bundle bundle;
        switch (view.getId()) {
            case R.id.llPayNow:
                selectFeature(ServiceUtil.PAYBILL);
                break;
            case R.id.llSubscription:
                intent = new Intent(getActivity(), SubScreenActivity.class);
                bundle = new Bundle();
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_SUBSCRIPTION);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.llInvoices:
                intent = new Intent(getActivity(), SubScreenActivity.class);
                bundle = new Bundle();
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_INVOICES);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.llTickets:
                selectFeature(ServiceUtil.TICKET);
                break;
            case R.id.llGiftCard:
                selectFeature(ServiceUtil.VOUCHER);

                break;
            case R.id.llAirtime:
                selectFeature(ServiceUtil.AIRTIME);
                break;
        }
    }

    public void selectFeature(String paybill) {
        switch (paybill) {
            case ServiceUtil.PAYBILL:
                Intent intent;
                Bundle bundle;
                if (ServiceUtil.checkValidService(mActivity, ServiceUtil.PAYBILL)) {
                    intent = new Intent(getActivity(), SubScreenActivity.class);
                    bundle = new Bundle();
                    bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAY_BILL);
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                } else {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL,
                            getString(R.string.service_not_available_for_this_country)).show();
                }
                break;
            case ServiceUtil.TICKET:
                if (ServiceUtil.checkValidService(mActivity, ServiceUtil.TICKET)) {
                    intent = new Intent(getActivity(), SubScreenActivity.class);
                    bundle = new Bundle();
                    bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_CALLED_TICKETS);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL,
                            getString(R.string.service_not_available_for_this_country)).show();
                }
                break;

            case ServiceUtil.VOUCHER:
                if (ServiceUtil.checkValidService(mActivity, ServiceUtil.VOUCHER)) {
                    intent = new Intent(getActivity(), SubScreenActivity.class);
                    bundle = new Bundle();
                    bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_GIFT_CARD);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL,
                            getString(R.string.service_not_available_for_this_country)).show();
                }
                break;

            case ServiceUtil.AIRTIME:
                if (ServiceUtil.checkValidService(mActivity, ServiceUtil.AIRTIME)) {
                    intent = new Intent(getActivity(), SubScreenActivity.class);
                    bundle = new Bundle();
                    bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_AIRTIME);
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                } else {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL,
                            getString(R.string.service_not_available_for_this_country)).show();
                }
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getValidService();
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        String content = data.toString();
        content = content.replaceAll("^\"|\"$", "");
        content = content.replaceAll("\\\\", "");
        JSONObject result;
        try {
            result = new JSONObject(content);
            if ((result != null) && (result.length() > 0)) {
                switch (method) {
                    case ConfigUrlsAPI.REQUEST_LOGOUT:
                        values.setAuthToken(null);
                        values.setUserEmail(null);
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        break;
                    case ConfigUrlsAPI.REQUEST_GET_USER_INFO:
                        try {
                            JSONObject obj = result.getJSONObject(ConfigUrlsAPI.PARA_USER);
                            mEmail = obj.getString(ConfigUrlsAPI.PARA_EMAIL);
                            mDateOfBirth = obj.getString(ConfigUrlsAPI.PARA_DATE_OF_BIRTH);
                            mFirstName = obj.getString(ConfigUrlsAPI.PARA_FIRST_NAME);
                            mLastName = obj.getString(ConfigUrlsAPI.PARA_LAST_NAME);
                            mCustomerId = obj.getString(ConfigUrlsAPI.PARA_CUSTOMER_ID);
                            mCurrency = obj.getString(ConfigUrlsAPI.PARA_CURRENCY);
                            mCountryCode = obj.getString(ConfigUrlsAPI.PARA_COUNTRY).toString();
                            mCountryPhoneCode = obj.getString(ConfigUrlsAPI.PARA_COUNTRY_CODE);
                            mMobileNumber = obj.getString(ConfigUrlsAPI.PARA_MOBILE);

                            values.setUserEmail(mEmail);
                            values.setUserLastname(mLastName);
                            values.setUserDob(mDateOfBirth);
                            values.setUserFirstname(mFirstName);
                            values.setCustomerId(mCustomerId);
                            values.setCurrency(mCurrency);
                            values.setUserCountry(mCountryCode);
                            values.setCountryCode(mCountryPhoneCode);
                            values.setUserMobile(mMobileNumber);
                        } catch (JSONException e) {
                            LogUtils.e("Exception is ", e.toString());
                        }
                        break;

                    case ConfigUrlsAPI.REQUEST_GET_BILLER:
                        Utils.saveToFileCache(getActivity(), result.toString(), FlobillerConstaints.BILLER_CACHE_FILE_NAME);
                        break;

                    case ConfigUrlsAPI.REQUEST_GET_VALID_SERVICE:
                        String status = result.getString(ConfigUrlsAPI.PARA_STATUS);
                        String services = "";
                        try {
                            if (status.equals(ConfigUrlsAPI.STATUS_SUCESS)) {
                                services = result.getJSONArray(ConfigUrlsAPI.MSG_RESPONSE).getJSONObject(0).getString(ConfigUrlsAPI.PARA_AVAIL_SERVICE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        values.setValidService(services);
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void retrieveUserInfo() {
        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_USER_INFO, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.user_retrieve_url, JsonParse.REQUEST_TYPE_GET});
    }

    private void retrieveBillers() {
        String token = values.getAuthToken();
        LogUtils.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(String.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_BILLER, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.billers_url, JsonParse.REQUEST_TYPE_GET});
    }

    private void getValidService() {
        JsonParse client = new JsonParse(JsonObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, values.getAuthToken());
        client.AddParam(ConfigUrlsAPI.FIELD_ID, values.getUserCountry());
        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_VALID_SERVICE, client, this);
        apiConnectTask.setShowDialog(false);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_GET_VALID_SERVICE, JsonParse.REQUEST_TYPE_GET});
    }
}

package com.app.flocash.appflobiller.models;

/**
 * Created by tuan.pv on 11/13/2015.
 */
public class CountryModel {
    private String mKey;
    private String mValue;
    private String mCode;

    public CountryModel(String mKey, String mValue, String mCode) {
        this.mKey = mKey;
        this.mValue = mValue;
        this.mCode = mCode;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey(String mKey) {
        this.mKey = mKey;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String mValue) {
        this.mValue = mValue;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }
}

package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.models.ItemListing;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 8/20/2016.
 */
public class ListingAdapter extends BaseAdapter {

    private ArrayList<ItemListing> listings;
    private Context context;

    public ListingAdapter(Context context, ArrayList<ItemListing> listings) {
        this.listings = listings;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listings.size();
    }

    @Override
    public Object getItem(int position) {
        return listings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HolderView holderView;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_listing, null);
            holderView = new HolderView(convertView);
            convertView.setTag(holderView);
        } else {
            holderView = (HolderView) convertView.getTag();
        }

        ItemListing itemListing = listings.get(position);
        holderView.tvName.setText(itemListing.getName());
        if (TextUtils.isEmpty(itemListing.getLogo())) {
            holderView.ivLogo.setVisibility(View.GONE);
        } else {
            Picasso.with(context).load(itemListing.getLogo()).into(holderView.ivLogo);
        }

        if (TextUtils.isEmpty(itemListing.getLastPaid())) {
            holderView.tvNote.setVisibility(View.GONE);
        } else {
            holderView.tvNote.setText(itemListing.getLastPaid());
        }

        return convertView;
    }

    public class HolderView {
        ImageView ivLogo;
        TextView tvName;
        TextView tvNote;

        public HolderView(View view) {
            ivLogo = (ImageView) view.findViewById(R.id.ivLogo);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvNote = (TextView) view.findViewById(R.id.tvNote);
        }
    }
}
package com.app.flocash.appflobiller.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.text.Spanned;
import android.widget.Toast;

import com.app.flocash.appflobiller.common.FlobillerConstaints;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils {

    // Check network available
    public static boolean checkNetwork(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();

        return !(info == null || !info.isConnected());
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static Bitmap getThumbnailBimapFromFilePath(String path) {
        Bitmap returnBitmap = null;
        try {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                Bitmap bm = null;
                // First decode with inJustDecodeBounds=true to check dimensions
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, options);

                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, 300, 300);

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                returnBitmap = BitmapFactory.decodeFile(path, options);
            }
        } catch (Exception e) {
            return returnBitmap;
        }
        return returnBitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    public static File saveToFileCache(Context context, String content, String nameFile) {
        InputStream is;
        String tempPath;
        FileOutputStream out = null;
        File file = null;
        try {
            is = new ByteArrayInputStream(content.getBytes("UTF-8"));
            tempPath = context.getExternalFilesDir(null) + FlobillerConstaints.FOLDER_CACHE;
            file = new File(tempPath);
            if (!file.exists()) {
                file.mkdir();
            }
            tempPath += FlobillerConstaints.APPEND_PATH_COMPONENT + nameFile;
            file = new File(tempPath);
            out = new FileOutputStream(tempPath);
            //transfer bytes from the input file to the output file
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static final File getFileFromCache(Context context, String fileName) {
        String filePath = context.getExternalFilesDir(null) + FlobillerConstaints.FOLDER_CACHE + FlobillerConstaints.APPEND_PATH_COMPONENT + fileName;
        File file = new File(filePath);
        return file.exists() ? file : null;
    }

    public static final void clearCache(Context context) {
        try {
            String filePath = context.getExternalFilesDir(null) + FlobillerConstaints.FOLDER_CACHE + FlobillerConstaints.APPEND_PATH_COMPONENT;
            File file = new File(filePath);
            String[] children = file.list();
            if (children == null) {
                LogUtils.e("Cache empty");
                return;
            }
            for (int i = 0; i < children.length; i++)
            {
                new File(file, children[i]).delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static Spanned getTextFromHtml(String text) {
        Spanned  result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(text);
        }
        return result;
    }


}
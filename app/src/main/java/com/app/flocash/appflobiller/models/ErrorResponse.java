package com.app.flocash.appflobiller.models;

/**
 * Created by tuan.pv on 11/13/2015.
 */
public class ErrorResponse {
    private String status;
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

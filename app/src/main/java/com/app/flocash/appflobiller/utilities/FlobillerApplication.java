package com.app.flocash.appflobiller.utilities;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.io.File;
import java.util.Locale;

import uk.co.senab.bitmapcache.BitmapLruCache;

/**
 * Created by ${binhpd} on 3/12/2016.
 */
public class FlobillerApplication extends Application {
    private BitmapLruCache mCache;
    private static Context mContext;
    private final String TAG = getClass().getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        File cacheDir = new File(getCacheDir(), FlobillerConstaints.CACHE_DIR);
        if (!cacheDir.mkdirs()) {
            LogUtils.e(TAG, "error cacheDir.mkdir()");
        }
        BitmapLruCache.Builder builder = new BitmapLruCache.Builder(this);
        builder.setMemoryCacheEnabled(true).setMemoryCacheMaxSizeUsingHeapSize();
        builder.setDiskCacheEnabled(true).setDiskCacheLocation(cacheDir);

        mCache = builder.build();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setLanguage();
    }

    public BitmapLruCache getBitmapCache() {
        return mCache;
    }

    public static FlobillerApplication getInstance() {
        return (FlobillerApplication) mContext;
    }

    private void setLanguage() {
        SavedSharedPreference savedSharedPreference = new SavedSharedPreference(mContext);
        String languageToLoad  = savedSharedPreference.getLanguage();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
        getBaseContext().getResources().getDisplayMetrics());

    }
}
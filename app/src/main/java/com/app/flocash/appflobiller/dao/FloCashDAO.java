package com.app.flocash.appflobiller.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;


import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

import com.app.flocash.appflobiller.models.CountryModel;

public class FloCashDAO extends SQLiteAssetHelper {
	private static final String DATABASE_NAME = "FloCash.sqlite";
	private static final int DATABASE_VERSION = 1;

	private static final String TABLE_COUNTRIES = "Countries";
	// column table Countries
	private static final String COLUMN_COUNTRY_KEY = "key";
	private static final String COLUMN_COUNTRY_VALUE = "value";
	private static final String COLUMN_COUNTRY_CODE = "code";

	public FloCashDAO(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public ArrayList<CountryModel> getAllCountry() {
		ArrayList<CountryModel> countryModels = new ArrayList<>();
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		String selectQuery = "SELECT * FROM " + TABLE_COUNTRIES + " ORDER BY " + COLUMN_COUNTRY_VALUE + " ASC";
		qb.setTables(TABLE_COUNTRIES);
		Cursor cursor = null;
		try {
		cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					String key = cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY_KEY));
					String value = cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY_VALUE));
					String code = cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY_CODE));

					CountryModel countryModel = new CountryModel(key, value, code);
					countryModels.add(countryModel);
				} while (cursor.moveToNext());
			}
		}
		}catch (SQLException ex) {
			ex.printStackTrace();
        }finally {
			if (cursor != null) {
				cursor.close();
			}
            db.close();
        }
		return countryModels;
	}
	
	public void addCountry(CountryModel countryModel) {
		SQLiteDatabase db = getWritableDatabase();
		try {
		ContentValues values = new ContentValues();
		values.put(COLUMN_COUNTRY_KEY, countryModel.getKey());
		values.put(COLUMN_COUNTRY_VALUE, countryModel.getValue());
		values.put(COLUMN_COUNTRY_CODE, countryModel.getCode());

		db.insert(TABLE_COUNTRIES, null, values);
		}
        catch (SQLException ex) {
			ex.printStackTrace();
        }finally {
        	db.close();
        }
	}
}
package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 11/18/2016.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Holder> {

    private ArrayList<CategoryModel> mCategories;
    private Context mContext;
    private FlobillerInterfaces.OnItemClick mListener;

    public CategoryAdapter(Context context, ArrayList<CategoryModel> categories) {
        this.mContext = context;
        this.mCategories = categories;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_category, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        CategoryModel item = mCategories.get(position);
        holder.tvName.setText(item.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(holder.tvName, position);
            }
        });
        Picasso.with(mContext).load(item.getLogo()).into(holder.ivLogo);
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public void setClickListener(FlobillerInterfaces.OnItemClick listener) {
        this.mListener = listener;
    }

    public class Holder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public ImageView ivLogo;

        public Holder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivLogo = (ImageView) itemView.findViewById(R.id.ivLogo);
        }
    }
}

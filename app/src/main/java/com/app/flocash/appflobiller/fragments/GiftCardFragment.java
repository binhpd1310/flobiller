package com.app.flocash.appflobiller.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.adapters.GiftCardAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.models.GiftCard;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by ${binhpd} on 5/23/2016.
 */
public class GiftCardFragment extends BaseFragment implements ApiConnectTask.FloBillerRequestListener, GiftCardAdapter.OnSendGiftListener {

    private ArrayList<GiftCard> mListGiftCards = new ArrayList<>();
    private GiftCardAdapter mGiftCardAdapter;

    @Bind(R.id.lvGiftCard)
    protected ListView mLvGiftCard;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_gift_card;
    }

    @Override
    public void onCreateViewFragment(View view) {
        receiveAllGiftCards();
    }

    private void receiveAllGiftCards() {
        if (values.getCurrency().equals("ETB")) {
            Toast.makeText(mActivity, R.string.service_not_available_for_this_country, Toast.LENGTH_LONG).show();
            return;
        }
        String authen_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, authen_token);
        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_ALL_GIFT_CARD, client, this);
        apiConnectTask.execute(new String[] {ConfigUrlsAPI.API_RECEIVE_ALL_GIFT_CARD_URL, JsonParse.REQUEST_TYPE_GET});
    }

    @Override
    public <T> void onSuccess(T result, String method) {
        final JSONObject resultJSON = (JSONObject) result;
        try {
            if (resultJSON != null && resultJSON.length() > 0) {
                switch (method) {
                    case ConfigUrlsAPI.REQUEST_GET_ALL_GIFT_CARD:
                        onGetAllGiftCards(resultJSON);
                        break;
                    case ConfigUrlsAPI.REQUEST_ADD_INVOICE:

                        Log.e("reuslt", "" + resultJSON);
                        try {
                            String status = resultJSON.getString(ConfigUrlsAPI.PARA_STATUS);
                            if (ConfigUrlsAPI.RESULT_SUCCESS.equalsIgnoreCase(status)) {
                                // direct to invoices
                                DialogUtilities.getAlertDialogColor(mActivity, DialogUtilities.SUCCESS,
                                        -1,
                                        getString(R.string.add_invoice_sucess),
                                        R.string.pay_now,
                                        R.string.cancel,
                                        -1,
                                        new DialogUtilities.DialogCallBack() {
                                            @Override
                                            public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                                                // direct to invoices
                                                if (pDialogType == DialogInterface.BUTTON_POSITIVE) {
                                                    try {
                                                        String invoiceId = resultJSON.getJSONObject(ConfigUrlsAPI.PARA_INVOICE).getString(ConfigUrlsAPI.PARA_INVOICE);
                                                        Intent intent = new Intent(mActivity, SubScreenActivity.class);
                                                        Bundle bundle = new Bundle();
                                                        bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT);
                                                        bundle.putString(FlobillerConstaints.KEY_EXTRA_ORDER_ID, invoiceId);
                                                        intent.putExtras(bundle);
                                                        startActivity(intent);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    mActivity.finish();
                                                }
                                            }
                                        }).show();
                            }  else if (ConfigUrlsAPI.result_error.equalsIgnoreCase(status)) {
                                String errMsg = resultJSON.getString(ConfigUrlsAPI.PARA_MESSAGE);
                                DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, errMsg).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void onGetAllGiftCards(JSONObject jsonObject) throws JSONException {
        JSONArray listGiftCards = jsonObject.getJSONArray(ConfigUrlsAPI.RECEIVE_ALL_GIFT_CARD_GIFT_CARDS);
        for (int i = 0; i < listGiftCards.length(); i++) {
            JSONObject giftCardNode = listGiftCards.optJSONObject(i);
            String id = giftCardNode.getString(ConfigUrlsAPI.RECEIVE_ALL_GIFT_CARD_GIFT_CARD);
            String name = giftCardNode.getString(ConfigUrlsAPI.RECEIVE_ALL_GIFT_CARD_GIFT_NAME);
            String logo = ConfigUrlsAPI.LOGO_URL_BASE + giftCardNode.getString(ConfigUrlsAPI.RECEIVE_ALL_GIFT_CARD_GIFT_LOGO);
            String amount = giftCardNode.getString(ConfigUrlsAPI.RECEIVE_ALL_GIFT_CARD_GIFT_AMOUNT);
            GiftCard giftCard = new GiftCard(id, name, logo, amount);
            mListGiftCards.add(giftCard);
        }

        mGiftCardAdapter = new GiftCardAdapter(mActivity, mListGiftCards, GiftCardFragment.this);
        mLvGiftCard.setAdapter(mGiftCardAdapter);
    }

    @Override
    public void onFail(String method) {

    }

    @Override
    public void onSendGift(String id,  String name, String amount) {
        addInvoice(id, name, amount);
    }

    public void addInvoice(String id, String name, String amount) {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_api, ConfigUrlsAPI.REQUEST_GIFT_CARD);
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_amount, amount);
        client.AddParam(ConfigUrlsAPI.add_gift_card_name, name);
        client.AddParam(ConfigUrlsAPI.add_gift_card_id, id);
        ApiConnectTask js_invoices = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_ADD_INVOICE, client, this);
        js_invoices.execute(new String[]{ConfigUrlsAPI.add_invoice_url, JsonParse.REQUEST_TYPE_POST});
    }
}

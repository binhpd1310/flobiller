package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 4/13/2016.
 */
public class PrivacyFragment extends BaseFragment {
    @Bind(R.id.tvLink)
    TextView tvLink;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_privacy;
    }

    @Override
    public void onCreateViewFragment(View view) {
//        tvLink.setText(Html.fromHtml("<a href=\\>https://flobiller.flocash.com</a>"));
        tvLink.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnClick(R.id.tvLink)
    public void onClick(View view) {
        Uri uriUrl = Uri.parse(getString(R.string.link_privacy));
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}
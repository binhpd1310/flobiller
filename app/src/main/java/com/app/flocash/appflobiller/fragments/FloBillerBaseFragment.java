package com.app.flocash.appflobiller.fragments;

import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;

import org.json.JSONObject;

/**
 * Created by ${binhpd} on 12/5/2015.
 */
public abstract class FloBillerBaseFragment extends BaseFragment implements
        ApiConnectTask.FloBillerRequestListener {

    protected boolean isHaveData(JSONObject jsonObject) {
        try {
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.getString("status").equals(ConfigUrlsAPI.STATUS_SUCESS)) {
                    return true;
                }
                return false;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onFail(String method) {
        Toast.makeText(mActivity, getString(R.string.notify_error_network), Toast.LENGTH_LONG).show();
    }
}


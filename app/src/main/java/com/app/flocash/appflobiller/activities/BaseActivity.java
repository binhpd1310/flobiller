package com.app.flocash.appflobiller.activities;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;

import org.json.JSONObject;

/**
 * Created by ${binhpd} on 5/5/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements ApiConnectTask.FloBillerRequestListener {
    protected boolean isHaveData(JSONObject jsonObject) {
        try {
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.getString("status").equals(ConfigUrlsAPI.STATUS_SUCESS)) {
                    return true;
                }
                return false;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onFail(String method) {
        Toast.makeText(this, getString(R.string.notify_error_network), Toast.LENGTH_LONG).show();
    }
}

package com.app.flocash.appflobiller.models;

/**
 * Created by ${binhpd} on 3/9/2016.
 */
public class CountryResponseModel {
    private String iso;
    private String key;
    private String id;
    private String name;
    private String value;
    private String urlImg;
    private String code;

    public CountryResponseModel(String iso, String key, String id, String name, String value, String urlImg, String code) {
        this.iso = iso;
        this.key = key;
        this.id = id;
        this.name = name;
        this.value = value;
        this.urlImg = urlImg;
        this.code = code;
    }

    public CountryResponseModel(String iso, String key, String id, String name, String value, String urlImg) {
        this.iso = iso;
        this.key = key;
        this.id = id;
        this.name = name;
        this.value = value;
        this.urlImg = urlImg;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

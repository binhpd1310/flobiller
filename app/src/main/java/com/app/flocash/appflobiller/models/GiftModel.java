package com.app.flocash.appflobiller.models;

/**
 * Created by lion on 3/16/17.
 */

public class GiftModel {
    String amount;

    public GiftModel(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}

package com.app.flocash.appflobiller.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.CategoryAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.CategoryModel;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

import butterknife.Bind;

/**
 * Created by ${binhpd} on 11/17/2016.
 */
public class CategoryFragment extends FloBillerBaseFragment implements FlobillerInterfaces.OnItemClick {

    private ArrayList<CategoryModel> categoryModels = new ArrayList<>();

    private CategoryAdapter mCategoryAdapter;

    private LinearLayoutManager mLayoutManager;
    @Bind(R.id.rvCategory)
    RecyclerView mRvCategory;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_call_tickets;
    }

    @Override
    public void onCreateViewFragment(View view) {
        mCategoryAdapter = new CategoryAdapter(mActivity, categoryModels);
        mRvCategory.setAdapter(mCategoryAdapter);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRvCategory.setLayoutManager(mLayoutManager);
        mCategoryAdapter.setClickListener(this);
        getCategories();

    }

    public void getCategories() {
        String token = values.getAuthToken();
        JsonParse jsonParse = new JsonParse(JSONObject.class);
        jsonParse.AddParam(ConfigUrlsAPI.Authentication_token, token);

        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_CATEGORIES, jsonParse, this);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_GET_CATEGORIES, JsonParse.REQUEST_TYPE_GET});
    }

    @Override
    public <T> void onSuccess(T result, String method) {
        JSONObject jsonResponse = (JSONObject) result;
        try {
            if (isHaveData(jsonResponse)) {
                switch (method) {
                    case ConfigUrlsAPI.REQUEST_GET_CATEGORIES:
                        getCategoriesFromResponse(jsonResponse);
                        mCategoryAdapter.notifyDataSetChanged();
                        break;
                }
            } else {
                Toast.makeText(mActivity, R.string.error_fail, Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFail(String method) {

    }


    private ArrayList<CategoryModel> getCategoriesFromResponse(JSONObject jsonResponse) throws JSONException {
        categoryModels.clear();
        Type type = new TypeToken<ArrayList<CategoryModel>>(){}.getType();
        Gson gson = new Gson();
        categoryModels.addAll((Collection<? extends CategoryModel>) gson.fromJson(jsonResponse.getString("msg"), type));
        return categoryModels;
    }

    @Override
    public void onItemClick(View view, int index) {
        EventListingFragment eventListingFragment = new EventListingFragment();
        Bundle bundle = new Bundle();
        bundle.putString(FlobillerConstaints.KEY_EXTRA_CATEGORY_TYPE, categoryModels.get(index).getType());
        bundle.putString(FlobillerConstaints.KEY_EXTRA_CATEGORY_NAME, categoryModels.get(index).getName());
        eventListingFragment.setArguments(bundle);
        addFragment(R.id.content, eventListingFragment);
    }
}

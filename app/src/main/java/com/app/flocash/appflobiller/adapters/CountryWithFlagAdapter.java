package com.app.flocash.appflobiller.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.models.CountryResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 3/12/2016.
 */
public class CountryWithFlagAdapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList data;
    private LayoutInflater inflater;
    private String baseUrl;

    public CountryWithFlagAdapter(Activity context, int textViewResourceId, ArrayList data) {
        super(context, textViewResourceId, data);

        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_spinner_flag, parent, false);
            holder = new Holder();
            holder.tvLabel = (TextView) convertView.findViewById(R.id.tvCountryName);
            holder.ivFlag = (ImageView) convertView.findViewById(R.id.ivFlag);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (data.get(position) instanceof CountryResponseModel) {
            CountryResponseModel countryResponseModel = (CountryResponseModel) data.get(position);
            if (countryResponseModel == null) {
                holder.tvLabel.setVisibility(View.INVISIBLE);
            } else {
                holder.tvLabel.setText(countryResponseModel.getName());
                holder.tvLabel.setVisibility(View.VISIBLE);
                Picasso.with(context).load(countryResponseModel.getUrlImg()).into(holder.ivFlag);
            }
        } else {
            String operatorName = (String) data.get(position);
            holder.tvLabel.setText(operatorName);
            if (position == 0) {
                holder.ivFlag.setVisibility(View.INVISIBLE);
            } else {
                holder.ivFlag.setVisibility(View.VISIBLE);
                Picasso.with(context).load((ConfigUrlsAPI.URL_BASE_LOGO + operatorName).replace(" ", "%20")).into(holder.ivFlag);
            }
        }
        return convertView;
    }

    public class Holder {
        TextView tvLabel;
        ImageView ivFlag;
    }
}


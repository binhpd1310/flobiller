package com.app.flocash.appflobiller.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ${binhpd} on 12/4/2015.
 */
public class SavedSharedPreference {
    private static final String SETTING = "SETTING";
    public static final String User_email = "email";
    public static final String User_firstname = "firstname";
    public static final String User_lastname = "lastname";
    public static final String User_mobile = "mobile";
    public static final String User_country = "country";
    public static final String User_dob = "dateofbirth";
    public static final String User_password = "password";
    public static final String Authentication = "auth_token";
    public static final String Country_code = "country_code";
    public static final String Biller_id="biller_id";
    public static final String Subscription_id="subscription_id";
    public static final String Status="status";
    public static final String ACC_NUMBER ="account_number";
    public static final String RID="RID";
    public static final String CUSTOMER_ID="customer_id";
    public static final String CURRENCY="currency";
    public static final String VALID_SERVICE="valid_service";
    public static final String SEE_INTRODUCTION ="see_introduction";
    private static final String LANGUAGE = "language";
    private static final String FIRST_LOGIN = "first_login";

    private Context activity;

    public SavedSharedPreference(Context activity){
        this.activity = activity;
    }


    public void setAuthToken(String auth_token){
        SharedPreferences prefs=activity.getSharedPreferences(Authentication, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor=prefs.edit();
        editor.putString(Authentication, auth_token);
        editor.commit();
    }
    public String getAuthToken(){
        SharedPreferences pref=activity.getSharedPreferences(Authentication, activity.MODE_PRIVATE);
        String auth_token=pref.getString(Authentication, "");
        return auth_token;
    }
    public void setUserEmail(String user_email){
        SharedPreferences prefs = activity.getSharedPreferences(User_email, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_email,user_email);
        editor.commit();
    }

    public String getUserEmail(){
        SharedPreferences prefs = activity.getSharedPreferences(User_email, activity.MODE_PRIVATE);
        String user_email=prefs.getString(User_email, "");
        return user_email;
    }

    public void setUserFirstname(String user_firstname){
        SharedPreferences prefs = activity.getSharedPreferences(User_firstname, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_firstname,user_firstname);
        editor.commit();
    }

    public String getUserFirstname(){
        SharedPreferences prefs = activity.getSharedPreferences(User_firstname, activity.MODE_PRIVATE);
        String user_firstname=prefs.getString(User_firstname, "");
        return user_firstname;
    }
    public void setUserLastname(String user_lastname){
        SharedPreferences prefs = activity.getSharedPreferences(User_lastname, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_lastname,user_lastname);
        editor.commit();
    }

    public String getUserLastname(){
        SharedPreferences prefs = activity.getSharedPreferences(User_lastname, activity.MODE_PRIVATE);
        String user_lastname=prefs.getString(User_lastname, "");
        return user_lastname;
    }

    public void setUserMobile(String user_mobile){
        SharedPreferences prefs = activity.getSharedPreferences(User_mobile, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_mobile,user_mobile);
        editor.commit();
    }

    public String getUserMobile(){
        SharedPreferences prefs = activity.getSharedPreferences(User_mobile, activity.MODE_PRIVATE);
        String user_mobile=prefs.getString(User_mobile, "");
        return user_mobile;
    }
    public void setUserCountry(String user_country){
        SharedPreferences prefs = activity.getSharedPreferences(User_country, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_country,user_country);
        editor.commit();
    }

    public String getUserCountry(){
        SharedPreferences prefs = activity.getSharedPreferences(User_country, activity.MODE_PRIVATE);
        String user_country=prefs.getString(User_country, "");
        return user_country;
    }
    public void setUserDob(String user_dob){
        SharedPreferences prefs = activity.getSharedPreferences(User_dob, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_dob,user_dob);
        editor.commit();
    }

    public String getUserDob(){
        SharedPreferences prefs = activity.getSharedPreferences(User_dob, activity.MODE_PRIVATE);
        String user_dob=prefs.getString(User_dob, "");
        return user_dob;
    }
    public void setUserPassword(String user_password){
        SharedPreferences prefs = activity.getSharedPreferences(User_password, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(User_password,user_password);
        editor.commit();
    }

    public String getUserPassword(){
        SharedPreferences prefs = activity.getSharedPreferences(User_password, activity.MODE_PRIVATE);
        String user_password=prefs.getString(User_password, "");
        return user_password;
    }
    public void setCountryCode(String country_code){
        SharedPreferences prefs = activity.getSharedPreferences(Country_code, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Country_code,country_code);
        editor.commit();
    }

    public String getCountryCode(){
        SharedPreferences prefs = activity.getSharedPreferences(Country_code, activity.MODE_PRIVATE);
        String country_code=prefs.getString(Country_code, "");
        return country_code;
    }
    public void setBillerId(String biller_id){
        SharedPreferences prefs = activity.getSharedPreferences(Biller_id, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Biller_id,biller_id);
        editor.commit();
    }
    public String getBillerId(){
        SharedPreferences prefs = activity.getSharedPreferences(Biller_id, activity.MODE_PRIVATE);
        String biller_id=prefs.getString(Biller_id, "");
        return biller_id;
    }
    public void setStatus(String status){
        SharedPreferences prefs = activity.getSharedPreferences(Status, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Status,status);
        editor.commit();
    }
    public String getStatus(){
        SharedPreferences prefs = activity.getSharedPreferences(Status, activity.MODE_PRIVATE);
        String status=prefs.getString(Status, "");
        return status;
    }
    public void setSubId(String subId){
        SharedPreferences prefs = activity.getSharedPreferences(Subscription_id, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Subscription_id,subId);
        editor.commit();
    }
    public String getSubId(){
        SharedPreferences prefs = activity.getSharedPreferences(Subscription_id, activity.MODE_PRIVATE);
        String subId=prefs.getString(Subscription_id, "");
        return subId;
    }
    public void setAccNo(String accNo){
        SharedPreferences prefs = activity.getSharedPreferences(ACC_NUMBER, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ACC_NUMBER,accNo);
        editor.commit();
    }
    public String getAccNo(){
        SharedPreferences prefs = activity.getSharedPreferences(ACC_NUMBER, activity.MODE_PRIVATE);
        String accNo=prefs.getString(ACC_NUMBER, "");
        return accNo;
    }
    public String getRid() {
        SharedPreferences prefs = activity.getSharedPreferences(RID, activity.MODE_PRIVATE);
        String accNo=prefs.getString(RID,"");
        return accNo;
    }

    public void setRid(String rid) {
        SharedPreferences prefs = activity.getSharedPreferences(RID, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(RID, rid);
        editor.commit();
    }

    public String getCustomerId() {
        SharedPreferences prefs = activity.getSharedPreferences(CUSTOMER_ID, activity.MODE_PRIVATE);
        String accNo=prefs.getString(CUSTOMER_ID, "");
        return accNo;
    }

    public void setCustomerId(String customerId) {
        SharedPreferences prefs = activity.getSharedPreferences(CUSTOMER_ID, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CUSTOMER_ID, customerId);
        editor.commit();
    }


    public String getCurrency() {
        SharedPreferences prefs = activity.getSharedPreferences(CURRENCY, activity.MODE_PRIVATE);
        String accNo=prefs.getString(CURRENCY, "");
        return accNo;
    }

    public void setCurrency(String mCurrency) {
        SharedPreferences prefs = activity.getSharedPreferences(CURRENCY, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CURRENCY, mCurrency);
        editor.commit();
    }

    public String getValidServices() {
        SharedPreferences prefs = activity.getSharedPreferences(VALID_SERVICE, activity.MODE_PRIVATE);
        String accNo=prefs.getString(CURRENCY, "");
        return accNo;
    }

    public void setValidService(String mCurrency) {
        SharedPreferences prefs = activity.getSharedPreferences(VALID_SERVICE, activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CURRENCY, mCurrency);
        editor.commit();
    }

    public boolean isSawIntroduction() {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SEE_INTRODUCTION, false);
    }

    public void setSeeIntroduction(boolean isDone) {
        SharedPreferences preferences = activity.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SEE_INTRODUCTION, isDone);
        editor.commit();
    }

    public String getLanguage() {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        return sharedPreferences.getString(LANGUAGE, "en");
    }

    public void setLanguage(String language) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LANGUAGE, language);
        editor.commit();
    }


    public void clear() {
//        setUserFirstname(null);
//        setUserLastname(null);
//        set
        SharedPreferences preferences = activity.getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public boolean isFirstLogin() {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(FIRST_LOGIN, true);
    }

    public void setFirstLogin(boolean isFirstLogin) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(FIRST_LOGIN, isFirstLogin);
        editor.commit();
    }
}

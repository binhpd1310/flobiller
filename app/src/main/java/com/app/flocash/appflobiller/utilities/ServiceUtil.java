package com.app.flocash.appflobiller.utilities;

import android.content.Context;
import android.text.TextUtils;

/**
 * Created by ${binhpd} on 1/26/2017.
 */

public class ServiceUtil {
    public static final String PAYBILL = "paybill";
    public static final String VOUCHER = "voucher";
    public static final String AIRTIME = "airtime";
    public static final String TICKET = "ticket";

    public static final boolean checkValidService(Context context, String service) {
        SavedSharedPreference savedSharedPreference = new SavedSharedPreference(context);
        String services = savedSharedPreference.getValidServices();
        if (!TextUtils.isEmpty(services)) {
            if (services.contains(service)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}

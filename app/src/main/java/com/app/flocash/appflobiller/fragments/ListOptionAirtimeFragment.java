package com.app.flocash.appflobiller.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.ListingAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.dao.HistoryDataAccess;
import com.app.flocash.appflobiller.models.ItemListing;
import com.app.flocash.appflobiller.models.PayBillHistoryModel;
import com.app.flocash.appflobiller.models.PayBillerItem;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.JsonParse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by ${binhpd} on 8/20/2016.
 */
public class ListOptionAirtimeFragment extends BaseFragment implements ApiConnectTask.FloBillerRequestListener {

    public static final String EXTRA_LOGO = "EXTRA_LOGO";
    public static final String EXTRA_OPERATOR_ID = "EXTRA_OPERATOR_ID";
    public static final String EXTRA_OPERATOR_NAME = "EXTRA_OPERATOR_NAME";

    private ListingAdapter mListingAdapter;
    private ArrayList<ItemListing> mListItems;
    private ArrayList<PayBillHistoryModel> mListHistory;

    @Bind(R.id.lvList)
    ListView mLvList;

    @Bind(R.id.llDoNotHaveSubscription)
    LinearLayout llDoNotHaveSubscription;

    @Bind(R.id.tvNotice)
    TextView mTvNotice;

    @Bind(R.id.btnTryLater)
    Button mBtnTryAgain;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_list;
    }

    @Override
    public void onCreateViewFragment(View view) {
        if (mIsLoad) {
            return;
        }
        mListItems = new ArrayList<>();
        String countryUserProfile = values.getUserCountry();
        retrieveOperators(countryUserProfile);
        readHistory();
    }

    public void retrieveOperators(String countryCode) {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.airtime_invoice_countryIso, countryCode);
        ApiConnectTask js_operators = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_OPERATOR, client, this);
        js_operators.execute(new String[]{ConfigUrlsAPI.airtimeOperators_url, "0"});
    }

    private void readHistory() {
        HistoryDataAccess historyDataAccess = new HistoryDataAccess(mActivity);
        mListHistory = historyDataAccess.getAllHistory();
        // filter item that is will be header
        int size = mListHistory.size();
        if (size > 0) {
            mListHistory.get(0).setHeader(true);
            for (int i = 1; i < size; i++) {
                mListHistory.get(i).setHeader(!mListHistory.get(i).getTimeHistory().equalsIgnoreCase(mListHistory.get(i-1).getTimeHistory()));
            }
        }
    }

    @Override
    public <T> void onSuccess(T result, String method) {
        final JSONObject resultJson = (JSONObject) result;
        if (result == null || resultJson.length() == 0) {
            return;
        }
        switch (method) {
            case ConfigUrlsAPI.REQUEST_GET_OPERATOR:
                try {
                    JSONObject result_array1 = resultJson.getJSONObject(ConfigUrlsAPI.PARA_OPERATORS);
                    String operators = result_array1.getString(ConfigUrlsAPI.PARA_OPERATOR);
                    String operatorId = result_array1.getString(ConfigUrlsAPI.PARA_OPERATOR_ID);
                    String logoUrl = result_array1.getString(ConfigUrlsAPI.PARA_LOGO);
                    ArrayList<String> operatorNames = new ArrayList<>();
                    ArrayList<String> operatorIds = new ArrayList<>();
                    ArrayList<String> logoUrls = new ArrayList<>();

                    for (String parts : operators.split(",")) {
                        operatorNames.add(parts);
                    }

                    for (String parts1 : operatorId.split(",")) {
                        operatorIds.add(parts1);
                        logoUrls.add(logoUrl.replace("{ID}", parts1));
                    }

                    for (int i = 0; i < operatorNames.size(); i++) {
                        PayBillerItem payBillerItem = new PayBillerItem();
                        payBillerItem.setBillerId(operatorIds.get(i));
                        payBillerItem.setName(operatorNames.get(i));
                        payBillerItem.setLogo(logoUrls.get(i));

                        for (PayBillHistoryModel payBillHistoryModel : mListHistory) {
                            if (payBillerItem.getName().equals(payBillHistoryModel.getSubscriptionName())) {
                                String time = FlobillerConstaints.TIME_PAID.format(
                                        FlobillerConstaints.TIME_VISIT_FORMAT.parseObject(payBillHistoryModel.getTimeHistory()));
                                payBillerItem.setLastPaid("You last paid " + payBillHistoryModel.getCurrencyCode() + payBillHistoryModel.getAmount() +
                                        " on " + time);
                                break;
                            }
                        }
                        mListItems.add(payBillerItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (mListItems.size() == 0) {
                    llDoNotHaveSubscription.setVisibility(View.VISIBLE);
//                    mBtnTryAgain.setVisibility(View.VISIBLE);
                    mLvList.setVisibility(View.GONE);
                    mTvNotice.setText(getString(R.string.dont_have_operators));
                }

                mListingAdapter = new ListingAdapter(mActivity, mListItems);
                mLvList.setAdapter(mListingAdapter);
                break;
        }
    }

    @OnItemClick(R.id.lvList)
    public void onClickItemList(AdapterView<?> parent, View view, int position, long id) {
        AirtimeFragment airtimeFragment = new AirtimeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_LOGO, mListItems.get(position).getLogo());
        bundle.putString(EXTRA_OPERATOR_ID, ((PayBillerItem)mListItems.get(position)).getBillerId());
        bundle.putString(EXTRA_OPERATOR_NAME, mListItems.get(position).getName());
        airtimeFragment.setArguments(bundle);
        replaceFragment(R.id.content, airtimeFragment);
    }

    @OnClick({R.id.btnTryLater})
    public void onClick(View view) {
        String countryUserProfile = values.getUserCountry();
        retrieveOperators(countryUserProfile);
    }
    @Override
    public void onFail(String method) {

    }
}

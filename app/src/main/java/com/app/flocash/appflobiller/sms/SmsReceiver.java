package com.app.flocash.appflobiller.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by lion on 5/20/17.
 */

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from;
            String msgBody;
            if (bundle != null) {
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];

                    for(int i=0; i<msgs.length; i++){
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                            msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        } else {
                            msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        }
                        msg_from = msgs[i].getOriginatingAddress();
                        msgBody = msgs[i].getMessageBody();
                        Toast.makeText(context, msg_from + ": " + msgBody, Toast.LENGTH_LONG).show();

                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}

package com.app.flocash.appflobiller.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.utilities.KeyboardUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;

import butterknife.ButterKnife;

/**
 * Created by ${binhpd} on 1/12/2016.
 */
public abstract class BaseFragment extends Fragment {
    protected Activity mActivity;
    protected SavedSharedPreference values;
    protected boolean mIsLoad = false;
    protected View contentView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity = getActivity();
        if (contentView != null) {
            return contentView;
        }
        contentView = inflater.inflate(getContentIdFragment(), null);
        ButterKnife.bind(this, contentView);
        KeyboardUtils.hideKeyboard(mActivity, contentView);
        values = new SavedSharedPreference(mActivity);
        onCreateViewFragment(contentView);
        mIsLoad = true;
        return contentView;
    }

    abstract public int getContentIdFragment();

    abstract public void onCreateViewFragment(View view);

    public void replaceFragment(int pLayoutResId, Fragment pFragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
        final String backStateName = pFragment.getClass().getName();
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.add(pLayoutResId, pFragment, backStateName);
        fragmentTransaction.commit();
    }

    public void finishFragment() {
        int backStackCount = getFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0) {
            getFragmentManager().popBackStack();
            if (backStackCount == 1) {
                getActivity().finish();
            }
        } else {
            getActivity().finish();
        }
    }

    public void addFragment(int contentId, Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        String nameFragment = fragment.getClass().toString();
        fragmentTransaction.setCustomAnimations(
                R.anim.anim_right_in, R.anim.anim_left_out,
                R.anim.anim_left_in, R.anim.anim_right_out).addToBackStack(nameFragment);
        fragmentTransaction.replace(contentId, fragment, nameFragment);
        fragmentTransaction.commit();
    }

    public Fragment findFragmentByTag(Class<HomeFragment> fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        return fragmentManager.findFragmentByTag(fragment.getClass().getName());
    }

    protected void updateTitle(String title) {
        ((SubScreenActivity)mActivity).updateTitle(title);
    }
}

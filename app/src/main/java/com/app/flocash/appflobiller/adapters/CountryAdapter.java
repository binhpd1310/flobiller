package com.app.flocash.appflobiller.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

import com.app.flocash.appflobiller.R;

import com.app.flocash.appflobiller.models.CountryModel;

/**
 * Created by TuanMobile on 11/15/2015.
 */
public class CountryAdapter extends ArrayAdapter<String> {
    private Activity activity;
    private ArrayList data;
    CountryModel tempValues = null;
    LayoutInflater inflater;

    /*************
     * CustomAdapter Constructor
     *****************/
    public CountryAdapter(Activity activitySpinner, int textViewResourceId, ArrayList objects) {
        super(activitySpinner, textViewResourceId, objects);

        /********** Take passed values **********/
        activity = activitySpinner;
        data = objects;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.row_spinner, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (CountryModel) data.get(position);
        TextView label = (TextView) row.findViewById(R.id.tvCountryName);
        label.setText(tempValues.getValue());

        return row;
    }
}
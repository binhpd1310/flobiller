package com.app.flocash.appflobiller.models;

/**
 * Created by ${binhpd} on 8/18/2016.
 */
public class ItemListing {
    protected String logo;
    protected String name;
    protected String lastPaid;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String billerLogo) {
        this.logo = billerLogo;
    }

    public String getName() {
        return name;
    }

    public void setName(String billerName) {
        this.name = billerName;
    }

    public String getLastPaid() {
        return lastPaid;
    }

    public void setLastPaid(String lastPaid) {
        this.lastPaid = lastPaid;
    }
}

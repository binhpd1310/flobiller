package com.app.flocash.appflobiller.models;

/**
 * Created by ${binhpd} on 3/16/2016.
 */
public class PayBillHistoryModel {
    private String billerName;
    private String timeVisit;
    private String urlLogo;
    private String invoiceId;
    private String accountName;
    private String accountNumber;
    private String amount;
    private boolean header;
    private String countryCode;
    private String countryName;
    private String currencyCode;
    private String billerId;
    private String subscriptionId;
    private String subscriptionName;

    public PayBillHistoryModel() {}

    public PayBillHistoryModel(String billerName, String timeVisit, String urlLogo, String invoiceId, String accountName, String accountNumber, String amount) {
        this.billerName = billerName;
        this.timeVisit = timeVisit;
        this.urlLogo = urlLogo;
        this.invoiceId = invoiceId;
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getTimeHistory() {
        return timeVisit;
    }

    public void setTimeVisit(String timeVisit) {
        this.timeVisit = timeVisit;
    }

    public String getUrlLogo() {
        return urlLogo;
    }

    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public String getTimeVisit() {
        return timeVisit;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }
}

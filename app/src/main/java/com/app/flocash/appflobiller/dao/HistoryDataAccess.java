package com.app.flocash.appflobiller.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.app.flocash.appflobiller.models.PayBillHistoryModel;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 3/16/2016.
 */
public class HistoryDataAccess {
    private static final int PROJECTION_BILLER_NAME = 0;
    private static final int PROJECTION_TIME_HISTORY = 1;
    private static final int PROJECTION_URL_LOGO = 2;
    private static final int PROJECTION_INVOICE_ID = 3;
    private static final int PROJECTION_ACCOUNT_NAME = 4;
    private static final int PROJECTION_ACCOUNT_NUMBER = 5;
    private static final int PROJECTION_AMOUNT = 6;
    private static final int PROJECTION_COUNTRY_CODE = 7;
    private static final int PROJECTION_COUNTRY_NAME = 8;
    private static final int PROJECTION_CURRENCY_CODE = 9;
    private static final int PROJECTION_BILLER_ID = 10;
    private static final int PROJECTION_SUBSCRIPTION_ID = 11;
    private static final int PROJECTION_SUBSCRIPTION_NAME = 12;
    private static final int MAX_RECORD = 10;

    private final DbHelper mDbHelper;
    private SQLiteDatabase db;

    public static final String[] PROJECTION_HISTORY = new String[] {
            HistoryTable.COLUMN_BILLER_NAME,                  // 0
            HistoryTable.COLUMN_TIME_HISTORY,
            HistoryTable.COLUMN_URL_LOGO,                  // 1
            HistoryTable.COLUMN_INVOICE_ID,
            HistoryTable.COLUMN_ACCOUNT_NAME,
            HistoryTable.COLUMN_ACCOUNT_NUMBER,
            HistoryTable.COLUMN_AMOUNT,
            HistoryTable.COLUMN_COUNTRY_CODE,
            HistoryTable.COLUMN_COUNTRY_NAME,
            HistoryTable.COLUMN_CURRENCY_CODE,
            HistoryTable.COLUMN_BILLER_ID,
            HistoryTable.COLUMN_SUBSCRIPTION_ID,
            HistoryTable.COLUMN_SUBSCRIPTION_NAME
    };

    public static final String[] PROJECTION = new String[] {
            HistoryTable.COLUMN_BILLER_NAME,                  // 0
            HistoryTable.COLUMN_URL_LOGO
    };

    public HistoryDataAccess(Context context) {
        mDbHelper = new DbHelper(context);
    }

    private void open() {
        db = mDbHelper.getWritableDatabase();
    }


    private void close() {
        db.close();
    }

    //--------------

    /**
     * get all history visit file of use
     */
    public ArrayList<PayBillHistoryModel> getAllHistory() {
        open();
        ArrayList<PayBillHistoryModel> historyModels = new ArrayList<>();
        String orderByTime;
        orderByTime = HistoryTable.COLUMN_TIME_HISTORY +" DESC ";
        Cursor cursor = db.query(HistoryTable.TABLE_NAME, PROJECTION_HISTORY, null, null, null, null, orderByTime);
        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                historyModels.add(cursorToHistoryModel(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        close();
        return historyModels;
    }

    /**
     *
     * @param cursor convert cursor to Event
     * @return
     */
    private PayBillHistoryModel cursorToHistoryModel(Cursor cursor) {
        PayBillHistoryModel payBillHistoryModel = new PayBillHistoryModel();
        payBillHistoryModel.setBillerName(cursor.getString(PROJECTION_BILLER_NAME));
        payBillHistoryModel.setTimeVisit(cursor.getString(PROJECTION_TIME_HISTORY));
        payBillHistoryModel.setUrlLogo(cursor.getString(PROJECTION_URL_LOGO));
        payBillHistoryModel.setInvoiceId(cursor.getString(PROJECTION_INVOICE_ID));
        payBillHistoryModel.setAccountName(cursor.getString(PROJECTION_ACCOUNT_NAME));
        payBillHistoryModel.setAccountNumber(cursor.getString(PROJECTION_ACCOUNT_NUMBER));
        payBillHistoryModel.setAmount(cursor.getString(PROJECTION_AMOUNT));
        payBillHistoryModel.setCountryCode(cursor.getString(PROJECTION_COUNTRY_CODE));
        payBillHistoryModel.setCountryName(cursor.getString(PROJECTION_COUNTRY_NAME));
        payBillHistoryModel.setCurrencyCode(cursor.getString(PROJECTION_CURRENCY_CODE));
        payBillHistoryModel.setBillerId(cursor.getString(PROJECTION_BILLER_ID));
        payBillHistoryModel.setSubscriptionId(cursor.getString(PROJECTION_SUBSCRIPTION_ID));
        payBillHistoryModel.setSubscriptionName(cursor.getString(PROJECTION_SUBSCRIPTION_NAME));
        return payBillHistoryModel;
    }

    /**
     * delete record
     */
    public void deleteHistory() {
        open();
        db.delete(HistoryTable.TABLE_NAME, null, null);
        close();
    }

    /**
     * count all records in table recently
     * @return
     */
    public int countRecords() {
        Cursor cursor;
        int size = 0;
        if (db != null && db.isOpen()) {
            cursor = db.query(HistoryTable.TABLE_NAME, new String[]{HistoryTable.COLUMN_BILLER_NAME}, null, null, null, null, null );
            if (cursor != null)  {
                size = cursor.getCount();
            }
            cursor.close();
        }
        return size;
    }

    /**
     * remove record that is oldest document visit
     */
    public void removeOldestRecord() {
        String selectQuery = HistoryTable.COLUMN_TIME_HISTORY +
                " = (select min([" + HistoryTable.COLUMN_TIME_HISTORY + "]) from " + HistoryTable.TABLE_NAME + ")";
        if (db != null && db.isOpen()) {
            db.delete(HistoryTable.TABLE_NAME, selectQuery, null);
        }
    }

    public void addHistory(PayBillHistoryModel cpoDataModel) {
        open();
            if (countRecords() >= MAX_RECORD) {
                removeOldestRecord();
            }
            insertRecord(cpoDataModel);
        close();
    }

    /**
     * insert new record
     */
    private void insertRecord(PayBillHistoryModel history) {
        ContentValues values = new ContentValues();
        values.put(HistoryTable.COLUMN_BILLER_NAME, history.getBillerName());
        values.put(HistoryTable.COLUMN_TIME_HISTORY, history.getTimeHistory());
        values.put(HistoryTable.COLUMN_URL_LOGO, history.getUrlLogo());
        values.put(HistoryTable.COLUMN_INVOICE_ID, history.getInvoiceId());
        values.put(HistoryTable.COLUMN_ACCOUNT_NAME, history.getAccountName());
        values.put(HistoryTable.COLUMN_ACCOUNT_NUMBER, history.getAccountNumber());
        values.put(HistoryTable.COLUMN_AMOUNT, history.getAmount());
        values.put(HistoryTable.COLUMN_COUNTRY_CODE, history.getCountryCode());
        values.put(HistoryTable.COLUMN_COUNTRY_NAME, history.getCountryName());
        values.put(HistoryTable.COLUMN_CURRENCY_CODE, history.getCurrencyCode());
        values.put(HistoryTable.COLUMN_BILLER_ID, history.getBillerId());
        values.put(HistoryTable.COLUMN_SUBSCRIPTION_ID, history.getSubscriptionId());
        values.put(HistoryTable.COLUMN_SUBSCRIPTION_NAME, history.getSubscriptionName());
        db.insert(HistoryTable.TABLE_NAME, null, values);
    }

    /**
     * Class defined Table Recently
     */
    public static abstract class HistoryTable implements BaseColumns {
        public static final String TABLE_NAME = "history";
        public static final String COLUMN_BILLER_NAME = "biller_name";
        public static final String COLUMN_TIME_HISTORY = "time_biller";
        public static final String COLUMN_URL_LOGO = "url_logo";
        public static final String COLUMN_INVOICE_ID = "invoice_id";
        public static final String COLUMN_ACCOUNT_NAME = "account_name";
        public static final String COLUMN_ACCOUNT_NUMBER = "account_number";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_COUNTRY_CODE = "countryCode";
        public static final String COLUMN_COUNTRY_NAME = "countryName";
        public static final String COLUMN_CURRENCY_CODE = "currencyCode";
        public static final String COLUMN_BILLER_ID = "billerId";
        public static final String COLUMN_SUBSCRIPTION_ID = "subscriptionId";
        public static final String COLUMN_SUBSCRIPTION_NAME = "subscriptionName";

    }
}

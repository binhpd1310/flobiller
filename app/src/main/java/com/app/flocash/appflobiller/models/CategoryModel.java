package com.app.flocash.appflobiller.models;

/**
 * Created by ${binhpd} on 11/17/2016.
 */

public class CategoryModel {
    String type;
    String name;
    String logo;

    public CategoryModel(){}

    public CategoryModel(String field, String value, String logo) {
        this.type = field;
        this.name = value;
        this.logo = logo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}

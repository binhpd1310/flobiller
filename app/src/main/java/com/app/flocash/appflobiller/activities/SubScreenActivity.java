package com.app.flocash.appflobiller.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.fragments.AirtimeFragment;
import com.app.flocash.appflobiller.fragments.AppInfoFragment;
import com.app.flocash.appflobiller.fragments.BillerFragment;
import com.app.flocash.appflobiller.fragments.CategoryFragment;
import com.app.flocash.appflobiller.fragments.CreateSubcribleFragment;
import com.app.flocash.appflobiller.fragments.GiftCardFragment;
import com.app.flocash.appflobiller.fragments.HomeFragment;
import com.app.flocash.appflobiller.fragments.InvoiceFragment;
import com.app.flocash.appflobiller.fragments.ListOptionAirtimeFragment;
import com.app.flocash.appflobiller.fragments.ListOptionPayBillFragment;
import com.app.flocash.appflobiller.fragments.PayBillFragment;
import com.app.flocash.appflobiller.fragments.PayNowFragment;
import com.app.flocash.appflobiller.fragments.PaymentResultFragment;
import com.app.flocash.appflobiller.fragments.PrivacyFragment;
import com.app.flocash.appflobiller.fragments.ProfileFragment;
import com.app.flocash.appflobiller.fragments.SubscriptionFragment;
import com.app.flocash.appflobiller.fragments.UpdateSubscribeFragment;
import com.app.flocash.appflobiller.fragments.VirtualTerminalFragment;
import com.app.flocash.appflobiller.utilities.LogUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ${binhpd} on 12/3/2015.
 */
public class SubScreenActivity extends AppCompatActivity {
    private Fragment fragment;
    private int screenId;

    @Bind(R.id.tvTitle)
    TextView mTvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_base);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            screenId = bundle.getInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAY_BILL);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_back);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            fragment = new PayBillFragment();
            String backstackStateName = HomeFragment.class.getName();
            switch (screenId) {
                case FlobillerConstaints.SCREEN_PAY_BILL:
//                    fragment = new PayBillFragment();
                    fragment = new ListOptionPayBillFragment();
                    bundle = new Bundle();
                    bundle.putInt(ListOptionPayBillFragment.EXTRA_STEP, 0);
                    fragment.setArguments(bundle);
                    backstackStateName = PayNowFragment.class.getName();
                    mTvTitle.setText(getString(R.string.title_pay_bill).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_AIRTIME:
                    fragment = new ListOptionAirtimeFragment();
                    backstackStateName = ListOptionAirtimeFragment.class.getName();
                    mTvTitle.setText(getString(R.string.title_airtime).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_SUBSCRIPTION:
                    fragment = new SubscriptionFragment();
                    backstackStateName = SubscriptionFragment.class.getName();
                    mTvTitle.setText(getString(R.string.title_subcriptions).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_INVOICES:
                    fragment = new InvoicesFragment();
                    fragment.setArguments(bundle);
                    backstackStateName = InvoicesFragment.class.getName();
                    mTvTitle.setText(getString(R.string.title_invoices).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_EDIT:
                    break;
                case FlobillerConstaints.SCREEN_PROFILE:
                    fragment = new ProfileFragment();
                    mTvTitle.setText(getString(R.string.title_profile).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_BILLER:
                    backstackStateName = BillerFragment.class.getName();
                    mTvTitle.setText(getString(R.string.title_billers).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_CREATE_SUBCRIBLE:
                    fragment = new CreateSubcribleFragment();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.title_subcrible).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_UPDATE_SUBCRIBLE:
                    fragment = new UpdateSubscribeFragment();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.title_subcrible).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_PAYMENT:
                    fragment = new InvoiceFragment();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.title_payment).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_VIRTUAL_TERMINAL:
                    fragment = new VirtualTerminalFragment();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.virtual_terminal).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_PAYMENT_RESULT_FRAGMENT:
                    fragment = new PaymentResultFragment();
                    backstackStateName = PaymentResultFragment.class.getName();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.payment_result).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_ABOUT_APP:
                    fragment = new AppInfoFragment();
                    backstackStateName = AppInfoFragment.class.getName();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.title_about).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_PRIVACY:
                    fragment = new PrivacyFragment();
                    fragment.setArguments(bundle);
                    mTvTitle.setText(getString(R.string.title_privacy).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_GIFT_CARD:
                    fragment = new GiftCardFragment();
                    mTvTitle.setText(getString(R.string.title_gift_cards).toUpperCase());
                    break;
                case FlobillerConstaints.SCREEN_CALLED_TICKETS:
                    fragment = new CategoryFragment();
                    mTvTitle.setText(getString(R.string.title_tickets).toUpperCase());
                    break;
            }
            addFragment(R.id.content, fragment);
        }

    }

    protected void addFragment(int layoutResource, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.replace(layoutResource, fragment, fragment.getClass().getName());
        fragmentTransaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            Fragment topFragment = getCurrentFragment();
            if (topFragment instanceof PayBillFragment || topFragment instanceof AirtimeFragment) {
                finish();
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();
            }
        } else {
            fragment.onOptionsItemSelected(menuItem);
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (screenId == FlobillerConstaints.SCREEN_PROFILE||
                screenId == FlobillerConstaints.SCREEN_UPDATE_SUBCRIBLE ||
                screenId == FlobillerConstaints.SCREEN_CREATE_SUBCRIBLE) {
            getMenuInflater().inflate(R.menu.menu_profile, menu);
        }
        return true;
    }

    private Fragment getCurrentFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = getSupportFragmentManager()
                .findFragmentByTag(fragmentTag);
        return currentFragment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.d("onActivity result");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void updateTitle(String title) {
        mTvTitle.setText(title);
    }
}

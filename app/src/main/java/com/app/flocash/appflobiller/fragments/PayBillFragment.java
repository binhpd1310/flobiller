package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.adapters.HistoryAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.dao.HistoryDataAccess;
import com.app.flocash.appflobiller.models.PayBillHistoryModel;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.KeyboardUtils;
import com.app.flocash.appflobiller.views.ExpandedListView;
import com.flocash.core.service.entity.MerchantInfo;
import com.flocash.core.service.entity.OrderInfo;
import com.flocash.core.service.entity.PayerInfo;
import com.flocash.core.service.entity.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by ${binhpd} on 12/5/2015.
 */
public class PayBillFragment extends PayNowFragment {

    @Bind(R.id.tvCurrency)
    TextView mTvCurrency;

    @Bind(R.id.edtAccountName)
    EditText mEdtEnterAccountName;

    @Bind(R.id.edtEnterAccountNumber)
    EditText mEdtEnterAccountNumber;

    @Bind(R.id.edtEnterVoiceNumber)
    EditText mEdtEnterVoiceNumber;

    @Bind(R.id.npAmount)
    EditText mEdtAmount;

    @Bind(R.id.ivFlag)
    ImageView mIvFlag;

    @Bind(R.id.ivFlagBill)
    ImageView mIvFlagBill;

    @Bind(R.id.tvCountryName)
    TextView mTvCountryName;

    @Bind(R.id.tvBillerName)
    TextView mTvBillerName;

    @Bind(R.id.tvSubscriptionName)
    TextView mTvSubscriptionName;

    @Bind(R.id.lvHistory)
    ExpandedListView mLvHistory;

    @Bind(R.id.tv_title_select_from_previous)
    TextView mTvTitleSelectFromPrevious;

    private String mFirstName;
    private String mCountry;
    private String mLastName;
    private String mMobileNumber;
    private String mEmail;
    private String mInvoiceId;
    private String mCountryCode, countryName, mBillerId, mBillerName, mUrlLogo, subscriptionId, subScriptionName, mCurrencyCode;
    private HistoryAdapter mHistoryAdapter;
    private ArrayList<PayBillHistoryModel> mListHistory;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_pay_bill;
    }

    @Override
    public void onCreateViewFragment(View view) {
        super.onCreateViewFragment(view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mBillerId = bundle.getString(ListOptionPayBillFragment.EXTRA_BILLER_ID);
            mBillerName = bundle.getString(ListOptionPayBillFragment.EXTRA_BILLER_NAME);
            subscriptionId = bundle.getString(ListOptionPayBillFragment.EXTRA_SUBSCRIPTION_ID);
            subScriptionName = bundle.getString(ListOptionPayBillFragment.EXTRA_SUBSCRIPTION_NAME);
            mUrlLogo = bundle.getString(ListOptionPayBillFragment.EXTRA_LOGO);
        }
        initValue();
        mCountryCode = values.getUserCountry();
        mCurrencyCode = values.getCurrency();

        mTvCountryName.setText(getNameCountry(mCountryCode));
        Picasso.with(mActivity).load(ConfigUrlsAPI.BASE_URL_FLAG + mCountryCode + ".png").into(mIvFlag);

        mTvBillerName.setText(mBillerName);
        Picasso.with(mActivity).load(mUrlLogo).into(mIvFlagBill);

        mTvSubscriptionName.setText(subScriptionName);

        mTvCurrency.setText(mCurrencyCode);
    }

    private void initValue() {

        HistoryDataAccess historyDataAccess = new HistoryDataAccess(mActivity);
        mListHistory = historyDataAccess.getAllHistory();
        // filter item that is will be header
        int size = mListHistory.size();
        if (size > 0) {
            mListHistory.get(0).setHeader(true);
            for (int i = 1; i < size; i++) {
                mListHistory.get(i).setHeader(!mListHistory.get(i).getTimeHistory().equalsIgnoreCase(mListHistory.get(i-1).getTimeHistory()));
            }
        } else {
            mTvTitleSelectFromPrevious.setVisibility(View.GONE);
        }
        mHistoryAdapter = new HistoryAdapter(mActivity, mListHistory);
        mLvHistory.setAdapter(mHistoryAdapter);
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        final JSONObject result = (JSONObject) data;
        if (result != null && result.length() > 0) {
            switch (method) {
                case ConfigUrlsAPI.REQUEST_ADD_INVOICE:
                    receiveInvoiceResponse(result);
                    break;
                case ConfigUrlsAPI.REQUEST_GET_USER_INFO:
                    try {
                        JSONObject obj = result.getJSONObject("user");
                        mFirstName = obj.getString("firstname");
                        mLastName = obj.getString("lastname");
                        mMobileNumber = obj.getString("mobile");
                        mCountry = obj.getString(ConfigUrlsAPI.PARA_COUNTRY);
                        mEmail = values.getUserEmail();
                        getInvoice(mInvoiceId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
                    }
                    break;
                case ConfigUrlsAPI.REQUEST_GET_INVOICE:
                    // card payment
                    mProgressDialog.dismiss();
                    OrderInfo order1 = new OrderInfo();
                    PayerInfo payer1 = new PayerInfo();
                    try {
                        JSONObject orderInfo = result.getJSONObject("order").getJSONObject("invoice");
                        order1.setAmount(new BigDecimal(orderInfo.getString(ConfigUrlsAPI.PARA_AMOUNT)));
                        order1.setCurrency(orderInfo.getString("currency"));
                        order1.setItem_name(orderInfo.getString(ConfigUrlsAPI.PARA_NAME));
                        order1.setItem_price(orderInfo.getString(ConfigUrlsAPI.PARA_AMOUNT));
                        order1.setOrderId(result.getString(ConfigUrlsAPI.PARA_ORDER_ID));
                        order1.setQuantity("1");
                        payer1.setCountry(mCountry);
                        payer1.setFirstName(mFirstName);
                        payer1.setLastName(mLastName);
                        payer1.setEmail(mEmail);
                        payer1.setMobile(mMobileNumber);
                        MerchantInfo merchant1 = new MerchantInfo();
                        Request request1 = new Request();
                        request1.setOrder(order1);
                        request1.setPayer(payer1);
                        request1.setMerchant(merchant1);
                        merchant1.setMerchantAccount(InvoiceFragment.MERCHANT_ACCOUNT);

                        // fill information invoice
                        mActivity.finish();
                        openPayGate(result.getString(ConfigUrlsAPI.PARA_ORDER_ID));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
                    }
                    break;
            }
        }
    }

    private void receiveInvoiceResponse(JSONObject result) {
        try {
            String status = result.getString(ConfigUrlsAPI.PARA_STATUS);
            if (status.equalsIgnoreCase(ConfigUrlsAPI.RESULT_SUCCESS)) {

                // save to history
                mInvoiceId = result.getJSONObject(ConfigUrlsAPI.PARA_INVOICE).getString(ConfigUrlsAPI.PARA_INVOICE);
                HistoryDataAccess historyDataAccess = new HistoryDataAccess(mActivity);
                PayBillHistoryModel payBillHistoryModel = new PayBillHistoryModel();
                payBillHistoryModel.setBillerName(mBillerName);
                Calendar calendar = Calendar.getInstance();
                payBillHistoryModel.setTimeVisit(FlobillerConstaints.TIME_VISIT_FORMAT.format(calendar.getTime()));
                payBillHistoryModel.setUrlLogo(mUrlLogo);
                payBillHistoryModel.setInvoiceId(mBillerId);
                payBillHistoryModel.setAccountName(mEdtEnterAccountName.getText().toString());
                payBillHistoryModel.setAccountNumber(mEdtEnterAccountNumber.getText().toString());
                payBillHistoryModel.setAmount(mEdtAmount.getText().toString());
                payBillHistoryModel.setCountryCode(mCountryCode);
                payBillHistoryModel.setCountryName(countryName);
                payBillHistoryModel.setCurrencyCode(mCurrencyCode);
                payBillHistoryModel.setBillerId(mBillerId);
                payBillHistoryModel.setSubscriptionId(subscriptionId);
                payBillHistoryModel.setSubscriptionName(subScriptionName);
                historyDataAccess.addHistory(payBillHistoryModel);
                // end add to history
                refreshPage();

                retrieve_user();
            } else if (ConfigUrlsAPI.result_error.equalsIgnoreCase(status)) {
                mProgressDialog.dismiss();
                String errMsg = result.getString(ConfigUrlsAPI.PARA_MESSAGE);
                DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, errMsg).show();
            }

        } catch (JSONException e) {
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onFail(String method) {
        mProgressDialog.dismiss();
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }

    @OnClick({R.id.btnPayBillNow})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPayBillNow:
                if (TextUtils.isEmpty(mEdtEnterAccountName.getText())
                        || TextUtils.isEmpty(mEdtEnterAccountNumber.getText())
                        || TextUtils.isEmpty(mEdtAmount.getText().toString())) {

                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, mActivity.getString(R.string.notify_fields_required)).show();
                } else {
                    KeyboardUtils.hideKeyboard(mActivity);
                    createInvoice();
                }
                break;
        }
    }

    public void createInvoice() {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_api, ConfigUrlsAPI.API_PAYBILL);

        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_currency, mCurrencyCode);
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_amount, mEdtAmount.getText().toString());
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_country, mCountryCode);
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_accName, mEdtEnterAccountName.getText().toString());
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_accNumber, mEdtEnterAccountNumber.getText().toString());
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_topUpName, mBillerName);
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_topup, mEdtEnterVoiceNumber.getText().toString());
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_invoiceNumber, subscriptionId);
        client.AddParam(ConfigUrlsAPI.add_paybill_invoice_billerId, mBillerId);

        mProgressDialog.show();
        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_ADD_INVOICE, client, this);
        apiConnectTask.setShowDialog(false);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.add_invoice_url, JsonParse.REQUEST_TYPE_POST });
    }

    public void retrieve_user() {
        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_USER_INFO, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.user_retrieve_url, JsonParse.REQUEST_TYPE_GET});
    }

    public void getInvoice(String orderId) {
        String token = values.getAuthToken();
        // String subId = values.getSubId();
        Log.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        client.AddParam(ConfigUrlsAPI.INVOICE_ORDER_ID, orderId);

        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_INVOICE, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.API_GET_INVOICE, JsonParse.REQUEST_TYPE_GET });
    }

    public void refreshPage() {
        mEdtAmount.setText("");
        mEdtEnterAccountName.setText("");
        mEdtEnterVoiceNumber.setText("");
        mEdtEnterAccountNumber.setText("");
    }

    private void openPayGate(String invoiceId) {
        Intent intent = new Intent(mActivity, SubScreenActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT);
        bundle.putString(FlobillerConstaints.KEY_EXTRA_ORDER_ID, invoiceId);
        intent.putExtras(bundle);
        startActivity(intent);

        mActivity.finish();
    }

    @OnItemClick(R.id.lvHistory)
    public void onItemCLick(AdapterView<?> parent, View view, int index, long id) {
        PayBillHistoryModel payBillHistoryModel = mListHistory.get(index);
        // show layout
//        mLlBillerSpinner.setVisibility(View.VISIBLE);
//        mLlSubscriptionSpinner.setVisibility(View.VISIBLE);
//        llFields.setVisibility(View.VISIBLE);
        // populate
        mCountryCode = payBillHistoryModel.getCountryCode();
        mBillerName = payBillHistoryModel.getBillerName();
        mBillerId = payBillHistoryModel.getBillerId();
        mCurrencyCode = payBillHistoryModel.getCurrencyCode();

        Picasso.with(mActivity).load(ConfigUrlsAPI.BASE_URL_FLAG + mCountryCode + ".png").into(mIvFlag);
        Picasso.with(mActivity).load(payBillHistoryModel.getUrlLogo()).into(mIvFlagBill);
        mTvCountryName.setText(payBillHistoryModel.getCountryName());
        mTvBillerName.setText(payBillHistoryModel.getBillerName());
        mTvSubscriptionName.setText(payBillHistoryModel.getSubscriptionName());
        mEdtEnterAccountName.setText(payBillHistoryModel.getAccountName());
        mEdtEnterAccountNumber.setText(payBillHistoryModel.getAccountNumber());
        mEdtAmount.setText(payBillHistoryModel.getAmount());
        mTvCurrency.setText(mCurrencyCode);
        subscriptionId = payBillHistoryModel.getSubscriptionId();
    }
}

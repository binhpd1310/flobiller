package com.app.flocash.appflobiller.adapters;

///**
// * Created by ${binhpd} on 3/17/2016.
// */
//public class HistoryWithTitleAdapter extends BaseAdapter implements StickyListHeadersAdapter, Filterable {
//    private Context context;
//    private ArrayList<PayBillHistoryModel> payBillHistoryModels = new ArrayList<>();
//    private ArrayList<PayBillHistoryModel> payBillHistoryModelsFilter = new ArrayList<>();
//
//    private final LayoutInflater mInflater;
//    private ItemFilter mFilter = new ItemFilter();
//
//    public HistoryWithTitleAdapter(Context context, ArrayList<PayBillHistoryModel> listInvoices) {
//        this.context = context;
//        this.payBillHistoryModels = payBillHistoryModelsFilter = listInvoices;
//        this.mInflater = ((Activity) context).getLayoutInflater();
//    }
//
//    @Override
//    public int getCount() {
//        return payBillHistoryModels.size();
//    }
//
//    @Override
//    public PayBillHistoryModel getItem(int position) {
//        return payBillHistoryModelsFilter.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHoler holder;
//        if (convertView==null){
//            convertView = mInflater.inflate(R.layout.item_history, parent,false);
//            holder = new ViewHoler();
//            holder.tvBillerName = (TextView)convertView.findViewById(R.id.tvBillerName);
//            holder.tvTime = (TextView)convertView.findViewById(R.id.tvTime);
//            convertView.setTag(holder);
//
//        } else{
//            holder = (ViewHoler) convertView.getTag();
//        }
//        PayBillHistoryModel payBillHistoryModel = payBillHistoryModels.get(position);
//        holder.tvBillerName.setText(payBillHistoryModel.getBillerName());
//        try {
//            holder.tvTime.setText(FlobillerConstaints.TIME_VISIT_FORMAT_DISPLAY.format(
//                    FlobillerConstaints.TIME_VISIT_FORMAT.parse(payBillHistoryModel.getTimeHistory())));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return convertView;
//    }
//
//    @Override
//    public View getHeaderView(int position, View convertView, ViewGroup parent) {
//        HeaderViewHolder holder;
//        if (convertView == null) {
//            holder = new HeaderViewHolder();
//            convertView = mInflater.inflate(R.layout.item_header_email, parent, false);
//            holder.text = (TextView) convertView.findViewById(R.id.tvEmail);
//            convertView.setTag(holder);
//        } else {
//            holder = (HeaderViewHolder) convertView.getTag();
//        }
//        try {
//            String time = FlobillerConstaints.TIME_VISIT_FORMAT_DISPLAY.format(FlobillerConstaints.TIME_VISIT_FORMAT.parse(payBillHistoryModelsFilter.get(position).getTimeHistory()));
//            //set header text as first char in name
//            holder.text.setText(time);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return convertView;
//    }
//
//    @Override
//    public long getHeaderId(int position) {
//        //return the first character of the country as ID because this is what headers are based upon
//        try {
//            String time = FlobillerConstaints.TIME_VISIT_FORMAT_DISPLAY.format(FlobillerConstaints.TIME_VISIT_FORMAT.parse(payBillHistoryModelsFilter.get(position).getTimeHistory()));
//            return time.subSequence(0, 1).charAt(0);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }
//
//    public Filter getFilter() {
//        return mFilter;
//    }
//
//    private class ItemFilter extends Filter {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//
//            String filterString = constraint.toString().toLowerCase();
//
//            FilterResults results = new FilterResults();
//
//            final List<PayBillHistoryModel> list = payBillHistoryModels;
//
//            int count = list.size();
//            final ArrayList<String> nList = new ArrayList<>(count);
//
//            String filterableString ;
//
//            for (int i = 0; i < count; i++) {
//                try {
//                    filterableString = FlobillerConstaints.TIME_VISIT_FORMAT_DISPLAY.format(FlobillerConstaints.TIME_VISIT_FORMAT.parse(payBillHistoryModelsFilter.get(i).getTimeHistory()));
//                    if (filterableString.toLowerCase().contains(filterString)) {
//                        nList.add(filterableString);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            results.values = nList;
//            results.count = nList.size();
//
//            return results;
//        }
//
//        @SuppressWarnings("unchecked")
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            payBillHistoryModelsFilter = (ArrayList<PayBillHistoryModel>) results.values;
//            notifyDataSetChanged();
//        }
//
//    }
//
//    class HeaderViewHolder {
//        TextView text;
//    }
//
//    static class ViewHoler{
//        TextView tvBillerName;
//        TextView tvTime;
//    }

//}

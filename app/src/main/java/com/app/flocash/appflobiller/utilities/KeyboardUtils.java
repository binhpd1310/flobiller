package com.app.flocash.appflobiller.utilities;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.ToggleButton;

/**
 * All utils for keyboard.
 */
public class KeyboardUtils {
    /**
     * Hide keyboard.
     *
     * @param activity activity
     */
    private static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null
                && activity.getCurrentFocus().getWindowToken() != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            activity.getCurrentFocus().clearFocus();
        }
    }

    /**
     * Hide keyboard when touch outside view.
     *
     * @param activity activity
     * @param view     view root
     */
    public static void hideKeyboard(final Activity activity, View view) {
        if (activity == null) {
            return;
        }
        // Set up touch listener for non-text box views to hide keyboard.
//        if (!(view instanceof EditText) && !(view instanceof Button)) {
        if (!(view instanceof EditText) && !(view instanceof ScrollView) && !(view instanceof ToggleButton)) {
            view.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity, v);
                    return false;
                }
            });
        }

        // If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            int size = ((ViewGroup) view).getChildCount();
            for (int i = 0; i < size; i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboard(activity, innerView);
            }
        }
    }

//    /**
//     * Hide keyboard when touch outside view.
//     *
//     * @param activity activity
//     * @param view     view root
//     */
//    public static void hideKeyboard(final Activity activity, View view, final SearchWidget lvSearchWidget, final SearchWidget gvSearchWidget) {
//        if (activity == null) {
//            return;
//        }
//        // Set up touch listener for non-text box views to hide keyboard.
////        if (!(view instanceof EditText) && !(view instanceof Button)) {
//        if (!(view instanceof EditText) && !(view instanceof ScrollView) && !(view instanceof ToggleButton)) {
//            view.setOnTouchListener(new OnTouchListener() {
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    hideSoftKeyboard(activity, v);
//                    lvSearchWidget.cancelPauseSearch();
//                    gvSearchWidget.cancelPauseSearch();
//                    return false;
//                }
//            });
//        }
//
//        // If a layout container, iterate over children and seed recursion.
//        if (view instanceof ViewGroup) {
//            int size = ((ViewGroup) view).getChildCount();
//            for (int i = 0; i < size; i++) {
//                View innerView = ((ViewGroup) view).getChildAt(i);
//                hideKeyboard(activity, innerView);
//            }
//            lvSearchWidget.cancelPauseSearch();
//            gvSearchWidget.cancelPauseSearch();
//        }
//    }

    /**
     * Hide keyboard when touch outside view.
     *
     * @param activity activity
     * @param view     view root
     */
//    public static void hideKeyboard(final Activity activity, View view, final SearchWidget lvSearchWidget) {
//        if (activity == null) {
//            return;
//        }
//        // Set up touch listener for non-text box views to hide keyboard.
////        if (!(view instanceof EditText) && !(view instanceof Button)) {
//        if (!(view instanceof EditText) && !(view instanceof ScrollView) && !(view instanceof ToggleButton)) {
//            view.setOnTouchListener(new OnTouchListener() {
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    hideSoftKeyboard(activity, v);
//                    lvSearchWidget.cancelPauseSearch();
//                    return false;
//                }
//            });
//        }
//
//        // If a layout container, iterate over children and seed recursion.
//        if (view instanceof ViewGroup) {
//            int size = ((ViewGroup) view).getChildCount();
//            for (int i = 0; i < size; i++) {
//                View innerView = ((ViewGroup) view).getChildAt(i);
//                hideKeyboard(activity, innerView);
//            }
//            lvSearchWidget.cancelPauseSearch();
//        }
//    }

    /**
     * Show keyboard
     *
     * @param view view
     */
    public static void showKeyboard(View view) {
        if (view.isFocused()) {
            view.clearFocus();
        }
        view.requestFocus();
        InputMethodManager keyboard = (InputMethodManager) view.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    public static void hideKeyboardOnLostFocus(final Activity activity, View v) {
        v.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideSoftKeyboard(activity, v);
                }
            }
        });
    }

    /**
     * Show keyboard after a delay
     *
     * @param view      view
     * @param timeDelay time
     * @return {@link Handler}. Should call
     * {@link Handler#removeCallbacksAndMessages(Object)} with
     * parameter= null when view is destroyed to avoid memory leak.
     */
    public static Handler showDelayKeyboard(final View view, long timeDelay) {
        Handler handler = new Handler();
        if (view == null || view.getContext() == null) {
            return handler;
        }

        if (timeDelay < 0) {
            timeDelay = 0;
        }
        view.requestFocus();
        Runnable delayRunnable = new Runnable() {

            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) view
                        .getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(view, InputMethodManager.SHOW_FORCED);
            }
        };
        handler.postDelayed(delayRunnable, timeDelay);
        return handler;
    }

    public static void hideKeyboardOnCustomView(Activity mActivity, View v) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void hideKeyboard(Activity mActivity) {
        try {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                final View focusView = mActivity.getCurrentFocus();
                if (focusView != null) {
                    imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}

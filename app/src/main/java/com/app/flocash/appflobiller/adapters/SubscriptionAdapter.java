package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.models.SubscriptionItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by binh.pd on 12/10/2015.
 */
public class SubscriptionAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SubscriptionItem> subscriptionItems = new ArrayList<>();
    private LayoutInflater inflater;
    private ClickItemButtonListener obClickItemButtonListener;

    public SubscriptionAdapter(Context context, ArrayList<SubscriptionItem> subscriptionItems, ClickItemButtonListener clickItemButtonListener) {
        this.context = context;
        this.subscriptionItems = subscriptionItems;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.obClickItemButtonListener = clickItemButtonListener;
    }

    public void setListSubscriptions(ArrayList<SubscriptionItem> subscriptionItems) {
        this.subscriptionItems = subscriptionItems;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return subscriptionItems.size();
    }

    @Override
    public SubscriptionItem getItem(int position) {
        return subscriptionItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            row = inflater.inflate(R.layout.item_thumbnail_subcriptions, parent, false);
            holder = new ViewHolder();
            holder.rlFrameContent = (RelativeLayout)row.findViewById(R.id.rlFrameContent);
            holder.ivLogo = (ImageView) row.findViewById(R.id.ivLogo);
            holder.tvName = (TextView) row.findViewById(R.id.tvBillerName);
            holder.tvviewInvoice = (TextView) row.findViewById(R.id.tvSubscribe);
            holder.tvEditSubscription = (TextView) row.findViewById(R.id.tvEditSubscribe);
            holder.tvRemoveSubscription = (TextView) row.findViewById(R.id.tvRemoveSubscription);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        holder.tvviewInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obClickItemButtonListener.onClickItem(position, ClickItemButtonListener.VIEW_INVOICE);
            }
        });
        holder.tvEditSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obClickItemButtonListener.onClickItem(position, ClickItemButtonListener.EDIT_SUBSCRIPTION);
            }
        });
        holder.tvRemoveSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obClickItemButtonListener.onClickItem(position, ClickItemButtonListener.REMOVE_SUBSCRIPTION);
            }
        });
        SubscriptionItem subscriptions = subscriptionItems.get(position);
        if ((position % 2 == 0)) {
            holder.rlFrameContent.setBackgroundResource(R.drawable.bg_thumb_round_red);
            holder.tvName.setBackgroundResource(R.drawable.bg_round_title_thumb_red);
            holder.tvRemoveSubscription.setTextColor(context.getResources().getColor(R.color.color_title_thumb_red));
        } else {
            holder.rlFrameContent.setBackgroundResource(R.drawable.bg_thumb_round_blue);
            holder.tvName.setBackgroundResource(R.drawable.bg_round_title_thumb_blue);
            holder.tvRemoveSubscription.setTextColor(context.getResources().getColor(R.color.color_title_thumb_blue));
        }
        holder.tvName.setText(subscriptions.getName());
        holder.tvviewInvoice.setText(subscriptions.getInvoices());

        Picasso.with(context).load(subscriptions.getLogo()).into(holder.ivLogo);
        return row;
    }

    static class ViewHolder {
        RelativeLayout rlFrameContent;
        ImageView ivLogo;
        TextView tvName, tvviewInvoice;
        TextView tvEditSubscription;
        TextView tvRemoveSubscription;
    }

    public interface ClickItemButtonListener {
        int VIEW_INVOICE = 0;
        int EDIT_SUBSCRIPTION = 1;
        int REMOVE_SUBSCRIPTION = 2;
        void onClickItem(int index, int buttonId);
    }
}

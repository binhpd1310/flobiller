package com.app.flocash.appflobiller.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by ${binhpd} on 5/17/2017.
 */

public class WelcomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private int[] mResources = {R.drawable.logo_feature_1, R.drawable.logo_feature_2, R.drawable.logo_feature_3};
    private int[] mColorResource = {R.color.color_feature_1, R.color.color_feature_2, R.color.color_feature_3};

    private ViewPagerAdapter mViewPagerAdapter;

    @Bind(R.id.vpWelcome)
    ViewPager mVpWelcome;

    @Bind(R.id.indicator)
    CircleIndicator mCircleIndicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        mViewPagerAdapter = new ViewPagerAdapter(this, mResources, mColorResource);
        mVpWelcome.setAdapter(mViewPagerAdapter);
        mVpWelcome.setCurrentItem(0);

        mCircleIndicator.setViewPager(mVpWelcome);
    }

    @OnClick({R.id.tvSkip})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSkip:
                SavedSharedPreference savedSharedPreference = new SavedSharedPreference(WelcomeActivity.this);
                savedSharedPreference.setSeeIntroduction(true);
                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public  class ViewPagerAdapter extends PagerAdapter {
        private Context mContext;
        private int[] mArrLogoRes;
        private int[] mArrColorRes;

        public ViewPagerAdapter(Context context, int[] arrLogoRes, int[] arrColorRes) {
            this.mContext = context;
            this.mArrLogoRes = arrLogoRes;
            this.mArrColorRes = arrColorRes;
        }

        @Override
        public int getCount() {
            return mArrLogoRes.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.welcome_pager_item, container, false);
            container.addView(itemView);
            ImageView ivFeature = (ImageView) itemView.findViewById(R.id.ivFeature);
            LinearLayout llBackground = (LinearLayout) itemView.findViewById(R.id.llBackground);

            ivFeature.setImageResource(mArrLogoRes[position]);
            llBackground.setBackgroundColor(ContextCompat.getColor(mContext, mArrColorRes[position]));
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}

package com.app.flocash.appflobiller.fragments;

import android.view.View;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.views.ExpandedListView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 5/24/2017.
 */

public class ManagerPaymentCardFragment extends BaseFragment {

    @Bind(R.id.lvRegisteredCard)
    ExpandedListView mLvRegisteredCard;

    @Bind(R.id.llPaymentCardList)
    View mLlPaymentCardList;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_manager_payment_card;
    }

    @Override
    public void onCreateViewFragment(View view) {

    }

    @OnClick({R.id.tvAddNewCard})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvAddNewCard:
                addFragment(R.id.content, new EnterCardDetailFragment());
                break;

        }
    }
}

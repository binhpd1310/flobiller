package com.app.flocash.appflobiller.models;

/**
 * Created by binh.pd on 12/10/2015.
 */
public class SubscriptionItem extends ItemListing {
    private String invoices;

    public String getInvoices() {
        return invoices;
    }

    public void setInvoices(String invoices) {
        this.invoices = invoices;
    }
}

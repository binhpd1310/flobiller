package com.app.flocash.appflobiller.activities;

import android.os.Bundle;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.fragments.ManagerPaymentCardFragment;

import butterknife.ButterKnife;

/**
 * Created by ${binhpd} on 5/24/2017.
 */

public class ManagePaymentCardActivity extends SubScreenActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_base);
        ButterKnife.bind(this);
        addFragment(R.id.content, new ManagerPaymentCardFragment());
        mTvTitle.setText(getString(R.string.manage_payments));
    }

}

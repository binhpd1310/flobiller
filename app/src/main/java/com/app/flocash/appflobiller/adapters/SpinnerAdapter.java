package com.app.flocash.appflobiller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;

import java.util.List;

/**
 * Created by ${binhpd} on 1/12/2016.
 */
// Creating an Adapter Class
public class SpinnerAdapter extends ArrayAdapter {
    private List<String> fieldsName;
    private LayoutInflater inflater;

    public SpinnerAdapter(Context context, int textViewResourceId, List objects) {
        super(context, textViewResourceId, objects);
        fieldsName = objects;
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.layout_custom_spinner, parent, false);
            holder.tvField = (TextView) convertView.findViewById(R.id.tvNameSpinner);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.tvField.setText(fieldsName.get(position));
        return convertView;
    }

    // It gets a View that displays in the drop down popup the data at the specified position
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // It gets a View that displays the data at the specified position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public class Holder {
        private TextView tvField;
    }
}
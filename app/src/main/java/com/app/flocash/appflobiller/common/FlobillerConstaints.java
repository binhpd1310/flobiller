package com.app.flocash.appflobiller.common;

import java.text.SimpleDateFormat;

/**
 * Created by ${binhpd} on 12/5/2015.
 */
public interface FlobillerConstaints {
    String PROJECT_NUMBER = "249404455925";

    String KEY_EXTRA_SCREEN_ID = "KEY_EXTRA_SCREEN_ID";
    String KEY_EXTRA_ORDER_ID = "KEY_EXTRA_ORDER_ID";
    String KEY_EXTRA_INVOICE_ITEM = "KEY_EXTRA_INVOICE_ITEM";
    String KEY_EXTRA_IS_PAID = "KEY_EXTRA_IS_PAID";
    String KEY_EXTRA_URL = "KEY_EXTRA_URL";
    String KEY_EXTRA_NANE = "KEY_EXTRA_NANE";
    String KEY_EXTRA_CATEGORY_TYPE = "KEY_EXTRA_CATEGORY_TYPE";
    String KEY_EXTRA_CATEGORY_NAME = "KEY_EXTRA_CATEGORY_NAME";
    String KEY_EXTRA_EVENT_ID = "KEY_EXTRA_EVENT_ID";
    String KEY_EXTRA_EVENT_NAME = "KEY_EXTRA_EVENT_NAME";

    int SCREEN_PAY_BILL = 0;
    int SCREEN_SUBSCRIPTION = 1;
    int SCREEN_INVOICES = 2;
    int SCREEN_EDIT = 3;
    int SCREEN_PROFILE = 4;
    int SCREEN_BILLER = 5;
    int SCREEN_FORGOT_PASSWORD = 6;
    int SCREEN_CREATE_SUBCRIBLE = 7;
    int SCREEN_UPDATE_SUBCRIBLE = 8;
    int SCREEN_PAYMENT = 9;
    int SCREEN_VIRTUAL_TERMINAL = 10;
    int SCREEN_PAYMENT_RESULT_FRAGMENT = 11;
    int SCREEN_ABOUT_APP = 12;
    int SCREEN_AIRTIME = 13;
    int SCREEN_PRIVACY = 14;
    int SCREEN_GIFT_CARD = 15;
    int SCREEN_CALLED_TICKETS = 16;
    int SCREEN_ADD_CARD = 17;

    String STATUS_PAID = "B";
    long COUNT_DOWN_TIME = 5000;
    String CACHE_DIR = "smoothie";
    String JPG = ".jpg";
    String DOT = ".";
    SimpleDateFormat TIME_VISIT_FORMAT = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat TIME_PAID = new SimpleDateFormat("MM/dd/yy");

    SimpleDateFormat TIME_VISIT_FORMAT_DISPLAY = new SimpleDateFormat("MMM dd");
    SimpleDateFormat TIME_VISIT_FORMAT_HEADER= new SimpleDateFormat("MMMM dd");
    String NIGERIA_CODE = "NG";
    String PARA_KEY = "key";
    String FOLDER_CACHE = "cache";
    String APPEND_PATH_COMPONENT = "/";
    String BILLER_CACHE_FILE_NAME = "biller.json";

    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm aaa");
    SimpleDateFormat ticketDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
    SimpleDateFormat dayOfWeekFormat = new SimpleDateFormat("EEE");
    String ENGLISH_LANGUAGE = "en";
    String FRENCH_LANGUAGE = "fr";
}

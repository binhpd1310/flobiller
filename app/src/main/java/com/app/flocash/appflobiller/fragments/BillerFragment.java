package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.adapters.BillerAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.models.BillerItem;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by binh.pd on 12/10/2015.
 */
public class BillerFragment extends FloBillerBaseFragment {
    private static final String TAG = BillerFragment.class.getSimpleName();
    @Bind(R.id.gvBillers)
    GridView mGridView;
    private BillerAdapter mGvBillerAdapter;
    private ArrayList<BillerItem> gridBillers;
    ArrayList<String> billerIds = new ArrayList<>();
    ArrayList<String> accNumbers = new ArrayList<>();

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_billers;
    }

    @Override
    public void onCreateViewFragment(View view) {
        gridBillers = new ArrayList<>();
        File cacheFile = Utils.getFileFromCache(mActivity, FlobillerConstaints.BILLER_CACHE_FILE_NAME);
        if (cacheFile == null) {
            retrieveBillers();
        } else {
            try {
                String jsonContent = Utils.getStringFromFile(cacheFile.getPath());
                bindBillerList(jsonContent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void retrieveBillers() {
        String token = values.getAuthToken();
        Log.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(String.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_BILLER, client, this);
        js.execute(new String[]{ConfigUrlsAPI.billers_url, JsonParse.REQUEST_TYPE_GET });
    }

    @Override
    public <T> void onSuccess(T data, final String method) {
        String result = (String) data;
        result = result.replaceAll("^\"|\"$", "");
        result = result.replaceAll("\\\\", "");
        bindBillerList(result);
    }

    private void bindBillerList(String result) {
        if ((result != null) && (result.length() > 0)) {
            try {
                //		Log.e("result",result);
                JSONObject jsonResponse = new JSONObject(result);
                JSONArray jsonMainNode = jsonResponse.getJSONArray("billers");
                BillerItem item;
                for(int i = 0; i < jsonMainNode.length(); i++) {
                    JSONObject jsonChildNode = jsonMainNode.optJSONObject(i);
                    String name = jsonChildNode.getString("name");
                    String country = jsonChildNode.getString("country");
                    String logo = jsonChildNode.getString("logo");
                    String billerId = jsonChildNode.getString("biller_id");
                    String accNumber = jsonChildNode.getString("account_number");
                    billerIds.add(billerId);
                    accNumbers.add(accNumber);
                    //	Log.e("billllerIDDDDS", ""+billerIds);
                    String subscribe;
                    if("null".equals(accNumber)) {
                        subscribe = getString(R.string.subcrible);
                    } else {
                        subscribe = getString(R.string.edit);
                    }
                    item = new BillerItem();
                    item.setName(name);
                    item.setCountry(country);
                    item.setImage(ConfigUrlsAPI.LOGO_URL_BASE + logo);
                    item.setSubscribe(subscribe);

                    gridBillers.add(item);
                }

                mGvBillerAdapter = new BillerAdapter(mActivity, gridBillers);
                mGridView.setAdapter(mGvBillerAdapter);

            } catch (JSONException e) {
                Log.e("Exception is", e.toString());
            }
        }

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                String biller_id = billerIds.get(position);
                String acc_number = accNumbers.get(position);
                values.setBillerId(biller_id);
                values.setAccNo(acc_number);
                Log.e("billerID", values.getBillerId());
                Log.e("accNumber", values.getAccNo());
                BillerItem billerItem = gridBillers.get(position);

                if (acc_number.equals("null")) {
                    Intent i = new Intent(mActivity, SubScreenActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_CREATE_SUBCRIBLE);
                    bundle.putString(FlobillerConstaints.KEY_EXTRA_URL, billerItem.getImage());
                    bundle.putString(FlobillerConstaints.KEY_EXTRA_NANE, billerItem.getName());
                    i.putExtras(bundle);
                    startActivity(i);
                } else {
                    Intent i = new Intent(mActivity, SubScreenActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_UPDATE_SUBCRIBLE);
                    bundle.putString(FlobillerConstaints.KEY_EXTRA_URL, billerItem.getImage());
                    bundle.putString(FlobillerConstaints.KEY_EXTRA_NANE, billerItem.getName());
                    i.putExtras(bundle);
                    startActivity(i);
                }

            }
        });
    }

    @Override
    public void onFail(String method) {
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }
}

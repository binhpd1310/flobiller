package com.app.flocash.appflobiller.service;

/**
 * Created by lion on 4/7/16.
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}

package com.app.flocash.appflobiller.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by binh.pd on 12/11/2015.
 */
public class ForgotPasswordActivity extends Activity implements ApiConnectTask.FloBillerRequestListener {
    @Bind(R.id.edtEmail)
    EditText mEdtEmail;
    @Bind(R.id.btnSubmit)
    Button mBtnSubmit;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnSubmit, R.id.ivBack, R.id.tvBackToLogin, R.id.btnRegister})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                if (TextUtils.isEmpty(mEdtEmail.getText())) {
                    DialogUtilities.getOkAlertDialog(this, "Enter your email id");
                } else {
                    send_mail();
                }
                break;
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvBackToLogin:
                finish();
                break;
            case R.id.btnRegister:
                Intent intent = new Intent(ForgotPasswordActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void send_mail() {
        try {
//            mProgressDialog.show();
            JsonParse client = new JsonParse(JSONObject.class);
            client.AddParam(ConfigUrlsAPI.forget_pass_email, mEdtEmail.getText().toString());

            ApiConnectTask js = new ApiConnectTask(this, "save", client, this);
            js.execute(new String[] { ConfigUrlsAPI.forgot_pass_url, "0" });
        } catch (Exception ex) {
            LogUtils.e("Exception ", ex.toString());
        }
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        JSONObject result = (JSONObject) data;
//        mProgressDialog.dismiss();
        if ((result != null) && (result.length() > 0)) {
            try {
                if (result.has(ConfigUrlsAPI.PARA_STATUS)) {
                    String status = result.getString(ConfigUrlsAPI.PARA_STATUS);
                    if (ConfigUrlsAPI.result_error.equalsIgnoreCase(status)) {
                        DialogUtilities.getOkAlertDialog(ForgotPasswordActivity.this, result.getString(ConfigUrlsAPI.PARA_MESSAGE)).show();
                    } else {
                        DialogUtilities.getOkAlertDialog(ForgotPasswordActivity.this, R.string.forgot_result_fail).show();
                    }
                } else {
                    LogUtils.d("result", "" + result);
                    Toast.makeText(this, R.string.forgot_result_success, Toast.LENGTH_LONG).show();
                    finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            DialogUtilities.getOkAlertDialog(ForgotPasswordActivity.this, R.string.forgot_result_fail).show();
        }
    }

    @Override
    public void onFail(String method) {
    }
}

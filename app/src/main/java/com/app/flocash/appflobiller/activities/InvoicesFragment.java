package com.app.flocash.appflobiller.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.InvoiceAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.fragments.FloBillerBaseFragment;
import com.app.flocash.appflobiller.models.InvoiceItem;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 12/9/2015.
 */
public class InvoicesFragment extends FloBillerBaseFragment implements ApiConnectTask.FloBillerRequestListener,
        InvoiceAdapter.OnSelectButtonInvoice {
    private static final String TAG = InvoicesFragment.class.getSimpleName();
    private static final int TAB_ALL = 0;
    private static final int TAB_PAID = 1;
    private static final int TAB_UNPAID = 2;

    private int mCurrentTab = TAB_ALL;
    private InvoiceAdapter mInvoiceAdapter;
    private ArrayList<InvoiceItem> mListInvoices;
    private ArrayList<InvoiceItem> mListInvoicesPaid;
    private ArrayList<InvoiceItem> mListInvoicesUnpaid;

    @Bind(R.id.lvInvoices)
    ListView mLvInvoices;
    @Bind(R.id.btnSearch)
    Button btnSearch;
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.tabAll)
    TextView mTabAll;
    @Bind(R.id.tabPaid)
    TextView mTabPaid;
    @Bind(R.id.tabUnpaid)
    TextView mTabUnpaid;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_invoices;
    }

    @Override
    public void onCreateViewFragment(View view) {
        values = new SavedSharedPreference(mActivity);
        mListInvoices = new ArrayList<>();
//        initView();
        final Bundle bundle  = getArguments();
        if (TextUtils.isEmpty(bundle.getString(FlobillerConstaints.KEY_EXTRA_INVOICE_ITEM))) {
            retrieve_invoices();
        } else {
            retrieve_invoice(bundle.getString(FlobillerConstaints.KEY_EXTRA_INVOICE_ITEM));
        }
        mInvoiceAdapter = new InvoiceAdapter(mActivity, mListInvoices);
        mInvoiceAdapter.setOnSelectButtonInvoice(this);
        mLvInvoices.setAdapter(mInvoiceAdapter);
        btnSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int textlength = etSearch.getText().length();
                ArrayList<InvoiceItem> arrayListinvoices = new ArrayList<>();
                for (InvoiceItem items : mListInvoices) {
                    if (textlength <= items.getCustAccountName().length()) {
                        if (items
                                .getCustAccountName()
                                .toLowerCase()
                                .contains(
                                        etSearch.getText().toString()
                                                .toLowerCase())) {
                            arrayListinvoices.add(items);
                        }

                    }
                }
                mInvoiceAdapter = new InvoiceAdapter(mActivity, arrayListinvoices);
                mLvInvoices.setAdapter(mInvoiceAdapter);

            }
        });
    }

    @OnClick({R.id.tabAll, R.id.tabPaid, R.id.tabUnpaid})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tabAll:
                mTabAll.setBackgroundResource(R.drawable.tab_invoice_select);
                mTabPaid.setBackgroundResource(R.drawable.tab_invoice_unselect);
                mTabUnpaid.setBackgroundResource(R.drawable.tab_invoice_unselect);
                mInvoiceAdapter = new InvoiceAdapter(mActivity, mListInvoices);
                mInvoiceAdapter.setOnSelectButtonInvoice(this);
                mLvInvoices.setAdapter(mInvoiceAdapter);
                mCurrentTab = TAB_ALL;
                break;
            case R.id.tabPaid:
                mCurrentTab = TAB_PAID;
                mTabAll.setBackgroundResource(R.drawable.tab_invoice_unselect);
                mTabPaid.setBackgroundResource(R.drawable.tab_invoice_select);
                mTabUnpaid.setBackgroundResource(R.drawable.tab_invoice_unselect);
                mListInvoicesPaid = new ArrayList<>();
                for (InvoiceItem invoiceItem : mListInvoices) {
                    if (invoiceItem.getFlocashTransId().toString().equals(ConfigUrlsAPI.PAID)) {
                        mListInvoicesPaid.add(invoiceItem);
                    }
                }
                mInvoiceAdapter = new InvoiceAdapter(mActivity, mListInvoicesPaid);
                mInvoiceAdapter.setOnSelectButtonInvoice(this);
                mLvInvoices.setAdapter(mInvoiceAdapter);
                break;
            case R.id.tabUnpaid:
                mCurrentTab = TAB_UNPAID;
                mTabAll.setBackgroundResource(R.drawable.tab_invoice_unselect);
                mTabPaid.setBackgroundResource(R.drawable.tab_invoice_unselect);
                mTabUnpaid.setBackgroundResource(R.drawable.tab_invoice_select);
                mListInvoicesUnpaid = new ArrayList<>();
                for (InvoiceItem invoiceItem : mListInvoices) {
                    if (!invoiceItem.getFlocashTransId().toString().equals(ConfigUrlsAPI.PAID)) {
                        mListInvoicesUnpaid.add(invoiceItem);
                    }
                }
                mInvoiceAdapter = new InvoiceAdapter(mActivity, mListInvoicesUnpaid);
                mInvoiceAdapter.setOnSelectButtonInvoice(this);
                mLvInvoices.setAdapter(mInvoiceAdapter);
                break;
        }
    }
    public void retrieve_invoices() {
        String token = values.getAuthToken();
        // String subId = values.getSubId();
        LogUtils.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        client.AddParam(ConfigUrlsAPI.INVOICE_SERVICE, "0");
        // LogUtils.e("subscription ID", subId);
        client.AddParam(ConfigUrlsAPI.INVOICE_START, "0");
        ApiConnectTask js = new ApiConnectTask(mActivity, "save", client, this);
        js.execute(new String[]{ConfigUrlsAPI.API_GET_INVOICES, "0"});
    }

    public void retrieve_invoice(String orderId) {
        String token = values.getAuthToken();
        // String subId = values.getSubId();
        LogUtils.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        client.AddParam(ConfigUrlsAPI.INVOICE_SERVICE, orderId);
        client.AddParam(ConfigUrlsAPI.INVOICE_START, "0");
        ApiConnectTask js = new ApiConnectTask(mActivity, "save", client, this);
        js.setShowDialog(false);
        js.execute(new String[] { ConfigUrlsAPI.API_GET_INVOICES, "0" });
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        final JSONObject result = (JSONObject) data;
        if ((result != null) && (result.length() > 0)) {
            switch (method) {
                case "save":
                    try {
                        LogUtils.e("result", "" + result);
                        JSONArray jsonMainNode = result.getJSONArray("orders");
                        InvoiceItem item;
                        mListInvoices = new ArrayList<>();
                        for (int i = 0; i < jsonMainNode.length(); i++) {
                            JSONObject jsonChildNode = jsonMainNode.optJSONObject(i);

                            String invoiceId = jsonChildNode.getString("invoice_id");
                            String biller_invoiceid = jsonChildNode.getString("biller_invoiceid");
                            String name = jsonChildNode.getString("name");
                            String currency = jsonChildNode.getString("currency");
                            String amount = jsonChildNode.getString("amount");
                            String service_id = jsonChildNode.getString("service_id");
                            String createdDate = jsonChildNode.getString("generate_date");
                            String last_pay_date = jsonChildNode.getString("last_pay_date");
                            String cust_email = jsonChildNode.getString("cust_email");
                            String cust_mobile = jsonChildNode.getString("cust_mobile");
                            String customerName = jsonChildNode.getString("cust_account_name");
                            String cust_account_number = jsonChildNode.getString("cust_account_number");
                            String cust_topup = jsonChildNode.getString("cust_topup");
                            String flocash_email = jsonChildNode.getString("flocash_email");
                            String flocash_status = jsonChildNode.getString("flocash_status");
                            String flocash_trans_id = jsonChildNode.getString("flocash_trans_id");
                            String flocash_paid_date = jsonChildNode.getString("flocash_paid_date");
                            String api = jsonChildNode.getString("api");
                            String api_status = jsonChildNode.getString("api_status");
                            String logo = jsonChildNode.getString("logo");
                            item = new InvoiceItem();
                            item.setInvoiceId(invoiceId);
                            item.setBillerInvoiceid(biller_invoiceid);
                            item.setName(name);
                            item.setCurrency(currency);
                            item.setAmount(amount);
                            item.setServiceId(service_id);
                            item.setGenerateDate(createdDate);
                            item.setLastPayDate(last_pay_date);
                            item.setCustEmail(cust_email);
                            item.setCustMobile(cust_mobile);
                            item.setCustAccountName(customerName);
                            item.setCustAccountNumber(cust_account_number);
                            item.setCustTopup(cust_topup);
                            item.setFlocashEmail(flocash_email);
                            item.setFlocashStatus(flocash_status);
                            item.setFlocashTransId(flocash_trans_id);
                            item.setFlocashPaidDate(flocash_paid_date);
                            item.setApi(api);
                            item.setApi_status(api_status);
                            item.setUrl(logo);
                            mListInvoices.add(item);
                        }
                        mInvoiceAdapter = new InvoiceAdapter(mActivity, mListInvoices);
                        mInvoiceAdapter.setOnSelectButtonInvoice(InvoicesFragment.this);
                        mLvInvoices.setAdapter(mInvoiceAdapter);
                        etSearch.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence cs, int start,
                                                      int before, int count) {
                                int textlength = cs.length();
                                if (textlength == 0) {
                                    mInvoiceAdapter = new InvoiceAdapter(mActivity, mListInvoices);
                                    mInvoiceAdapter.setOnSelectButtonInvoice(InvoicesFragment.this);
                                    mLvInvoices.setAdapter(mInvoiceAdapter);
                                }
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                            }
                        });
                    } catch (JSONException e) {
                        LogUtils.e("Exception is", e.toString());
                    }
                    break;
                case ConfigUrlsAPI.REQUEST_MARK_AS_PAID:
                    try {
                        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.SUCCESS, result.getString(ConfigUrlsAPI.PARA_MESSAGE), new DialogUtilities.DialogCallBack() {
                            @Override
                            public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                                retrieve_invoices();
                            }
                        }).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }

    @Override
    public void onFail(String method) {
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }

    @Override
    public void onSelectButtonInvoice(int button, int position) {
        InvoiceItem invoiceItem;
        if (mCurrentTab == TAB_ALL) {
            invoiceItem = mListInvoices.get(position);
        } else if (mCurrentTab == TAB_PAID) {
            invoiceItem = mListInvoicesPaid.get(position);
        } else {
            invoiceItem = mListInvoicesUnpaid.get(position);
        }

        if (InvoiceAdapter.OnSelectButtonInvoice.MARK_AS_READ == button) {
            markAsRead(mListInvoices.get(position));
        } else {
            Intent intent = new Intent(mActivity, SubScreenActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT);
            bundle.putString(FlobillerConstaints.KEY_EXTRA_ORDER_ID, invoiceItem.getInvoiceId());
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    private void markAsRead(InvoiceItem invoiceItem) {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.add_mark_as_paid_order_id, invoiceItem.getInvoiceId());

        ApiConnectTask jsCountriesTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_MARK_AS_PAID, client, this);
        jsCountriesTask.execute(new String[]{ConfigUrlsAPI.API_MARK_AS_PAID, JsonParse.REQUEST_TYPE_POST});
    }
}

package com.app.flocash.appflobiller.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.app.flocash.appflobiller.utilities.FlobillerApplication;
import com.app.flocash.appflobiller.utilities.Utils;

import uk.co.senab.bitmapcache.BitmapLruCache;

/**
 * Created by ${binhpd} on 3/12/2016.
 */
public class LoadingImageFromCache extends AsyncTask<Object, Void, Bitmap> {
    private final ImageView imageView;
    private final String path;
    private final int resId;
    private final Context context;

    public LoadingImageFromCache(Context context, ImageView imageView, String path, int resId) {
        this.imageView = imageView;
        this.path = path;
        this.resId = resId;
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(Object... objects) {
        Bitmap imgBitmap = null;
        BitmapLruCache cache = FlobillerApplication.getInstance().getBitmapCache();
        if (cache.get(path) == null) {
            imgBitmap = Utils.getThumbnailBimapFromFilePath(path);
            if (imgBitmap != null) {
                cache.put(path, imgBitmap);
            }
        } else {
            if (!cache.get(path).getBitmap().sameAs(Utils.getThumbnailBimapFromFilePath(path))) {
                imgBitmap = Utils.getThumbnailBimapFromFilePath(path);
                if (imgBitmap != null) {
                    cache.put(path, imgBitmap);
                }
            } else {
                imgBitmap = cache.get(path).getBitmap();
            }
        }
        return imgBitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(resId);
        }
    }

}

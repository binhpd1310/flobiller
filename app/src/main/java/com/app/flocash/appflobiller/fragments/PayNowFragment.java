package com.app.flocash.appflobiller.fragments;

import android.app.ProgressDialog;
import android.view.View;

import com.app.flocash.appflobiller.dao.FloCashDAO;
import com.app.flocash.appflobiller.models.CountryModel;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;

import com.app.flocash.appflobiller.R;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 12/5/2015.
 */
public abstract class PayNowFragment extends FloBillerBaseFragment implements ApiConnectTask.FloBillerRequestListener {
    protected ProgressDialog mProgressDialog;
    private FloCashDAO mFloCashDAO;
    private ArrayList<CountryModel> mListCountry;

    @Override
    public void onCreateViewFragment(View view) {
        mProgressDialog = new ProgressDialog(mActivity);
        mProgressDialog.setMessage(getString(R.string.notify_please_wait));
        mProgressDialog.setCancelable(false);

        mFloCashDAO = new FloCashDAO(mActivity);
        mListCountry = mFloCashDAO.getAllCountry();
    }

    protected String getNameCountry(String countryKey) {
        for (CountryModel countryModel : mListCountry) {
            if (countryKey.equals(countryModel.getKey())) {
                return countryModel.getValue();
            }
        }
        return "";
    }
}

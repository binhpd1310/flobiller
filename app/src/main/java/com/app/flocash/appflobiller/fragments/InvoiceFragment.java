package com.app.flocash.appflobiller.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.flocash.core.service.entity.MerchantInfo;
import com.flocash.core.service.entity.OrderInfo;
import com.flocash.core.service.entity.PayerInfo;
import com.flocash.core.service.entity.Request;
import com.flocash.sdk.ui.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 12/13/2015.
 */
public class InvoiceFragment extends FloBillerBaseFragment {
    public static final String MERCHANT_ACCOUNT = "flobiller@flocash.com";
    public static final int PAY_INVOICE = 1010;

    public static final String STATUS_PAID = "S";
    public static final String STATUS_PENDING = "P";
    public static final String STATUS_NOT_PAID = "PN";

    @Bind(R.id.tvInvoiceStatus)
    TextView mTvInvoiceStatus;

    @Bind(R.id.tvType)
    TextView mTvType;

    @Bind(R.id.tvName)
    TextView mTvName;

    @Bind(R.id.tvAccountNo)
    TextView mTvAccountNo;

    @Bind(R.id.tvInvoiceNo)
    TextView mTvInvoiceNo;

    @Bind(R.id.tvPay)
    TextView mTvPay;

    @Bind(R.id.tvAmount)
    TextView mTvAmount;

    private String mFirstName;
    private String mLastName;
    private String mEmail;
    private String mMobileNumber;
    private String mCountry;
    private String mOrderId;

    private ProgressDialog mProgressDialog;

    boolean isOpen = false;
    private String mAmount;
    private String mName;
    private String mAccName;
    private String mAccountNo;
    private String mInvoiceNo;

    private Request request;
    private String mAmountNumber;
    private String mLogo;
    private String mStatus;

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_invoice;
    }

    @Override
    public void onCreateViewFragment(View view) {
        if (!isOpen) {
            mOrderId = getArguments().getString(FlobillerConstaints.KEY_EXTRA_ORDER_ID);
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage(getString(R.string.notify_please_wait));
            mProgressDialog.setCancelable(false);
            getUserInfo();
            isOpen = true;
        } else {
            updateUI();
        }
    }

    @OnClick({R.id.tvPay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvPay: {
                openPayGate();
            }
            break;
        }
    }

    public void retrieve_invoice(String orderId) {
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
        String token = values.getAuthToken();
        // String subId = values.getSubId();
        Log.e("tokennnnnnnnnnnnnn", token);
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        client.AddParam(ConfigUrlsAPI.INVOICE_ORDER_ID, orderId);

        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_SAVE, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.API_GET_INVOICE, JsonParse.REQUEST_TYPE_GET});
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAY_INVOICE && resultCode == PaymentActivity.RESULT_OK) {
            retrieve_invoice(mOrderId);
        }
    }

    public void getUserInfo() {
        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_USER_INFO, client, this);
        js.setShowDialog(false);
        mProgressDialog.show();
        js.execute(new String[]{ConfigUrlsAPI.user_retrieve_url, JsonParse.REQUEST_TYPE_GET});
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        JSONObject result = (JSONObject) data;
        if ((result != null) && (result.length() > 0)) {
            switch (method) {
                case ConfigUrlsAPI.REQUEST_SAVE:
                    // card payment
                    OrderInfo order = new OrderInfo();
                    PayerInfo payer = new PayerInfo();
                    try {
                        JSONObject orderInfo = result.getJSONObject("order").getJSONObject("invoice");
                        order.setAmount(new BigDecimal(orderInfo.getString(ConfigUrlsAPI.PARA_AMOUNT)));
                        order.setCurrency(orderInfo.getString(ConfigUrlsAPI.PARA_CURRENCY));
                        order.setItem_name(orderInfo.getString(ConfigUrlsAPI.PARA_NAME));
                        order.setItem_price(orderInfo.getString(ConfigUrlsAPI.PARA_AMOUNT));
                        order.setOrderId(result.getString(ConfigUrlsAPI.PARA_ORDER_ID));
                        order.setQuantity("1");
                        //TODO: binh test again
                        payer.setCountry(orderInfo.getString(ConfigUrlsAPI.PARA_COUNTRY));
                        payer.setFirstName(orderInfo.getString(ConfigUrlsAPI.PARA_FIRST_NAME));
                        payer.setLastName(orderInfo.getString(ConfigUrlsAPI.PARA_LAST_NAME));
                        payer.setEmail(orderInfo.getString(ConfigUrlsAPI.PARA_CUSTOMER_EMAIL));
                        payer.setMobile(orderInfo.getString(ConfigUrlsAPI.PARA_CUSTOMER_MOBILE));
                        MerchantInfo merchant1 = new MerchantInfo();
                        merchant1.setMerchantAccount(MERCHANT_ACCOUNT);
                        request = new Request();
                        request.setOrder(order);
                        request.setPayer(payer);
                        request.setMerchant(merchant1);

                        // fill information invoice
                        mAmountNumber = orderInfo.getString(ConfigUrlsAPI.PARA_AMOUNT);
                        mAmount = orderInfo.getString(ConfigUrlsAPI.PARA_CURRENCY) + " " + orderInfo.getString(ConfigUrlsAPI.PARA_AMOUNT);
                        mName = orderInfo.getString(ConfigUrlsAPI.PARA_NAME);
                        mAccName = orderInfo.getString(ConfigUrlsAPI.add_paybill_invoice_accName);
                        mAccountNo = orderInfo.getString(ConfigUrlsAPI.add_paybill_invoice_accNumber);
                        mInvoiceNo = orderInfo.getString(ConfigUrlsAPI.PARA_BILLER_INVOICEID);
                        mLogo = orderInfo.getString(ConfigUrlsAPI.PARA_LOGO);
                        mStatus = orderInfo.getString(ConfigUrlsAPI.PARA_FLOCASH_STATUS);

                        updateUI();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
                    }
                    break;
                case ConfigUrlsAPI.REQUEST_GET_USER_INFO:
                    try {
                        JSONObject obj = result.getJSONObject(ConfigUrlsAPI.PARA_USER);
                        mFirstName = obj.getString(ConfigUrlsAPI.PARA_FIRST_NAME);
                        mLastName = obj.getString(ConfigUrlsAPI.PARA_LAST_NAME);
                        mMobileNumber = obj.getString(ConfigUrlsAPI.PARA_COUNTRY_CODE) + obj.getString(ConfigUrlsAPI.PARA_MOBILE);
                        mCountry = obj.getString(ConfigUrlsAPI.PARA_COUNTRY);
                        mEmail = values.getUserEmail();
                        retrieve_invoice(mOrderId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
                    }
                    break;
            }
        }
    }

    private void updateUI() {
        mTvAmount.setText(mAmount);
        mTvType.setText(mName);
        mTvName.setText(mAccName);
        mTvAccountNo.setText(mAccountNo);
        mTvInvoiceNo.setText(mInvoiceNo);

        if (TextUtils.isEmpty(mStatus) && STATUS_PAID.equals(mStatus)) {
            mTvInvoiceStatus.setText(R.string.invoice_status_paid);
            mTvPay.setVisibility(View.GONE);
        } else if (STATUS_NOT_PAID.equals(mStatus) || TextUtils.isEmpty(mStatus)) {
            mTvInvoiceStatus.setText(R.string.invoice_status_not_paid);
            mTvPay.setVisibility(View.VISIBLE);
        } else if (STATUS_PENDING.equals(mStatus)) {
            mTvInvoiceStatus.setText(R.string.invoice_status_pending);
            mTvPay.setVisibility(View.GONE);
        }
        mProgressDialog.dismiss();
    }

    @Override
    public void onFail(String method) {
        mProgressDialog.dismiss();
        DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }

    private void openPayGate() {
        Intent intent = new Intent(mActivity, PaymentActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(PaymentActivity.EXTRA_REQUEST_OBJECT, request);
        bundle.putString(PaymentActivity.EXTRA_LOGO, mLogo);
        bundle.putString(PaymentActivity.EXTRA_AMOUNT, mAmountNumber);
        bundle.putString(PaymentActivity.EXTRA_PHONE_CODE, values.getCountryCode());
        bundle.putString(PaymentActivity.EXTRA_PHONE_NUMBER, values.getUserMobile());
        bundle.putString(PaymentActivity.EXTRA_CURRENCY, values.getCurrency());
        bundle.putString(PaymentActivity.EXTRA_COUNTRY, values.getUserCountry());
        bundle.putString(PaymentActivity.EXTRA_ACCOUNT_NUMBER, mAccountNo);
        intent.putExtras(bundle);
        startActivityForResult(intent, PAY_INVOICE);
    }
}

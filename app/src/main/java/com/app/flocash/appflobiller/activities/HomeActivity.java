package com.app.flocash.appflobiller.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.NavDrawerListAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.fragments.HomeFragment;
import com.app.flocash.appflobiller.models.NavDrawerItem;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;
import com.app.flocash.appflobiller.utilities.ServiceUtil;
import com.app.flocash.appflobiller.utilities.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.appinvite.AppInviteInvitation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomeActivity extends BaseActivity {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private static final int REQUEST_INVITE = 0;
    private static final int MY_PERMISSIONS_REQUEST_SMS_RECEIVER = 101;

    private SavedSharedPreference values;
    private ListView mDrawerList;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private DrawerLayout mDrawer;
    private String mLatestVersionApp;
    private AlertDialog mAlCheckAppVersion;
    private HomeFragment mHomeFragment;

    public CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCallbackManager = CallbackManager.Factory.create();
        initDrawer();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        checkSmsPermission();
        values = new SavedSharedPreference(this);
        if (!TextUtils.isEmpty(values.getAuthToken())) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mHomeFragment = new HomeFragment();
            String backstackStateName = HomeFragment.class.getName();
            fragmentTransaction.addToBackStack(backstackStateName);
            fragmentTransaction.replace(R.id.content, mHomeFragment, backstackStateName);
            fragmentTransaction.commit();
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void initDrawer() {
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav mDrawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        navDrawerItems = new ArrayList<>();

        // adding nav mDrawer items to array
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], R.drawable.ic_add_payment_card));
        // Home
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], R.drawable.ic_paybill));
        // Find People
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], R.drawable.ic_airtime));
        // Photos
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], R.drawable.ic_subcription));
        // Communities, Will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], R.drawable.ic_invoices));
        // Pages
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], R.drawable.ic_edit));
        // What's hot, We  will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], R.drawable.ic_profile));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], R.drawable.ic_invite_friend_drawer));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], R.drawable.ic_about_drawer));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], R.drawable.ic_about_drawer));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], R.drawable.ic_logout_drawer));


        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav mDrawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }


    private void onInviteClicked(View view) {

        //Get hold of a view to anchor the popup
        //instantiate a popup menu. var "this" stands for activity
        final PopupMenu popup = new PopupMenu(this, view);
        //the following code for 3.0 sdk
        //popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //Or in sdk 4.0 and above
        popup.inflate(R.menu.popup_menu);

        //Only use below to click callback
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                popup.dismiss();
                mDrawer.closeDrawers();
                if (item.getItemId() == R.id.action_google) {
                    inviteViaGoogle();
                } else {
                    inviteViaFacebook();
                }
                return true;
            }
        });
        popup.show();
    }

    private void inviteViaGoogle() {
        try {
            Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                    .setMessage(getString(R.string.invitation_message))
                    .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                    .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                    .setCallToActionText(getString(R.string.invitation_cta))
                    .build();
            startActivityForResult(intent, REQUEST_INVITE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void inviteViaFacebook() {
        String appLinkUrl, previewImageUrl;

        appLinkUrl = getString(R.string.facebook_url_app);
        previewImageUrl = getString(R.string.invitation_custom_image);

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();

            AppInviteDialog appInviteDialog = new AppInviteDialog(this);
            appInviteDialog.registerCallback(mCallbackManager, new FacebookCallback<AppInviteDialog.Result>() {
                @Override
                public void onSuccess(AppInviteDialog.Result result) {
                    Toast.makeText(HomeActivity.this, R.string.invite_success, Toast.LENGTH_LONG).show();
//                    DialogUtilities.getOkAlertDialog(MainActivity.this, R.string.send_invite_friend_success).show();
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException e) {
                }
            });


            appInviteDialog.show(this, content);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Check how many invitations were sent and log a message
                // The ids array contains the unique invitation ids for each invitation sent
                // (one for each contact select by the user). You can use these for analytics
                // as the ID will be consistent on the sending and receiving devices.
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
//                Log.d(TAG, getString(R.string.sent_invitations_fmt, ids.length));
            } else {
                // Sending failed or it was canceled, show failure message to the user
                showMessage(getString(R.string.send_failed));
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showMessage(String msg) {
        Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    private void executeLogout() {
        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(this, ConfigUrlsAPI.REQUEST_LOGOUT, client, this);
        js.execute(new String[]{ConfigUrlsAPI.app_logout_url, JsonParse.REQUEST_TYPE_GET});

        values.setAuthToken(null);
        values.setUserEmail(null);
        values.clear();
        Utils.clearCache(HomeActivity.this);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLatestAppVersion();
    }

    private void checkVersionApp() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            String currentVersionApp = "v" + version + "." + verCode;
            if (!currentVersionApp.equals(mLatestVersionApp)) {
                mAlCheckAppVersion = DialogUtilities.getOkAlertDialogColor(this, DialogUtilities.SUCCESS,
                        getString(R.string.msg_dowwload_lastest_version),
                        new DialogUtilities.DialogCallBack() {
                            @Override
                            public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
                                mAlCheckAppVersion.dismiss();
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        });
                mAlCheckAppVersion.show();
            } else {
                if (mAlCheckAppVersion != null && mAlCheckAppVersion.isShowing()) {
                    mAlCheckAppVersion.dismiss();
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getLatestAppVersion() {
        String token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, token);
        ApiConnectTask js = new ApiConnectTask(this, ConfigUrlsAPI.REQUEST_GET_APP_VERSION, client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.user_version_app, JsonParse.REQUEST_TYPE_GET});
    }


    @Override
    public <T> void onSuccess(T data, String method) {
        JSONObject result = (JSONObject) data;
        try {
            switch (method) {
                case ConfigUrlsAPI.REQUEST_GET_APP_VERSION:
                    mLatestVersionApp = result.getString("msg");
                    checkVersionApp();
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFail(String method) {
        if (ConfigUrlsAPI.REQUEST_LOGOUT.equals(method)) {
            DialogUtilities.getOkAlertDialogColor(this, DialogUtilities.FAIL, getString(R.string.error_fail)).show();
        }
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav mDrawer item
            displayView(view, position);
        }
    }

    /**
     * Diplaying fragment view for selected nav mDrawer list item
     */
    private void displayView(View view, int position) {
        Bundle bundle = new Bundle();
        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent(this, ManagePaymentCardActivity.class);
                startActivity(intent);
                break;
            case 1:
                mHomeFragment.selectFeature(ServiceUtil.PAYBILL);
                break;
            case 2:
                mHomeFragment.selectFeature(ServiceUtil.AIRTIME);
                break;
            case 3:
                intent = new Intent(this, SubScreenActivity.class);
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_SUBSCRIPTION);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, SubScreenActivity.class);
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_INVOICES);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, SubScreenActivity.class);
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_BILLER);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, SubScreenActivity.class);
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PROFILE);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 7:
                onInviteClicked(view);
                break;
            case 8:
                intent = new Intent(this, SubScreenActivity.class);
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_ABOUT_APP);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 9:
                intent = new Intent(this, SubScreenActivity.class);
                bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PRIVACY);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 10:
                executeLogout();
                break;
        }

        if (position != 6) {
            mDrawer.closeDrawers();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SMS_RECEIVER: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void checkSmsPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this,
                    Manifest.permission.RECEIVE_SMS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(HomeActivity.this,
                        new String[]{Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_SMS_RECEIVER);

                // MY_PERMISSIONS_REQUEST_SMS_RECEIVER is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}

package com.app.flocash.appflobiller.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

/**
 * Created by lion on 8/1/16.
 */
public class PopupSelectionMenu {
    protected View mView;
    private static final int X_INDEX = 0;
    private static final int Y_INDEX = 1;
    private OnClickMenuListener onClickMenuListenerOb;
    protected PopupWindow mPopupWindow;
    private final Context context;

    public PopupSelectionMenu(Context context) {
        this.context = context;
        initPopupWindow();

    }

    private void initPopupWindow() {
        mPopupWindow = new PopupWindow(context);
        mPopupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        mPopupWindow.setAnimationStyle(animationStyle);
        mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    mPopupWindow.dismiss();
                    return true;
                } else return false;
            }
        });
    }

    /**
     * show popup window with content layout is @rootLayout above anchor
     * @param anchor
     */
    public void show(View anchor) {
        mPopupWindow.showAtLocation(anchor, Gravity.CENTER, 0, 0);
    }

    /**
     * handler click event when touch each child item
     * @param rootLayout
     */
    private void setOnClick(LinearLayout rootLayout) {
        for (int i = 0; i < rootLayout.getChildCount(); i ++) {
            final int position = i;
            View view = rootLayout.getChildAt(i);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickMenuListenerOb != null) {
                        onClickMenuListenerOb.onClickMenu(view, position);
                        mPopupWindow.dismiss();
                    }
                }
            });
        }
    }

    public OnClickMenuListener getOnClickMenuListenerOb() {
        return onClickMenuListenerOb;
    }

    public void setOnClickMenuListenerOb(OnClickMenuListener onClickMenuListenerOb) {
        this.onClickMenuListenerOb = onClickMenuListenerOb;
    }

    public interface OnClickMenuListener {
        void onClickMenu(View view, int position);
    }

    public void dismissPopup() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }
    }
}

package com.app.flocash.appflobiller.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.adapters.EventListAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.common.FlobillerInterfaces;
import com.app.flocash.appflobiller.models.EventModel;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

import butterknife.Bind;

/**
 * Created by ${binhpd} on 11/18/2016.
 */
public class EventListingFragment extends FloBillerBaseFragment implements FlobillerInterfaces.OnItemClick {

    @Bind(R.id.rvEventListing)
    RecyclerView mRvEventListing;

    private ArrayList<EventModel> mEventList = new ArrayList<>();

    private EventListAdapter mEventAdapter;

    private String mCategoryType;
    private String mCategoryName;

    private LinearLayoutManager layoutManager;
    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_event_listing;
    }

    @Override
    public void onCreateViewFragment(View view) {
        mEventAdapter = new EventListAdapter(mActivity, mEventList, this);
        mRvEventListing.setAdapter(mEventAdapter);
        layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        mRvEventListing.setLayoutManager(layoutManager);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCategoryType = bundle.getString(FlobillerConstaints.KEY_EXTRA_CATEGORY_TYPE);
            mCategoryName = bundle.getString(FlobillerConstaints.KEY_EXTRA_CATEGORY_NAME);
            updateTitle(mCategoryName);
            receivedEvents(mCategoryType);
        }
    }

    private void receivedEvents(String categoryType) {
        JsonParse jsonParse = new JsonParse<>(JSONObject.class);
        jsonParse.AddParam(ConfigUrlsAPI.Authentication_token, values.getAuthToken());
        jsonParse.AddParam(ConfigUrlsAPI.PARA_EVENT_TYPE, categoryType);
        jsonParse.AddParam(ConfigUrlsAPI.FIELD_COUNTRY, values.getUserCountry());

        ApiConnectTask apiConnectTask = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_EVENT_LISTING,
                jsonParse, this);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_GET_EVENTS, JsonParse.REQUEST_TYPE_GET});
    }

    @Override
    public <T> void onSuccess(T result, String method) {
        JSONObject jsonResult = (JSONObject) result;

        try {
            if (isHaveData(jsonResult)) {
                switch (method) {
                    case ConfigUrlsAPI.REQUEST_GET_EVENT_LISTING:
                        Type type = new TypeToken<ArrayList<EventModel>>(){}.getType();
                        Gson gson = new Gson();
                        mEventList.clear();
                        mEventList.addAll((Collection<? extends EventModel>) gson.fromJson(jsonResult.getString("msg"), type));
                        mEventAdapter.notifyDataSetChanged();
                        break;
                }
            } else {
                Toast.makeText(mActivity, R.string.event_in_category_empty, Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFail(String method) {
        Toast.makeText(mActivity, R.string.error_fail, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(View view, int index) {
        EventDetailFragment eventDetailFragment = new EventDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(FlobillerConstaints.KEY_EXTRA_EVENT_ID, mEventList.get(index).getEvent_id());
        bundle.putString(FlobillerConstaints.KEY_EXTRA_EVENT_NAME, mEventList.get(index).getName());
        eventDetailFragment.setArguments(bundle);
        addFragment(R.id.content, eventDetailFragment);
    }
}

package com.app.flocash.appflobiller.utilities;

import android.util.Base64;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by ${binhpd} on 12/4/2015.
 */
public class JsonParse<T> {
    public static final int TIMEOUT = 10000;
    public static final String REQUEST_TYPE_GET = "0";
    public static final String REQUEST_TYPE_POST = "1";
    public static final String REQUEST_TYPE_PUT = "2";
    public static final String REQUEST_TYPE_DELETE = "3";
    private ArrayList<NameValuePair> params;
    private int responseCode;
    private String message;

    private Class<T> type;
    private T response;

    public T getResponse() {
        return response;
    }

    public String getErrorMessage() {
        return message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public JsonParse(Class<T> type) {
        params = new ArrayList<NameValuePair>();
        this.type = type;
    }

    public void AddParam(String name, String value) {
        params.add(new BasicNameValuePair(name, value));
    }

    public void Execute(RequestMethod method, String url) throws Exception {
        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);
        switch (method) {
            case GET: {
                //add parameters
                String combinedParams = "";
                if (!params.isEmpty()) {
                    combinedParams += "?";
                    for (NameValuePair p : params) {
                        String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
                        if (combinedParams.length() > 1)
                            combinedParams += "&" + paramString;
                        else
                            combinedParams += paramString;
                    }
                }

                HttpGet request = new HttpGet(url + combinedParams);
                //add headers
                request.setHeader("Authorization", getB64Auth());
                executeRequest(request, url);
                break;
            }
            case POST: {
                HttpPost request = new HttpPost(url);
                //add headers
                request.setHeader("Authorization", getB64Auth());
                if (!params.isEmpty())
                    request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                executeRequest(request, url);
                break;
            }
            case PUT: {
                HttpPut request = new HttpPut(url);
                //add headers
                request.setHeader("Authorization", getB64Auth());
                if (!params.isEmpty())
                    request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                executeRequest(request, url);
                break;
            }
            case DELETE: {
                HttpDeleteWithBody request = new HttpDeleteWithBody(url);
                request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                request.setHeader("Authorization", getB64Auth());
                CloseableHttpClient httpclient = HttpClients.createDefault();
                CloseableHttpResponse responseHttp = httpclient.execute(request);

                try {
                    responseCode = responseHttp.getStatusLine().getStatusCode();
                    message = responseHttp.getStatusLine().getReasonPhrase();
                    HttpEntity entity = responseHttp.getEntity();
                    if (entity != null) {
                        InputStream instream = entity.getContent();
                        if (type.getSimpleName().equalsIgnoreCase("String"))
                            response = (T) convertStreamToString(instream);
                        else
                            response = (T)new JSONObject(convertStreamToString(instream));
                        // Closing the input stream will trigger connection release
                        instream.close();
                    }
                } catch (ClientProtocolException e) {
                    httpclient.getConnectionManager().shutdown();
                    e.printStackTrace();
                } catch (Exception e) {
                    httpclient.getConnectionManager().shutdown();
                    e.printStackTrace();
                }
                break;
            }

        }
    }

    private String getB64Auth() {
        String source = "flobiller" + ":" + "flo@12345";
        String ret = "Basic " + Base64.encodeToString(source.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
        return ret;
    }

    private void executeRequest(HttpUriRequest request, String url) throws Exception {
        HttpClient client = new DefaultHttpClient();
        HttpResponse httpResponse;
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                if (type.getSimpleName().equalsIgnoreCase("String"))
                    response = (T) convertStreamToString(instream);
                else
                    response = (T) new JSONObject(convertStreamToString(instream));
                // Closing the input stream will trigger connection release
                instream.close();
            }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public enum RequestMethod {
        GET,
        POST,
        PUT,
        DELETE
    }

    class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
        public static final String METHOD_NAME = "DELETE";

        public String getMethod() {
            return METHOD_NAME;
        }

        public HttpDeleteWithBody(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        public HttpDeleteWithBody(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpDeleteWithBody() {
            super();
        }
    }
}
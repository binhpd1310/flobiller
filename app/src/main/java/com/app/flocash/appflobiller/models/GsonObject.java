package com.app.flocash.appflobiller.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuanMobile on 11/12/2015.
 */
public class GsonObject {

    public static class GsonRegisterResult {
        @SerializedName("status")
        public String status;
        @SerializedName("msg")
        public String message;
    }
}

package com.app.flocash.appflobiller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.adapters.AmountAdapter;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.KeyboardUtils;
import com.app.flocash.appflobiller.utilities.Utils;
import com.app.flocash.appflobiller.views.ExpandedGridView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by ${binhpd} on 12/5/2015.
 */
public class AirtimeFragment extends PayNowFragment {

    private ArrayList<String> airtimeTopUps;
    private ArrayList<String> mListAmount;
    private String countryCode, mOperatorId, mUrlLogo, mTopUp;
    private String mAmount;
    private String mOperatorName;
    private AmountAdapter mAmountAdapter;
    @Bind(R.id.gvAmount)
    ExpandedGridView mGvAmount;

    @Bind(R.id.edtPhoneCode)
    EditText mEdtPhoneCode;

    @Bind(R.id.edtMobileNumber)
    EditText mEdtMobileNumber;

    @Bind(R.id.tvOperatorName)
    TextView mTvOperatorName;

    @Bind(R.id.ivLogo)
    ImageView mIvLogo;

    @Override
    public void onCreateViewFragment(View view) {
        super.onCreateViewFragment(view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mOperatorId = bundle.getString(ListOptionAirtimeFragment.EXTRA_OPERATOR_ID);
            mOperatorName = bundle.getString(ListOptionAirtimeFragment.EXTRA_OPERATOR_NAME);
            mUrlLogo = bundle.getString(ListOptionAirtimeFragment.EXTRA_LOGO);

            mTvOperatorName.setText(mOperatorName);
            Picasso.with(mActivity).load(mUrlLogo).into(mIvLogo);
        }

        airtimeTopUps = new ArrayList<>();

        mEdtPhoneCode.setText(values.getCountryCode());
        mEdtMobileNumber.setText(values.getUserMobile());

        countryCode = values.getUserCountry();
        retrieveTopups(mOperatorId);
    }

    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_content_airtime;
    }

    @OnClick({R.id.btnPayRecharge})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPayRecharge:
                KeyboardUtils.hideKeyboard(mActivity);
                if (mEdtMobileNumber.getText().toString().length() < 1) {
                    DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, "All fields are mandatory").show();
                } else {
                    addInvoice();
                }
                break;
        }
    }

    @OnItemClick(R.id.gvAmount)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mAmountAdapter.setIndexFocus(position);
        mAmountAdapter.notifyDataSetChanged();
        mAmount = mListAmount.get(position);
        mTopUp = airtimeTopUps.get(position);
    }

    public void addInvoice() {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_api, "airtime");
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_currency, values.getCurrency());
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_amount, mAmount);
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_countryCode, mEdtPhoneCode.getText().toString());
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_mobile, mEdtMobileNumber.getText().toString());
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_name, mOperatorName);
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_topUp, mTopUp);
        client.AddParam(ConfigUrlsAPI.add_airtime_invoice_operator_id, mOperatorId);
        ApiConnectTask js_invoices = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_ADD_INVOICE, client, this);
        js_invoices.execute(new String[]{ConfigUrlsAPI.add_invoice_url, "1"});
    }



    public void retrieveTopups(String operatorId) {
        String auth_token = values.getAuthToken();
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, auth_token);
        client.AddParam(ConfigUrlsAPI.airtime_invoice_operatorId, operatorId);
        ApiConnectTask js_operators = new ApiConnectTask(mActivity, ConfigUrlsAPI.REQUEST_GET_TOP_UP, client, this);
        js_operators.execute(new String[]{ConfigUrlsAPI.airtimeTopups_url, "0"});
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        final JSONObject result = (JSONObject) data;
        if (result != null && result.length() > 0) {
            switch (method) {

                case ConfigUrlsAPI.REQUEST_GET_TOP_UP:
                    try {
                        JSONObject result_array1 = result.getJSONObject(ConfigUrlsAPI.PARA_TOPUPS);
                        String topups = result_array1.getString(ConfigUrlsAPI.PARA_PRODUCT_LIST);
                        String currency = result_array1.getString(ConfigUrlsAPI.PARA_DESTINATION_CURRENCY);
                        String valueAmout = result_array1.getString(ConfigUrlsAPI.PARA_RETAIL_PRICE_LIST);
                        airtimeTopUps.clear();

                        mListAmount = new ArrayList<>();
                        for (String parts : topups.split(",")) {
                            airtimeTopUps.add(currency + " " + parts);
                        }
                        for (String parts : valueAmout.split(",")) {
                            mListAmount.add(parts);
                        }

                        mAmountAdapter = new AmountAdapter(mActivity, airtimeTopUps);
                        mGvAmount.setAdapter(mAmountAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case ConfigUrlsAPI.REQUEST_ADD_INVOICE:
                    Log.e("reuslt", "" + result);
                    try {
                        String status = result.getString(ConfigUrlsAPI.PARA_STATUS);
                        if (ConfigUrlsAPI.RESULT_SUCCESS.equalsIgnoreCase(status)) {
                            // direct to invoices
                            String invoiceId = result.getJSONObject(ConfigUrlsAPI.PARA_INVOICE).getString(ConfigUrlsAPI.PARA_INVOICE);
                            Intent intent = new Intent(mActivity, SubScreenActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT);
                            bundle.putString(FlobillerConstaints.KEY_EXTRA_ORDER_ID, invoiceId);
                            intent.putExtras(bundle);
                            startActivity(intent);

                            mActivity.finish();
                        }  else if (ConfigUrlsAPI.result_error.equalsIgnoreCase(status)) {
                            String errMsg = result.getString(ConfigUrlsAPI.PARA_MESSAGE);
                            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, errMsg).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }
    }

    @Override
    public void onFail(String method) {
        mProgressDialog.dismiss();
        if (!Utils.checkNetwork(mActivity)) {
            DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
        }

    }
}



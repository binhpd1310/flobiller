package com.app.flocash.appflobiller.common;

/**
 * Created by ${binhpd} on 12/3/2015.
 */
public class ConfigUrlsAPI {
    public static final String BASE_URL = "https://flobiller.flocash.com/";
    public static final String new_user_register_url="https://flobiller.flocash.com/api/user";
    public static final String new_user_register_firstname="firstname";
    public static final String new_user_register_cpass="cpassword";
    public static final String new_user_register_lastname="lastname";
    public static final String new_user_register_country="country";
    public static final String new_user_register_mobile="mobile";
    public static final String new_user_register_emailid="email";
    public static final String new_user_register_password="password";
    public static final String new_user_register_dob="dateofbirth";

    public static final String LOGO_URL_BASE = "https://flobiller.flocash.com/assets/images/upload/";

    public static final String Authentication_token="auth_token";

    public static final String app_login_url="https://flobiller.flocash.com/api/login";
    public static final String app_logout_url="https://flobiller.flocash.com/api/logout";
    public static final String user_email="email";
    public static final String user_password="password";
    public static final String google="google";

    public static final String user_retrieve_url="https://flobiller.flocash.com/api/user";
    public static final String user_version_app="https://flobiller.flocash.com/api/version";

    public static final String user_update_url="https://flobiller.flocash.com/api/user";
    public static final String update_firstname="firstname";
    public static final String update_lastname="lastname";
    public static final String update_dob="dateofbirth";
    public static final String update_country="country";
    public static final String update_mobile="mobile";

    public static final String forgot_pass_url="https://flobiller.flocash.com/api/forget";
    public static final String forget_pass_email="email";

    public static final String billers_url="https://flobiller.flocash.com/api/billers";
    public static final String biller_name="name";
    public static final String biller_logo="logo_txt";
    public static final String biller_id="id";

    public static final String subscribe_url="https://flobiller.flocash.com/api/subscription";
    public static final String subscribe_billerId="biller";
    public static final String subscribe_accName="accname";
    public static final String subscribe_accno = "accno";

    public static final String update_subAccName="accname";
    public static final String update_subAccNo="accno";

    public static final String add_invoice_url ="https://flobiller.flocash.com/api/invoice";
    public static final String add_paybill_invoice_country="country";
    public static final String add_paybill_invoice_topUpName="name";
    public static final String add_paybill_invoice_api="api";
    public static final String add_paybill_invoice_topup="cust_topup";
    public static final String add_paybill_invoice_accName="cust_account_name";
    public static final String add_paybill_invoice_accNumber="cust_account_number";
    public static final String add_paybill_invoice_invoiceNumber="billerinvoice";
    public static final String add_paybill_invoice_billerId="billerid";
    public static final String add_paybill_invoice_currency="currency";
    public static final String add_paybill_invoice_amount="amount";

    public static final String add_mark_as_paid_order_id = "orderid";
    public static final String add_airtime_invoice_api="api";
    public static final String add_airtime_invoice_currency="currency";
    public static final String add_airtime_invoice_amount="amount";
    public static final String add_airtime_invoice_countryCode="countrycode";
    public static final String add_airtime_invoice_mobile="mobile";
    public static final String add_airtime_invoice_name="name";
    public static final String add_airtime_invoice_topUp="cust_topup";
    public static final String add_airtime_invoice_operator_id="operator_id";

    public static final String airtime_invoice_countryId="cid";
    public static final String airtime_invoice_countryIso="ciso";
    public static final String airtime_invoice_operatorId="operator";
    public static final String URL_BASE_LOGO = "https://placeholdit.imgix.net/~text?bg=6EB020&txtcolor=ffffff&txtsize=25&txt=";
    public static final String paybillcountries_url="https://flobiller.flocash.com/api/paybillcountries";
    public static final String paybillbillers_url="https://flobiller.flocash.com/api/paybillbillers";
    public static final String paybillbillers_country_code="country_code";
    public static final String paybillbiller_billerId="biller";
    public static final String API_PAYBILL_SUBSCRIPTIONS ="https://flobiller.flocash.com/api/paybillsubscriptions";

    public static final String airtimeCountries_url="https://flobiller.flocash.com/api/airtimecountries";
    public static final String airtimeOperators_url="https://flobiller.flocash.com/api/airtimeoperators";
    public static final String airtimeTopups_url="https://flobiller.flocash.com/api/airtimetopup";

    public static final String API_GET_INVOICES = "https://flobiller.flocash.com/api/orders";
    public static final String API_GET_INVOICE = "https://flobiller.flocash.com/api/order";
    public static final String API_GET_USER_COUNTRIES = "https://flobiller.flocash.com/api/usercountries";
    public static final String API_REMOVE_SUBSCRIPTION_URL ="https://flobiller.flocash.com/api/subscription";
    public static final String API_MARK_AS_PAID ="https://flobiller.flocash.com/api/notify";
    public static final String BASE_URL_FLAG = "https://flobiller.flocash.com/assets/images/flags/";
    public static final String URL_BASELOGO_PAYBILLER = "https://flobiller.flocash.com/assets/images/upload/";

    public static final String API_RECEIVE_ALL_GIFT_CARD_URL = "https://flobiller.flocash.com/api/giftcards";
    public static final String API_GET_CATEGORIES = "https://flobiller.flocash.com/api/eventcategory";
    public static final String API_GET_EVENTS = "https://flobiller.flocash.com/api/eventtype";
    public static final String API_GET_EVENT_BY_ID = "https://flobiller.flocash.com/api/eventid";
    public static final String API_GET_VALID_SERVICE ="https://flobiller.flocash.com/api/avail_service";
    public static final String API_GET_COUNTRY_FLOBILLER = "https://flobiller.flocash.com/api/flocountries";
    public static final String API_GET_PROMO_LINK = "https://flobiller.flocash.com/api/promo";

    public static final String RECEIVE_ALL_GIFT_CARD_GIFT_CARDS = "giftcards";
    public static final String RECEIVE_ALL_GIFT_CARD_GIFT_CARD = "giftcard";
    public static final String RECEIVE_ALL_GIFT_CARD_GIFT_NAME = "name";
    public static final String RECEIVE_ALL_GIFT_CARD_GIFT_LOGO = "logo";
    public static final String RECEIVE_ALL_GIFT_CARD_GIFT_AMOUNT = "amount";

    public static final String REMOVE__SUBSCRIPTION_ID ="id";
    public static final String INVOICE_SERVICE ="service";
    public static final String INVOICE_START ="start";
    public static final String INVOICE_ORDER_ID ="orderid";
    public static final String result_error = "error";
    public static final String PARA_MESSAGE = "msg";
    public static final String PARA_STATUS = "status";
    public static final String RESULT_SUCCESS = "success";
    public static final String PARA_COUNTRY = "country";
    public static final String PARA_COUNTRIES = "countries";
    public static final String PARA_CURRENCIES = "currencies";
    public static final String PARA_ORDER_ID = "orderid";
    public static final String REQUEST_GET_USER_INFO = "getUserInfo";
    public static final String REQUEST_UPDATE_USER = "updateUser";
    public static final String REQUEST_SAVE = "save";
    public static final String REQUEST_GET_COUNTRIES_LIST = "REQUEST_GET_COUNTRIES_LIST";
    public static final String REQUEST_CREATE_TICKET = "REQUEST_CREATE_TICKET";
    public static final String PARA_MERCHANT = "merchant";
    public static final String PARA_CURRENCY = "currency";
    public static final String PARA_NAME = "name";
    public static final String PARA_AMOUNT = "amount";
    public static final String PARA_INVOICE = "invoice";
    public static final String PARA_BILLER_INVOICEID= "invoice_id";
    public static final String PARA_FLOCASH_STATUS = "flocash_status";
    public static final String PARA_MOBILE = "mobile";
    public static final String PARA_LAST_NAME = "lastname";
    public static final String PARA_FIRST_NAME = "firstname";
    public static final String PARA_DATE_OF_BIRTH = "dateofbirth";
    public static final String PARA_USER = "user";
    public static final String PARA_COUNTRY_ID = "countryid";

    public static final String PARA_OPERATORS = "operators";
    public static final String PARA_OPERATOR = "operator";
    public static final String PARA_OPERATOR_ID = "operatorid";
    public static final String PARA_LOGO = "logo";
    public static final String PARA_TOPUPS = "topups";
    public static final String PARA_PRODUCT_LIST = "product_list";
    public static final String PARA_DESTINATION_CURRENCY = "destination_currency";
    public static final String PARA_RETAIL_PRICE_LIST = "retail_price_list";
    public static final String API_PAYBILL = "paybill";
    public static final String PARA_BILLER_NAME = "billername";
    public static final String PARA_BILLER_LOGO = "billerlogo";
    public static final String PARA_SERVICE_ID = "service_id";
    public static final String PARA_EXTENSION = "extension";
    public static final String PARA_BASE_URL = "base_url";
    public static final String PARA_FLAG = "flag";
    public static final String PAID = "USER-PAID";
    public static final String PARA_VALUE = "value";
    public static final String PARA_EMAIL = "email";
    public static final String PARA_CUSTOMER_ID = "cust_id";
    public static final String PARA_COUNTRY_CODE = "countrycode";
    public static final String add_gift_card_name = "giftcardname";
    public static final String add_gift_card_id = "giftcardid";
    public static final String PARA_EVENT_TYPE = "type";
    public static final String PARA_EVENT_ID = "id";
    public static final String PARA_CREATE_EVENT_ID = "event_id";
    public static final String PARA_CREATE_TICKET_ID = "ticket_id";
    public static final String PARA_QUANTITY = "qty";
    //==============================================================================================
    public static final String REQUEST_GIFT_CARD = "giftcard";
    public static final String REQUEST_GET_ALL_GIFT_CARD = "giftcards";
    public static final String REQUEST_GET_SUBSCRIPTION_DETAIL = "request_get_subscription_detail";
    public static final String REQUEST_UPDATE_SUBSCRIPTION = "request_update_subscription";
    public static final String REQUEST_GET_CATEGORIES = "request_get_categories";
    public static final String REQUEST_ADD_INVOICE = "addInvoice";
    public static final String REQUEST_GET_TOP_UP = "getTopUps";
    public static final String REQUEST_GET_OPERATOR = "getOperators";
    public static final String REQUEST_GET_COUNTRY = "getCountry";
    public static final String REQUEST_GET_BILLER = "getBillers";
    public static final String REQUEST_GET_SUBSCRIPTION = "getSubscriptions";
    public static final String REQUEST_REMOVE_SUBSCRIPTION = "removeSubscriptions";
    public static final String REQUEST_MARK_AS_PAID = "markAsPaid";
    public static final String REQUEST_LOGOUT = "LOG_OUT";
    public static final String REQUEST_GET_APP_VERSION = "REQUEST_GET_APP_VERSION";
    public static final String REQUEST_GET_INVOICE = "getInvoice";
    public static final String REQUEST_GET_EVENT_BY_ID = "REQUEST_GET_EVENT_BY_ID";
    public static final String REQUEST_GET_EVENT_LISTING = "REQUEST_GET_EVENT_LISTING";
    public static final String REQUEST_GET_VALID_SERVICE = "REQUEST_GET_VALID_SERVICE";
    public static final String REQUEST_GET_PROMO_LINK = "REQUEST_GET_PROMO_LINK";

    public static final String STATUS_SUCESS = "success";
    public static final String REQUEST_EVENT_QUANTITY = "REQUEST_EVENT_QUANTITY";
    public static final String API_CONFIRM_QUANTITY = "https://flobiller.flocash.com/api/eventqty";
    public static final String API_EVENT_ADD_INVOICE = "https://flobiller.flocash.com/api/eventadd";
    public static final String MSG_RESPONSE = "msg";
    public static final String PAR_ORDER = "order";
    public static final String PARA_EVENT_DATA = "eventdata";
    public static final String PARA_EVENT_TICKET_DATA = "eventticketdata";
    public static final String PAR_LOCAL_INVOICE = "localinvoice";
    public static final String PARA_AVAIL_SERVICE = "avail_service";
    public static final String FIELD_ID = "id";
    public static final String FIELD_COUNTRY = "country";
    public static final String PARA_CUSTOMER_EMAIL = "cust_email";
    public static final String PARA_CUSTOMER_MOBILE = "cust_mobile";
}

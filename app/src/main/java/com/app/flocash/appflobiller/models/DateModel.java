package com.app.flocash.appflobiller.models;

import java.util.ArrayList;

/**
 * Created by ${binhpd} on 12/1/2016.
 */
public class DateModel {
    private String dateTicket;
    private ArrayList<String> timeTickets = new ArrayList<>();

    public String getDateTicket() {
        return dateTicket;
    }

    public void setDateTicket(String dateTicket) {
        this.dateTicket = dateTicket;
    }

    public void addTime(String time) {
        timeTickets.add(time);
    }
}

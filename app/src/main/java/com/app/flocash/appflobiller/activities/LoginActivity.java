package com.app.flocash.appflobiller.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.service.QuickstartPreferences;
import com.app.flocash.appflobiller.service.RegistrationIntentService;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.KeyboardUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ${binhpd} on 12/1/2015.
 */
public class LoginActivity extends Activity implements ApiConnectTask.FloBillerRequestListener {

    private SavedSharedPreference mSavedSharedPreference;

    @Bind(R.id.edtEmailAccount)
    EditText mEdtEmailAccount;

    @Bind(R.id.edtPassword)
    EditText mEdtPassword;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSavedSharedPreference = new SavedSharedPreference(this);
        ButterKnife.bind(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.notify_please_wait));
        mProgressDialog.setCancelable(false);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                executeLogin();
            }
        };

        mEdtPassword.setImeActionLabel("Done", KeyEvent.KEYCODE_ENTER);

        mEdtPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    login();
                }
                return false;
            }
        });
        // Registering BroadcastReceiver
        registerReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    @OnClick({R.id.edtEmailAccount, R.id.edtPassword, R.id.tvForgot, R.id.btnLogin, R.id.btnRegister})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.edtEmailAccount:
                break;
            case R.id.tvForgot:
                intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.btnLogin:
                login();
                break;
            case R.id.btnRegister:
                intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void login() {
        if (validLoginForm()) {
            KeyboardUtils.hideKeyboard(this);
            getRegId();
        } else {
            Toast.makeText(getApplicationContext(), "Please enter email and password", Toast.LENGTH_LONG).show();
        }
    }

    private void executeLogin() {
        JsonParse client = new JsonParse(JSONObject.class);
        client.AddParam(ConfigUrlsAPI.user_email, mEdtEmailAccount.getText().toString());
        client.AddParam(ConfigUrlsAPI.user_password, mEdtPassword.getText().toString());
        client.AddParam(ConfigUrlsAPI.google, mSavedSharedPreference.getRid());
        ApiConnectTask js = new ApiConnectTask(this, "save", client, this);
        js.setShowDialog(false);
        js.execute(new String[]{ConfigUrlsAPI.app_login_url, JsonParse.REQUEST_TYPE_POST});
    }

    /**
     * valid input user, password
     *
     * @return true if valid otherwise return false
     */
    private boolean validLoginForm() {
        return !TextUtils.isEmpty(mEdtEmailAccount.getText()) && !TextUtils.isEmpty(mEdtPassword.getText());
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        JSONObject result = (JSONObject) data;
        if ((result != null) && (result.length() > 0))
            switch (method) {
                case "save":
                    mProgressDialog.dismiss();
                    SavedSharedPreference values = new SavedSharedPreference(LoginActivity.this);
                    try {
                        String status = result.getString("status");
                        String auth_token = result.getString("auth_token");
                        values.setAuthToken(auth_token);
                        Log.e("token", "" + values.getAuthToken());
                        if (status.equalsIgnoreCase("success")) {
                            mSavedSharedPreference.setUserEmail(mEdtEmailAccount.getText().toString().trim());
                            mSavedSharedPreference.setFirstLogin(true);
                            Intent intent = new Intent(LoginActivity.this, PromoActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                        // e.printStackTrace();
                    }
            }
    }

    @Override
    public void onFail(String method) {
        mProgressDialog.dismiss();
        DialogUtilities.getOkAlertDialogColor(LoginActivity.this, DialogUtilities.FAIL, getString(R.string.notify_error_network)).show();
    }

    public void getRegId() {
        mProgressDialog.show();
        if (TextUtils.isEmpty(mSavedSharedPreference.getRid())) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        } else {
            executeLogin();
        }
    }
}

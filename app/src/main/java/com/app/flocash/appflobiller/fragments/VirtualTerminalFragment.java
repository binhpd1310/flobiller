package com.app.flocash.appflobiller.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.activities.SubScreenActivity;
import com.app.flocash.appflobiller.common.FlobillerConstaints;
import com.app.flocash.appflobiller.utilities.DialogUtilities;
import com.flocash.core.service.IService;
import com.flocash.core.service.ServiceFactory;
import com.flocash.core.service.entity.Response;
import com.flocash.sdk.common.ConfigServer;

/**
 * Created by ${binhpd} on 1/5/2016.
 */
public class VirtualTerminalFragment extends FloBillerBaseFragment{
    public static final String KEY_URL = "com.flocash.flocashecomgui.VirtualTerminalFragment.URL";
    public static final String KEY_TRACENUMBER = "com.flocash.flocashecomgui.VirtualTerminalFragment.TraceNumber";
    private WebView mWvPayment;
    private ProgressDialog mDialog;
    private String mRedirectUrl;
    private String mTraceNumber;
    private String mInvoiceId;
    private boolean mIsLoadingFinish = false;

    public static VirtualTerminalFragment newInstance(String url, String traceNumber)
    {
        VirtualTerminalFragment fragment = new VirtualTerminalFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_URL, url);
        bundle.putString(KEY_TRACENUMBER, traceNumber);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            mRedirectUrl = getArguments().getString(KEY_URL);
            mTraceNumber = getArguments().getString(KEY_TRACENUMBER);
            mInvoiceId = getArguments().getString(FlobillerConstaints.KEY_EXTRA_ORDER_ID);
            //testing only
//			url = "https://mpi.valucardnigeria.com:443/index.jsp?OrderID=2714666&SessionID=E1F73AB11DBDBF6DC48A523D5988A49D";
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
    }


    @Override
    public int getContentIdFragment() {
        return R.layout.fragment_virtual_terminal;
    }

    @Override
    public void onCreateViewFragment(View view) {
        fragmentGettingStarted(view);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void fragmentGettingStarted(View view) {
        try {
            mWvPayment = (WebView) view.findViewById(R.id.wvPayment);
            mWvPayment.getSettings().setJavaScriptEnabled(true);
            mWvPayment.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (!mIsLoadingFinish) {
//                    view.loadUrl(url);
                        if (!mRedirectUrl.equalsIgnoreCase(url)) {
//                        ((MainActivity) getActivity()).pushFragment(PaymentResultFragment.newInstance(PaymentResultFragment.PaymentResultPushedEnum.FROM_VIRTUAL.getName(), mTraceNumber, null));
//                            Intent intent = new Intent(mActivity, SubScreenActivity.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT_RESULT_FRAGMENT);
//                            bundle.putString(PaymentResultFragment.KEY_TRACENUMBER, mTraceNumber);
//                            bundle.putInt(PaymentResultFragment.KEY_PUSHEDFROM, PaymentResultFragment.PaymentResultPushedEnum.FROM_VIRTUAL.getName());
//                            bundle.putString(PaymentResultFragment.KEY_INSTRUCTION, null);

                            new GetOrderTask().execute(new String[]{mTraceNumber});

//                            intent.putExtras(bundle);
//                            mActivity.startActivity(intent);
//                            mActivity.finish();
                        }
                        mIsLoadingFinish = true;
                    }
                    return false;
                }
            });
            loadWebviewUrl(mRedirectUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadWebviewUrl(String url)
    {
        if(mDialog != null) {
            mDialog.cancel();
        }
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Loading...");
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
        mWvPayment.loadUrl(url);
    }

    @Override
    public void onDestroyView() {
        mWvPayment.clearCache(true);
        super.onDestroyView();
    }

    @Override
    public <T> void onSuccess(T result, String method) {

    }

    @Override
    public void onFail(String method) {

    }

    private class GetOrderTask extends AsyncTask<String, Void, Response> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            if(pDialog != null)
            {
                pDialog.cancel();
            }

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Getting data...");
            pDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Response doInBackground(String... params) {
            IService service = new ServiceFactory().getSerivceStaging(ConfigServer.USER_NAME, ConfigServer.PASSWORD, ConfigServer.PUBLICKEY);
            Response result = null;
            try {
                result = service.getOrder(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Response result) {
            if(pDialog != null)
            {
                pDialog.cancel();
            }
            if(result != null && result.isSuccess() && result.getOrder() != null)
            {
                StringBuilder message = new StringBuilder();
                message.append("Order number: " + result.getOrder().getTraceNumber() + "\n");
                message.append("Amount: " + result.getOrder().getAmount() + "\n");
                message.append("Status: " + result.getOrder().getStatus() + "\n");
                message.append("Currency: " + result.getOrder().getCurrency());
                final String content = message.toString();
                final AlertDialog alertDialog = DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.SUCCESS, content + "\nRedirect 5s", false);
                alertDialog.show();
                new CountDownTimer(FlobillerConstaints.COUNT_DOWN_TIME, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
//                        button.setText("Redirect 00:" + (millisUntilFinished / 1000));
                    }

                    @Override
                    public void onFinish() {
                        alertDialog.dismiss();
                        Intent intent = new Intent(mActivity, SubScreenActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt(FlobillerConstaints.KEY_EXTRA_SCREEN_ID, FlobillerConstaints.SCREEN_PAYMENT);
                        bundle.putString(FlobillerConstaints.KEY_EXTRA_ORDER_ID, mInvoiceId);
                        bundle.putBoolean(FlobillerConstaints.KEY_EXTRA_IS_PAID, true);
                        intent.putExtras(bundle);
                        mActivity.startActivity(intent);
                        mActivity.finish();
                    }
                }.start();
//                tvResult.setText(message.toString());
            } else if(result != null && !result.isSuccess()){
                DialogUtilities.getOkAlertDialogColor(mActivity, DialogUtilities.FAIL, result.getErrorCode()+": "+ result.getErrorMessage()).show();
            }
            super.onPostExecute(result);
        }

    }
}

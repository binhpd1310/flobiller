package com.app.flocash.appflobiller.dao;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by ${binhpd} on 3/16/2016.
 */
public class DbHelper extends SQLiteAssetHelper {
    public static final String DATABASE_NAME = "FloCash.sqlite";
    private static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}


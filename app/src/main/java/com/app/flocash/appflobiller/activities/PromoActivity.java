package com.app.flocash.appflobiller.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.app.flocash.appflobiller.R;
import com.app.flocash.appflobiller.common.ConfigUrlsAPI;
import com.app.flocash.appflobiller.tasks.ApiConnectTask;
import com.app.flocash.appflobiller.utilities.JsonParse;
import com.app.flocash.appflobiller.utilities.LogUtils;
import com.app.flocash.appflobiller.utilities.SavedSharedPreference;
import com.google.gson.JsonObject;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lion on 7/13/17.
 */

public class PromoActivity extends BaseActivity {
    protected SavedSharedPreference values;

    @Bind(R.id.ivPromo)
    ImageView mIvPromo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);
        ButterKnife.bind(this);
        values = new SavedSharedPreference(this);
        getPromoLink();
    }


    @OnClick({R.id.tvSkip})
    public void onClick(View view) {
        Intent intent = new Intent(PromoActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public <T> void onSuccess(T data, String method) {
        String content = data.toString();
        content = content.replaceAll("^\"|\"$", "");
        content = content.replaceAll("\\\\", "");
        JSONObject result;
        try {
            result = new JSONObject(content);
            if ((result != null) && (result.length() > 0)) {
                switch (method) {
                    case ConfigUrlsAPI.REQUEST_GET_PROMO_LINK:
                        try {
                            JSONObject banner = result.getJSONObject("banner");
                            String url = banner.getString("base_url") + result.getString("path") + "."  + banner.getString("extension");
                            loadPromoImage(url);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getPromoLink() {
        JsonParse client = new JsonParse(JsonObject.class);
        client.AddParam(ConfigUrlsAPI.Authentication_token, values.getAuthToken());
        client.AddParam(ConfigUrlsAPI.user_email, values.getUserEmail());
        ApiConnectTask apiConnectTask = new ApiConnectTask(this, ConfigUrlsAPI.REQUEST_GET_PROMO_LINK, client, this);
        apiConnectTask.setShowDialog(false);
        apiConnectTask.execute(new String[]{ConfigUrlsAPI.API_GET_PROMO_LINK, JsonParse.REQUEST_TYPE_GET});
    }

    private void loadPromoImage(String url) {
        Picasso.with(this).load(url).into(mIvPromo, new Callback() {
            @Override
            public void onSuccess() {
                LogUtils.d("callback", "success");
            }

            @Override
            public void onError() {
                LogUtils.d("callback", "error");
            }
        });
    }
}

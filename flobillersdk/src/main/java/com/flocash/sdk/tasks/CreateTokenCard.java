package com.flocash.sdk.tasks;

import android.os.AsyncTask;

import com.flocash.core.service.IService;
import com.flocash.core.service.ServiceFactory;
import com.flocash.core.service.entity.Request;
import com.flocash.core.service.entity.Response;
import com.flocash.sdk.common.ConfigServer;

/**
 * Created by ${binhpd} on 5/28/2017.
 */

public class CreateTokenCard extends AsyncTask<Request, Void, Response> {
    @Override
    protected Response doInBackground(Request... params) {
        IService iService = new ServiceFactory().getSerivceStaging(ConfigServer.USER_NAME, ConfigServer.PASSWORD, ConfigServer.PUBLICKEY);
        Response result = null;
        try {
            result = iService.createTokenCard(params[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
    }
}
